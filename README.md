# Application BioMétéo du Conseil Général de la Dordogne

Basé sur la librairie :

![npm](https://img.shields.io/npm/v/@mnemotix/synaptix.js.svg?label=Synaptix.js)

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

- [Description](#description)
- [Procédure pour lancer l'application.](#proc%C3%A9dure-pour-lancer-lapplication)
  - [Lancement d'un environnement local pour développer l'application.](#lancement-dun-environnement-local-pour-d%C3%A9velopper-lapplication)
  - [Installation des dépendances javascript.](#installation-des-d%C3%A9pendances-javascript)
  - [Déclaration des variables d'environnement obligatoires](#d%C3%A9claration-des-variables-denvironnement-obligatoires)
  - [Démarrage de l'application](#d%C3%A9marrage-de-lapplication)
    - [En mode développement](#en-mode-d%C3%A9veloppement)
    - [En mode production](#en-mode-production)
      - [Désactiver le frontend](#d%C3%A9sactiver-le-frontend)
      - [Variables d'environnements](#variables-denvironnements)
  - [Déploiement applicatif](#d%C3%A9ploiement-applicatif)
    - [Construction de l'image Docker de l'application](#construction-de-limage-docker-de-lapplication)
    - [Déploiement dans un environnement de production](#d%C3%A9ploiement-dans-un-environnement-de-production)
  - [Tests](#tests)
- [Plus de ressources pour développer](#plus-de-ressources-pour-d%C3%A9velopper)
- [Modèle de données](#mod%C3%A8le-de-donn%C3%A9es)
- [Navigation grâce aux paramètres d'URL](#navigation-grâce-aux-paramètres-durl)
  - [cityCode](#citycode)
  - [anchor](#anchor)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Description

L'application BioMétéo est un serveur NodeJS offrant un point d'entrée public sur un HUB de données et un site web permettant de l'explorer. Pour cela, il démarre :

- Un point d'accès GraphQL pour délivrer de la donnée dans un format standardisé.
- Un point d'accès SPARQL pour accéder directement au Triple Store. (A venir)

## Procédure pour lancer l'application.

### Lancement d'un environnement local pour développer l'application.

Avant le lancer l'application, il faut un environnement de développement fonctionnel.

Au premier déploiement, installer le système de fichiers pour les volumes permanents Docker :

```
./launcher/console install
```

Puis lancer la stack :

```
./launcher/console start
```

### Installation des dépendances javascript.

Installer Yarn et lancer la commande :

```
yarn install
```

Elle a pour effet d'installer toutes les dépendances.

### Déclaration des variables d'environnement obligatoires

Créer un fichier `.env` à la racine de l'application et renseigner les variables d'environnements obligatoires pour le lanchement de l'application :

```
# Clé pour accéder à l'API OpenWeather
OPEN_WEATHER_KEY=
# Clé pour accéder à l'API Geonames
GEONAMES_USERNAME=
```

Note: D'autres variables d'environnement sont réglables dans ce fichier. Voir la section "Variables d'environnement".

### Démarrage de l'application

#### En mode développement

Lancer la commande :

```
yarn start
```

Elle démarre les serveurs Webpack / ExpressJS sur le port 3034 (par défaut, voir la section suivante pour le changer) avec un endpoint GraphQL.

L'option de rechargement à chaud est activée par défaut. Il permet un rafraîchissement automatique du navigateur à la détection de changement dans le code source.

#### En mode production

En mode production, le lancement se fait en deux étapes.

Lancer d'abord la commande de compilation et d'optimisation du code source :

```
yarn build:prod
```

Puis lancer l'application :

```
yarn start:prod
```

Elle démarre un serveur ExpressJS sur le port 3034 (par défaut, voir la section suivante pour le changer) avec un endpoint GraphQL. Webpack n'est plus présent.

##### Désactiver le frontend

Pour accélérer le démarrage de l'application dans le cas d'un développement de l'API seule, il est possible de désactiver le chargement du frontend.

```
FRONTEND_DISABLED=1 yarn start
```

Ou bien ajouter la variable d'environnement dans le fichier `.env`

```
FRONTEND_DISABLED=1
```

##### Variables d'environnements

Il est possible avant le démarrage d'initialiser vos propres variables d'environnements.

La liste est définie dans le fichier [./config/environment.js](./config/environment.js)

Par exemple pour changer le port d'écoute du serveur, il suffit de lancer :

```
APP_PORT=80 yarn start
```

Vous pouvez également définir une liste de variable à utiliser dans le fichier `.env` à la racine (fichier non versionné).

**Example :**

- fichier `.env`

```
APP_PORT=80
RABBITMQ_HOST=localhost
RABBITMQ_PORT=5672
```

### Déploiement applicatif

#### Construction de l'image Docker de l'application

L'application intègre un fichier `.gitlab-ci.yml` qui permet de configurer une intégration continue qui peut :

- Lancer des tests à chaque modification applicative (via `git push`).
- Constuire une image Docker de l'application à chaque création de tag applicatif (via `git push`).

Lancer la commande :

```
yarn version
```

Renseigner le numéro de tag en suivant la règle [Semantic Versioning](https://semver.org/).

A la validation, le fichier `CHANGELOG.md` ainsi que la version du fichier `package.json` est mis à jour, un tag Git est créé et un pipeline Gitlab CI est lancé à l'issu de laquelle une nouvelle image docker est créé et disponible dans la [bibliothèque de conteneurs applicatifs](https://gitlab.com/groups/cg24/-/container_registries).

#### Déploiement dans un environnement de production

Voici un template de fichier `docker-compose.yml` pour démarrer une stack application complète :

```
version: '2.1'
services:
  rabbitmq:
    image: rabbitmq:3.8.3-alpine
    environment:
      RABBITMQ_DEFAULT_PASS: ${RABBITMQ_PASSWORD}
      RABBITMQ_DEFAULT_USER: ${RABBITMQ_USER}
  index-controller:
    image: registry.gitlab.com/mnemotix/synaptix/synaptix-index-controller:0.1.10-SNAPSHOT
    environment:
      RABBITMQ_HOST: rabbitmq
      RABBITMQ_PORT: '5672'
      RABBITMQ_USER: ${RABBITMQ_USER}
      RABBITMQ_PWD:  ${RABBITMQ_PASSWORD}
      RABBITMQ_EXCHANGE_NAME: ${RABBITMQ_EXCHANGE_NAME}
      RABBITMQ_TIMEOUT: '10000'
      RABBITMQ_DURABLE_MESSAGES: 'true'
      ES_MASTER_URI: ${ES_MASTER_URI}
      ES_CLUSTER_USER:  ${ES_CLUSTER_USER}
      ES_CLUSTER_PWD: ${ES_CLUSTER_PWD}
  graph-controller:
    image: registry.gitlab.com/mnemotix/synaptix/synaptix-graph-controller:0.1.8-SNAPSHOT
    environment:
      RABBITMQ_HOST: rabbitmq
      RABBITMQ_PORT: '5672'
      RABBITMQ_USER: ${RABBITMQ_USER}
      RABBITMQ_PWD:  ${RABBITMQ_PASSWORD}
      RABBITMQ_EXCHANGE_NAME: ${RABBITMQ_EXCHANGE_NAME}
      RABBITMQ_TIMEOUT: '10000'
      RABBITMQ_DURABLE_MESSAGES: 'true'
      RDFSTORE_ROOT_URI: ${RDFSTORE_ROOT_URI}
      RDFSTORE_USER: ${RDFSTORE_USER}
      RDFSTORE_PWD:  ${RDFSTORE_PWD}
      RDFSTORE_READ_ENDPOINT: ''
      RDFSTORE_REPOSITORY_NAME: ${RDFSTORE_REPOSITORY_NAME}
      RDFSTORE_WRITE_ENDPOINT: statements
  biometeo:
    image: registry.gitlab.com/cg24/mnb-biometeo:${BIOMETEO_TAG_VERSION}
    environment:
      APP_PORT: '80'
      APP_URL: ${APP_URL}
      OAUTH_DISABLED: '1'
      RABBITMQ_HOST: rabbitmq
      RABBITMQ_PORT: '5672'
      RABBITMQ_LOGIN: ${RABBITMQ_USER}
      RABBITMQ_PASSWORD: ${RABBITMQ_PASSWORD}
      RABBITMQ_EXCHANGE_NAME: ${RABBITMQ_EXCHANGE_NAME}
      RABBITMQ_LOG_LEVEL: ERROR
      RABBITMQ_EXCHANGE_DURABLE: '1'
      RABBITMQ_RPC_TIMEOUT: '30000'
      INDEX_PREFIX_TYPES_WITH: ${ES_INDICES_PATTERN}
      SYNAPTIX_USER_SESSION_COOKIE_NAME: SNXID
      OPEN_WEATHER_KEY: ${OPEN_WEATHER_KEY}
      GEONAMES_USERNAME: ${GEONAMES_USERNAME}
      THUMBOR_BASE_URL: https://images.mnemotix.com/unsafe
```

Les variables d'environnement suivantes sont à renseigner dans le fichier `.env` joint au fichier `docker-compose.yml`.

```
# ----------------------------------------------
# --- RabbitMQ
# ----------------------------------------------
# RABBITMQ_LOGIN: Login RabbitMQ au choix.
# RABBITMQ_PASSWORD: Mot de passe aléatoire à générer.
# RABBITMQ_EXCHANGE_NAME: Nom aléatoire de l'exchange RabbiMQ interne. Par exemple : mnb-exchange.

# ----------------------------------------------
# --- GraphDB
# ----------------------------------------------
# GRAPHDB_ROOT_URI : URI de GraphDB (https://...)
# GRAPHDB_REPOSITORY : Nom du repo GraphDB pour les données du DDF
# GRAPHDB_USER : Utilisateur pour accéder au repo avec des droits en lecture/écriture
# GRAPHDB_PASSWORD : Mot de passe utilisateur.

# ----------------------------------------------
# --- ElasticSearch
# ----------------------------------------------
# ES_MASTER_URI : Uri d'ES (https://)
# ES_INDEX_NAME : Nom de l'index utilisé.
# ES_CLUSTER_USER : Utilisateur ES (si utilisé avec un utilisateur)
# ES_CLUSTER_PWD :  Mot de passe Utilisateur ES (si utilisé avec un utilisateur)
# ES_INDICES_PATTERN : Pattern ES (notion Kibana https://www.elastic.co/guide/en/kibana/current/index-patterns.html) préfixant tous les indices utilisés par l'application. Par exemple "mnb-"

# ----------------------------------------------
# --- APIs externes
# ----------------------------------------------
# GEONAMES_USERNAME : Identifiant Geonames
# OPEN_WEATHER_KEY  : Identifiant OpenWeather
# TWITTER_API_KEY   : Clé API Twitter
# TWITTER_SECRET_KEY : Secret API Twitter
# TWITTER_ACCESS_TOKEN : Access token compte Cg24 Twitter
# TWITTER_ACCESS_TOKEN_SECRET : Access token secret compte Cg24 Twitter
# TWITTER_USER_SCREEN_NAME : Twitter User (the one after @) to listen to
```

### Tests

Lancer la suite de test

```
yarn test
```

Lancer la suite de test en mode debuggage, en utilisant chrome developper tools comme client de debuggage.

```
yarn test:debug
```

## Plus de ressources pour développer

L'application est largement basée sur la bibliothèque [Synaptix.js](https://gitlab.com/mnemotix/synaptix.js). La partie complémentaire de la documentation est disponible dans son fichier [README](https://gitlab.com/mnemotix/synaptix.js/blob/master/README.md).

## Edition des fichiers statiques ( markdown )

Le contenu des pages statiques ce trouve dans les fichiers d'extensions "md" de ce dossier `/mnb-biometeo/src/client/assets/markdownPage/`

https://gitlab.com/cg24/mnb-biometeo/-/tree/a508ed6b16212e612f7deaeee7d234677bba1d09/src/client/assets/markdownPage

Il est également possible de rajouter une/des page(s) statique(s) en modifiant le fichier staticPages.js et en créant le(s) fichier(s) MD correspondant(s).

## Compte Twitter

Pour le moment le lien du compte twitter est défini la :
https://gitlab.com/cg24/mnb-biometeo/blob/0e6d9f6bc59b37b5bd8652b24addfce15c9905f8/src/client/components/widgets/News/News.js#L8

Il faudrait surement le mettre ailleur

## Modèle de données

La documentation du modèle de données est disponible à cette [URL](https://cg24.gitlab.io/mnb-models/).

## Routes

- /previsions
- /qualiteair
- /taxon/?taxonid=http%3A%2F%2Ftaxref.mnhn.fr%2Flod%2Ftaxon%2F64455%2F12.0&back=t
- /menu
- /especesfavorites
- /infopratiques
- /credits
- /actualitesfavorites

## Navigation grâce aux paramètres d'URL

### /?geoloc

Il est possible de passer le paramètre `geoloc` suivi de l'identifiant [geonames](http://www.geonames.org/) dans l'URL pour forcer la géolocalisation de l'application. La liste des codes géonames de toutes les communes de Dordogne est accessible à [ce lien](https://docs.google.com/spreadsheets/d/1g6KJvegNQHqyjzuot6FdGzsFuMOr86rtN__Iszi4d-4/edit#gid=0)

Par exemple l'URL suivante géolocalisera sur Périgueux :

```
https://biometeo.dordogne.fr?geoloc=6429478
```

### /?anchor

Il est possible de scroller automatiquement sur une partie de la page web désirée en passant le paramètre `anchor` suivi d'une des valeurs

- de `meteo`
- de catégorie : `air`, `eau` ou `sol`.
- de `graph` pour avoir le graphique des niveaux de rivières ou nappes
- de `actualites`pour avoir les actualités twitter

Par exemple l'URL suivante démarrera l'application directement sur la partie "eau".

```
https://biometeo.dordogne.fr?anchor=eau
```

### /?type

Il permet de paramètrer si on affiche le graphique sur les nappes ou les rivières

- ?type=nappe
- ?type=riviere

Pour que afficher directement la zone des graphiques "eau" sur un type prédéfini, il faut combiner `?type=` avec le paramètre `anchor`, e.g de cette manière :

- https://biometeo.dordogne.fr/?anchor=graph&type=nappe affichera le graphique de la nappe phréatique
- https://biometeo.dordogne.fr/?anchor=graph&type=riviere affichera le graphique de la rivière

### Combiner les paramètres pour automatiser la navigation

Il est possible de passer de combiner les paramètres `anchor` et `geoloc` pour afficher directement certaines zones de l'application sur une localisation prédéfinies. Cette fonction est notamment utilisée les affichages publics.

Exemples :

- Afficher la section "Actualité" pour la ville de Bergerac (code geonames : 6455004) https://test-mnb-biometeo.mnemotix.com/?geoloc=6455004&anchor=actualites
- Afficher la section "Graphiques du niveau de débit de la rivière" pour la ville de Périgueux (code geonames : 6429478) https://test-mnb-biometeo.mnemotix.com/?geoloc=6429478?anchor=graph&type=riviere

### Afficher directement la page détail d'un taxon /taxon/?taxonid

Il est possible de passer en paramètre l'identifiant d'un taxon pour le partager ( c'est ce qui est utilisé pour le partage sur les réseaux sociaux)
exemple :

https://test-mnb-biometeo.mnemotix.com/taxon/?taxonid=http%3A%2F%2Ftaxref.mnhn.fr%2Flod%2Ftaxon%2F3140%2F12.0

Note : il faut au préalable encoder l'identifiant passé en paramètre ( via `encodeURIComponent()` par exemple ) sinon les slash (`/`)
qu'il contient gènèrent des bugs. Cette opération est par ailleurs réalisée automatiquement lors de l'utilisation de la fonctionnalité de partage.
