/*
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
export default {
  UUID: {
    description: 'This is the application UUID.',
    defaultValue: 'mnb-biometeo',
    defaultValueInProduction: true
  },
  SCHEMA_NAMESPACE_MAPPING: {
    description: 'The MnB ontology schema namespace mapping',
    defaultValue: JSON.stringify({
      "rdfs": "http://www.w3.org/2000/01/rdf-schema#",
      "skos": "http://www.w3.org/2004/02/skos/core#",
      "geonames": "https://sws.geonames.org/",
      "foaf": "http://xmlns.com/foaf/0.1/",
      "prov": "http://www.w3.org/ns/prov#",
      "sioc": "http://rdfs.org/sioc/ns#",
      "mnx": "http://ns.mnemotix.com/ontologies/2019/8/generic-model/",
      "mnb": "http://mnb.dordogne.fr/ontology/"
    }),
    defaultValueInProduction: true
  },
  NODES_NAMESPACE_URI: {
    description: 'The MnB nodes (or individuals) (or class instances) namespace URI',
    defaultValue: 'http://mnb.dordogne.fr/data/',
    defaultValueInProduction: true
  },
  NODES_PREFIX: {
    description: 'The MnB nodes or individuals) (or class instances) namespace URI',
    defaultValue: 'mnbd',
    defaultValueInProduction: true
  },
  NODES_NAMED_GRAPH: {
    description: 'The MnB nodes named graph URI',
    defaultValue: 'http://mnb.dordogne.fr/data/taxrefNG/',
    defaultValueInProduction: true
  },
  OAUTH_DISABLED: {
    description: 'This is the OAUTH authentication URL',
    defaultValue: '1',
    defaultValueInProduction: true
  },
  APP_PORT: {
    description: 'This is listening port of the application.',
    defaultValue: 3034
  },
  APP_URL: {
    description: 'This is the base url of the application.',
    defaultValue: () => `http://localhost:${process.env.APP_PORT}`
  },
  RABBITMQ_HOST : {
    description: 'This is RabbitMQ host.',
    defaultValue: 'localhost'
  },
  RABBITMQ_PORT : {
    description: 'This is RabbitMQ port.',
    defaultValue: 5672
  },
  RABBITMQ_LOGIN : {
    description: 'This is RabbitMQ login.',
    defaultValue: 'guest'
  },
  RABBITMQ_PASSWORD : {
    description: 'This is RabbitMQ password.',
    defaultValue: 'rspwd!',
    obfuscate: true
  },
  RABBITMQ_EXCHANGE_NAME : {
    description: 'This is RabbitMQ exchange name.',
    defaultValue: 'local-mnb',
    defaultValueInProduction: true
  },
  RABBITMQ_EXCHANGE_DURABLE : {
    description: 'Is RabbitMQ exchange durable.',
    defaultValue: "0",
    defaultValueInProduction: true
  },
  RABBITMQ_RPC_TIMEOUT: {
    description: 'RPC timeout',
    defaultValue: 10e3,
    defaultValueInProduction: true
  },
  INDEX_DISABLED: {
    description: 'Is index disabled',
    defaultValue: '0',
    defaultValueInProduction: true
  },
  INDEX_PREFIX_TYPES_WITH: {
    description: 'Prefix all index types with a prefix',
    defaultValue: () => `${process.env.COMPOSE_PROJECT_NAME}-`,
    defaultValueInProduction: true
  },
  RABBITMQ_LOG_LEVEL: {
    description: 'RabbitMQ log level (DEBUG, ERROR or NONE)',
    defaultValue: "ERROR",
    defaultValueInProduction: true
  },
  GEONAMES_USERNAME: {
    description: 'Geonames usename to call geonames API',
    obfuscate: true
  },
  SYNAPTIX_USER_SESSION_COOKIE_NAME: {
    description: 'This is the session cookie name',
    defaultValue: 'SNXID',
    defaultValueInProduction: true,
    exposeInGraphQL: true
  },
  THUMBOR_BASE_URL: {
    description: 'This is the Thumbor (thumbnail generation) endpoint',
    defaultValue: 'https://images.mnemotix.com/unsafe',
    exposeInGraphQL: true
  },
  OPEN_WEATHER_KEY: {
    description: 'This is the OpenWeather API key',
    obfuscate: true
  },
  TWITTER_API_KEY: {
    description: 'This is the Twitter API key',
    obfuscate: true
  },
  TWITTER_SECRET_KEY: {
    description: 'This is the Twitter API secret key',
    obfuscate: true
  },
  TWITTER_ACCESS_TOKEN: {
    description: 'This is the Twitter auth token',
    obfuscate: true
  },
  TWITTER_ACCESS_TOKEN_SECRET: {
    description: 'This is the Twitter auth token secret',
    obfuscate: true
  }
}