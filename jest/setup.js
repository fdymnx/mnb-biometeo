import '@babel/polyfill';
import 'jest-dom/extend-expect';
import {setupI18n} from './setup/i18n';
import {setupFetchMock} from './setup/fetchMock';
import {setupEnvironmentVar} from './setup/environmentVar';
import {setupMockReactResponsive} from './setup/mockReactResponsive';
import {fr} from '../src/locales/fr';


/**
 * Setup Jest environment, in which tests will run.
 */

setupI18n({locales: {fr}});
setupFetchMock();
setupEnvironmentVar();
setupMockReactResponsive();
