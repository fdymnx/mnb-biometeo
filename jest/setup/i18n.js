import I18n from 'i18next';
import { initReactI18next } from 'react-i18next';

const defaultLocales = {fr: {}, en: {}};

export function setupI18n({locales} = {}) {
  I18n.use(initReactI18next)
  .init({
    lng: 'fr',
    fallbackLng: 'fr',
    ns: ['common'],
    defaultNS: 'common',
    debug: false,
    interpolation: {
      escapeValue: false // not needed for react!!
    },
    keySeparator: '.',
    resources: loadLocalesForTesting(locales || defaultLocales),
    react: {
      useSuspense: false
    }
  }, (err, t) => {});
}

function loadLocalesForTesting(locales) {
  const languages = ['fr', 'en'];

  const retLocales = {};

  languages.forEach((lng) => {
    retLocales[lng] = {
      'common': locales[lng]
    }
  });

  return retLocales;
}

