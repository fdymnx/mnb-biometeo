import { ApolloClient } from 'apollo-client';
import { InMemoryCache as Cache } from 'apollo-cache-inmemory';
import { MockLink } from '@apollo/react-testing';

export function generateMockedApolloClient({mocks, cache, defaultOptions, link, resolvers, addTypename}) {
  return new ApolloClient({
    cache: cache || new Cache({ addTypename }),
    defaultOptions,
    link: link || new MockLink(mocks || [], addTypename),
    resolvers
  });
}
