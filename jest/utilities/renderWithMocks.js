import React                          from 'react';
import {render}                       from '@testing-library/react';
import {MemoryRouter, Route}          from 'react-router-dom'
import {MockedProvider}               from '@apollo/react-testing';
import {CacheWithFragmentMatcher}     from './apolloClient';



/**
 * This helper function uses render() from @testing-library/react. It has side effects and it's important to call
 * cleanup() after the test in your test suite
 *
 */
export function renderWithMocks({element, locationPath, gqlMocks, routePath}) {
  locationPath = locationPath || "/";
  routePath = routePath || "/";

  return render(
    <MemoryRouter initialEntries={[locationPath]} >
      <MockedProvider mocks={gqlMocks} cache={CacheWithFragmentMatcher}>
        <Route path={routePath} render={() => element}/>
      </MockedProvider>
    </MemoryRouter>
  );
}
