
let initialWidth;

/**
 * @param {string} sizeText Either 'mobile' or 'desktop'
 */
export function setScreenSize(sizeText) {
  if (!initialWidth) {
    initialWidth = window.innerWidth;
  }
  if (sizeText == 'mobile') {
    window.innerWidth = 300;
  }
  if (sizeText == 'desktop') {
    window.innerWidth = 1920;
  }
}

export function resetScreenSize() {
  if (initialWidth) {
    window.innerWidth = initialWidth;
  }
}
