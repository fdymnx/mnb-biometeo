/*
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, {useState, useEffect} from "react";
import {goToAnchor} from "react-scrollable-anchor";

import {Switch, Route} from "react-router-dom";
import {useLazyQuery} from "@apollo/react-hooks";
import {gqlMeteo} from "./components/widgets/Meteo/meteo.gql";
import {gqlCategories} from "./components/widgets/Categorie/categorie.gql";
import {gqlWater} from "./components/widgets/Water/water.gql";
import {_riverObservation} from "./components/widgets/Water/WaterGraph";

import {SplashScreen} from "./components/pages/SplashScreen";
import {MainScreen} from "./components/pages/MainScreen";
import {Screen} from "./components/pages/Screen";
import {createMuiTheme} from "@material-ui/core/styles";
import {ThemeProvider} from "@material-ui/styles";
import Config from "./Config";
import "./App.css";
import {useUrlQuery} from "./hooks/useUrlQuery";
import {useMnbGeolocService} from "./hooks/useMnbGeolocService";
import {
  FavoritesSpeciesContextProvider,
  getFavoritesSpecies,
  setFavoritesSpecies
} from "./hooks/useFavoritesSpeciesContext";

import {FavoritesNewsContextProvider, getFavoritesNews, setFavoritesNews} from "./hooks/useFavoritesNewsContext";

import {NewsContext} from "./hooks/NewsContext";

import {DrawerRouter} from "./components/widgets/SideDrawer/DrawerRouter";

const theme = createMuiTheme({
  typography: {
    fontFamily: ["Segoe UI Regular", "Segoe UI", "Roboto", '"Helvetica Neue"', "Arial", "sans-serif"].join(",")
  },
  breakpoints: Config.breakpoints
});

export function BioMeteo({} = {}) {
  // context pour avoir le nombre de news non lue pour mettre en notif
  const [newsContextValue, newsContextSetValue] = useState(0);

  // context pour les espèces favorites dans le localstorage
  const [valueFavoritesSpecies, setValueFavoritesSpecies] = useState(getFavoritesSpecies());
  function handleFavoritesSpecies(newValue) {
    setFavoritesSpecies(newValue);
    setValueFavoritesSpecies(newValue);
  }

  // context pour les actualités favorites dans le localstorage
  const [valueFavoritesNews, setValueFavoritesNews] = useState(getFavoritesNews());
  function handleFavoritesNews(newValue) {
    setFavoritesNews(newValue);
    setValueFavoritesNews(newValue);
  }

  // recupere les parametres depuis l'url
  //  https://biometeo.dordogne.fr?geoloc=6616167&anchor=eau
  //  http://localhost:3034/taxon/?taxonid=http%3A%2F%2Ftaxref.mnhn.fr%2Flod%2Ftaxon%2F2669%2F12.0
  const anchor = useUrlQuery().get("anchor") || null; // récupère le paramètre anchor
  const geoloc = useUrlQuery().get("geoloc"); // récupère la valeur de geoloc
  const taxonid = useUrlQuery().get("taxonid"); // récupère l'id du taxon a afficher sur le drawer laterale ( pour le partage de taxon )

  // on affiche le splashScreen que 3 fois
  const sssValue = localStorage.getItem("showSplashScreen") ? parseInt(localStorage.getItem("showSplashScreen")) : 1;
  const [showSplashScreen, setShowSplashScreen] = useState(!(sssValue && sssValue > Config.splashScreen.show));

  const {selectedPlace: city, setPlaceFromId, error} = useMnbGeolocService();

  // contiendra les données pour les catégories
  const [getCategoriesData, {loading: loadingCategories, data: categoriesData}] = useLazyQuery(gqlCategories);
  //contiendra les données pour la météo
  const [getMeteoData, {loading: loadingMeteo, data: meteoData}] = useLazyQuery(gqlMeteo);
  //contiendra les données sur le niveau d'eau
  const [getWaterData, {loading: loadingWaterData, data: waterData}] = useLazyQuery(gqlWater);

  // boolean pour desactiver l'autoscroll a chaque re render
  const [activeAutoScroll, setActiveAutoScroll] = useState(false);

  useEffect(() => {
    if (showSplashScreen) {
      setTimeout(() => {
        setShowSplashScreen(false);
        localStorage.setItem("showSplashScreen", sssValue + 1);
      }, Config.splashScreen.timeout);
    }

    if (geoloc) {
      setPlaceFromId(geoloc);
    }
  }, []);

  useEffect(() => {
    if (city && city.id) {
      fetchData();
    }
  }, [city]);

  // recupère les données de météo et categories pour la nouvelle ville
  async function fetchData() {
    await getCategoriesData({variables: {geonamesId: city.id, taxonId: taxonid}});
    await getMeteoData({variables: {geonamesId: city.id}});
    await getWaterData({
      variables: {
        geonamesId: city.id,
        riverObservationType: _riverObservation.type,
        riverStepUnit: "Day", // Month Week Day Hour
        groundWaterStepUnit: "Day" // Month Week Day Hour
      }
    });
    setActiveAutoScroll(true);
  }

  if (!city?.id || showSplashScreen || loadingCategories || loadingMeteo || loadingWaterData) {
    return <SplashScreen />;
  }

  if (activeAutoScroll && !loadingCategories && !loadingMeteo && !loadingWaterData) {
    // les ancres normales ne marche pas a cause du background absolute ... fix temporaire
    setTimeout(() => {
      if (["meteo", "air", "eau", "sol", "graph", "actualites"].includes(anchor)) {
        goToAnchor(anchor);
        setActiveAutoScroll(false);
      }
    }, 1000);
  }

  return (
    <ThemeProvider theme={theme}>
      <NewsContext.Provider value={{newsContextValue, newsContextSetValue}}>
        <FavoritesSpeciesContextProvider
          contextFSValues={valueFavoritesSpecies}
          contextFSSetValues={handleFavoritesSpecies}>
          <FavoritesNewsContextProvider contextFNValues={valueFavoritesNews} contextFNSetValues={handleFavoritesNews}>
            <Switch>
              <Route exact path="/categorie/:name">
                <Screen
                  categoriesData={categoriesData}
                  meteoData={meteoData?.weather}
                  waterData={waterData}
                  city={city}
                />
              </Route>
              <Route>
                <MainScreen
                  categoriesData={categoriesData}
                  meteoData={meteoData?.weather}
                  waterData={waterData}
                  city={city}
                />
              </Route>
            </Switch>
            <DrawerRouter geonamesId={city.id} meteoData={meteoData?.weather} />
          </FavoritesNewsContextProvider>
        </FavoritesSpeciesContextProvider>
      </NewsContext.Provider>
    </ThemeProvider>
  );
}
