export default {
  splashScreen: {
    show: 3,
    timeout: 3000
  },
  colors: {
    blue: "#002D4B", // "#0d354e",
    blueRGBValue: "13, 53, 78",
    blueWithOpacity: "rgba(13, 53, 78, 0.8)",
    darkBlue: "#0b2d42",
    lightBlue: "#038FF5",
    white: "white",
    whiteWithOpacity: "rgba(255, 255, 255, 0.6)",
    error: "rgba(255,19,19,0.8)"
  },
  categorie: {
    gridSpacing: 1
  },
  imageSize: {
    main: {
      width: 800,
      height: 450
    },
    sub: {
      width: 400, //800,
      height: 275 // 550
    }
  },
  iconSize: {
    desktop: {width: "9vw", maxWidth: "55px"},
    mobile: {width: "6vw", maxWidth: "38px"}
  },
  imageServerUrl: "https://images.mnemotix.com/unsafe",
  containerMaxSize: "lg", // "lg"
  breakpoints: {
    values: {
      xs: 0, // small mobile
      sm: 299, // mobile
      md: 459,
      lg: 920, // reduction 960 a 920 pour matcher la largeur de la maquette avec LG
      xl: 1280
    }
  },
  leftMarginDesktop: "50px",
  leftMarginMobile: "5vw"
};
