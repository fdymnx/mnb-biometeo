# Contact

Tous les commentaires et suggestions constructifs sont les bienvenus pour améliorer cette application web.

Ecrivez-nous par courriel : [biometeo@dordogne.fr](mailto:biometeo@dordogne.fr) 

**NB:** Votre adresse mail ainsi que toute donnée personnelle que vous nous envoyez sur l’adresse [biometeo@dordogne.fr](mailto:biometeo@dordogne.fr) . Elles ne font pas l’objet d’une prise de décision automatisée ou de profilage. Elles ne sont destinées qu’à contribuer à l’amélioration de l'application web BioMétéo.
