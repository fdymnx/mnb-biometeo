/**
 * Pour rajouter des pages static en markdown
 *
 * l'icon doit être en PNG, importée puis passée en paramètre à la propriété icon, il ne faut pas juste passer le nom de l'icon
 */

// import heart from "./heart.png";
import creditsMD from "./credits.md";
import informationsMD from "./informations.md";
import contactMD from "./contact.md";

export default [
  {
    content: contactMD,
    title: "Contact",
    id: "contact",
    icon: false // heart (la variable retournée par l'import, et non pas "heart.png" )
  },
  {
    content: informationsMD,
    title: "Info pratiques",
    id: "infopratiques",
    icon: false // heart (la variable retournée par l'import, et non pas "heart.png" )
  },
  {
    content: creditsMD,
    title: "Crédits",
    id: "credits",
    icon: false
  }
];
