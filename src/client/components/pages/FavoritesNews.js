/*
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from "react";
import {makeStyles} from "@material-ui/core/styles";
import Config from "../../Config";
import clsx from "clsx";

import {Paper} from "@material-ui/core";
import {useMediaQueries} from "../../utilities/responsive";
import globalStyles from "../../globalStyles";
import {useFavoritesNewsContext} from "../../hooks/useFavoritesNewsContext";
import {News, NewsLink} from "../widgets/News/News";

const useStyles = makeStyles((theme) => ({
  ...globalStyles
}));

/***
 * affiche la page avec les News mises en favoris
 */
export default function FavoritesNews({}) {
  const classes = useStyles();

  const {isDesktop} = useMediaQueries();

  const {contextFNValues, contextFNSetValues} = useFavoritesNewsContext();

  return (
    <div>
      <div className={clsx(classes.fs20, classes.drawerPaddingTitle)} />
      <NewsLink isDesktop={isDesktop} />
      <div className={clsx(classes.fs20, classes.drawerPaddingTitle)}>Mes actualités favorites</div>
      {Object.values(contextFNValues).map((item, containerIndex) => (
        <div key={containerIndex}>
          <br />
          <Paper elevation={3}>
            <News addContainer={true} item={item} isDesktop={isDesktop} />
          </Paper>
        </div>
      ))}
      <br /> <br />
    </div>
  );
}
