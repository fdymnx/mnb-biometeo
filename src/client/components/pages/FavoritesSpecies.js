/*
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from "react";
import {Grid} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import Config from "../../Config";
import clsx from "clsx";
import {Specie} from "../widgets/Categorie/Specie";
import {useMediaQueries} from "../../utilities/responsive";
import globalStyles from "../../globalStyles";
import {useFavoritesSpeciesContext} from "../../hooks/useFavoritesSpeciesContext";
import HorizontalSpacer from "../widgets/HorizontalSpacer";

const useStyles = makeStyles((theme) => ({
  ...globalStyles
}));

/***
 * affiche la page avec les espèces mises en favoris
 */
export default function FavoritesSpecies({}) {
  const classes = useStyles();

  const {isDesktop, isMobile, isSmallMobile} = useMediaQueries();

  const {contextFSValues, contextFSSetValues} = useFavoritesSpeciesContext();

  const gridSize = {
    xs: 12,
    sm: !isDesktop ? 12 : 6
  };

  return (
    <div>
      <div className={clsx(classes.fs20, classes.drawerPaddingTitle)}>Mes espèces favorites</div>

      {chunk(Object.values(contextFSValues), 3).map((items, containerIndex) => (
        <div key={containerIndex}>
          <Specie
            item={items[0]}
            mainImage={true}
            isDesktop={isDesktop}
            isMobile={isMobile}
            isSmallMobile={isSmallMobile}
          />

          <HorizontalSpacer />
          <Grid container spacing={1} justify="center" alignItems="center">
            <Grid item {...gridSize}>
              {items?.[1] && (
                <Specie
                  item={items?.[1]}
                  mainImage={false}
                  isDesktop={isDesktop}
                  isMobile={isMobile}
                  isSmallMobile={isSmallMobile}
                />
              )}
            </Grid>
            <Grid item {...gridSize}>
              {items?.[2] && (
                <Specie
                  item={items?.[2]}
                  mainImage={false}
                  isDesktop={isDesktop}
                  isMobile={isMobile}
                  isSmallMobile={isSmallMobile}
                />
              )}
            </Grid>
          </Grid>
          <HorizontalSpacer />
        </div>
      ))}
    </div>
  );
}

// decoupe un tableau en tableau de size elements
function chunk(arr, size) {
  var arrTmp = [];

  for (var i = 0; i < arr.length; i += size) {
    arrTmp.push(arr.slice(i, i + size));
  }
  return arrTmp;
}
