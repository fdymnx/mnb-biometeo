import React from "react";
import {makeStyles} from "@material-ui/core/styles";
import {useParams} from "react-router-dom";

import TopMenu from "../widgets/TopMenu";
import Meteo from "../widgets/Meteo/Meteo";

import {WaterGraph} from "../widgets/Water/WaterGraph";
import {WaterDetails} from "../widgets/Water/WaterDetails";
import NewsReader from "../widgets/News/NewsReader";
import {useUrlQuery} from "../../hooks/useUrlQuery";
import Categorie from "../widgets/Categorie/Categorie";

const useStyles = makeStyles((theme) => ({
  center: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "flex-start"
  }
}));

// https://gitlab.com/cg24/mnb-biometeo/-/blob/7eb8ca4d6d87a7d29474c19b95b1ecce183252fb/src/client/components/widgets/Categorie.js

/***
 * affiche la page principale avec la barre du haut, la météo, catégérories etc
 */
export function Screen({categoriesData, meteoData, waterData, city}) {
  const classes = useStyles();

  let {name} = useParams();
  const portraitmode = !!useUrlQuery().get("portraitmode");

  function renderScreen(name) {
    if (["meteo", "air", "eau", "sol", "actualites", "graph"].includes(name)) {
      switch (name.toLowerCase()) {
        case "meteo":
          return <Meteo meteoData={meteoData} />;
        case "air":
          return (
            <Categorie
              name="air"
              data={categoriesData?.taxonsAir?.edges}
              onlyMe={true}
              bgpos="bottom"
              topChild={null}
              portraitmode={portraitmode}
            />
          );
        case "sol":
          return (
            <Categorie
              name="sol"
              data={categoriesData?.taxonsSol?.edges}
              onlyMe={true}
              portraitmode={portraitmode}
              bgpos="center"
              bottomChild={portraitmode && <WaterDetails city={city} waterData={waterData} />}
            />
          );
        case "eau":
          return (
            <Categorie
              name="eau"
              data={categoriesData?.taxonsEau?.edges}
              onlyMe={true}
              portraitmode={portraitmode}
              bgpos="top"
              bottomChild={portraitmode && <WaterGraph city={city} waterData={waterData} />}
            />
          );
        case "actualites":
          return <NewsReader city={city} />;
        case "graph":
          return <WaterGraph city={city} waterData={waterData} />;
      }
    } else {
      return <div>La categorie {name} est invalide.</div>;
    }
  }

  return (
    <div className={classes.center}>
      <TopMenu compact={true} />
      {renderScreen(name)}
    </div>
  );
}
