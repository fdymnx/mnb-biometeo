/*
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from "react";
import Loader from "react-loader-spinner";
import clsx from "clsx";
import DordogneV from "../../public/icons/dordogneV.svg";
import BiodivH from "../../public/icons/biometeoDesktop.png";
import globalStyles from "../../globalStyles";
import air from "../../public/backgrounds/air.jpg";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  relative: {
    width: "100%",
    height: "100vh",
    position: "relative",
    // background: "url('https://poc-b-i-o-meteo.netlify.com/photos/air.jpg') bottom/cover"
    background: `url(${air}) bottom/cover`
  },
  absoluteTF: {
    position: "absolute",
    top: "15%",
    left: "15%"
  },
  absoluteBR: {
    position: "absolute",
    bottom: "15%",
    right: "15%"
  },
  loadingText: {
    marginLeft: 10,
    fontFamily: "Segoe UI",
    fontSize: "calc(3px + 2vmin)",
    color: "#dcdbdb"
  },
  responsiveIcon: {
    width: "95%",
    height: "auto"
  },
  overlay: {
    backgroundColor: "rgba(13,53,78, 0.6)",
    color: "white",
    height: "100%"
  },
  ...globalStyles
}));

/***
 * affiche la page de chargement du démarrage de l'application
 */
export function SplashScreen(props) {
  const classes = useStyles();

  return (
    <div className={classes.relative}>
      <div className={classes.overlay}>
        <div className={classes.absoluteTF}>
          <img src={BiodivH} className={classes.responsiveIcon} />
          <div className={clsx(classes.marginTop15, classes.flexRow)}>
            <Loader type="Grid" color="white" height={20} width={20} />
            <span className={classes.loadingText}>Chargement des données...</span>
          </div>
        </div>
        <div className={classes.absoluteBR}>
          <img src={DordogneV} className={classes.responsiveIcon} />
        </div>
      </div>
    </div>
  );
}
