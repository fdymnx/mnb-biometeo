/*
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from "react";
import {makeStyles} from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import withWidth from "@material-ui/core/withWidth";
import {useMediaQueries} from "../../utilities/responsive";

const useStyles = makeStyles((theme) => ({
  gridRoot: {
    width: "100%",
    paddingTop: 15,
    paddingBottom: 15
  },
  box: {
    border: "1px solid rgb(212, 212, 212)"
  }
}));

/***
 * Petit composant de test uniquement qui permet de vérifier l'alignement des autres composants en affichant une ligne avec 12 carrés
 */
function AlignCheck(props) {
  const classes = useStyles();
  const {width} = props;
  const {isDesktop, isMobile, isSmallMobile} = useMediaQueries();

  return (
    <Grid
      className={classes.gridRoot}
      container
      spacing={1}
      direction="row"
      justify="flex-start"
      alignItems="flex-start"
      alignContent="center">
      {[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12].map((value) => (
        <Grid key={value} item xs={1} md={1} className={classes.box}>
          <Box display="flex" justifyContent="center" alignItems="center">
            {value === 6 ? (
              <span>
                {width + " "}
                {isMobile ? "m" : isSmallMobile ? "sm" : "d"}
              </span>
            ) : (
              value
            )}
          </Box>
        </Grid>
      ))}
    </Grid>
  );
}

export default withWidth()(AlignCheck);
