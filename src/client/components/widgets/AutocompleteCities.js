/*
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* eslint-disable no-use-before-define */
import React, {useState, useEffect, useRef} from "react";
import useAutocomplete from "@material-ui/lab/useAutocomplete";

import {IconButton, InputBase, Paper, ButtonBase, Button} from "@material-ui/core";
import CircularProgress from "@material-ui/core/CircularProgress";
import useComponentSize from "@rehooks/component-size";
import SearchIcon from "@material-ui/icons/Search";
import RoomIcon from "@material-ui/icons/Room";
import styled from "styled-components";
import {makeStyles} from "@material-ui/core/styles";
import clsx from "clsx";
import Config from "../../Config";
import globalStyles from "../../globalStyles";
import {useMnbGeolocService} from "../../hooks/useMnbGeolocService";
import {useMediaQueries} from "../../utilities/responsive";

const Listbox = styled("div")`
  width: auto;
  list-style: none;
  overflow: auto;
  max-height: 300px;
  padding: 0px 20px 20px 20px;
  margin: 0 20px 0 0;
  z-index: 1;
  scrollbar-color: ${Config.colors.white} ${Config.colors.darkBlue};
  scrollbar-width: thin;
  & div {
    padding: 5px;
    display: flex;
    & span {
      flex-grow: 1;
    }
  }

  & div[aria-selected="true"] {
    background-color: #fafafa;
  }

  & div[data-focus="true"] {
    background-color: ${Config.colors.blue};
    color: ${Config.colors.white};
    cursor: pointer;
  }
`;
const resultBoxStyle = {
  position: "absolute",
  marginTop: 10,
  color: Config.colors.white,
  backgroundColor: Config.colors.darkBlue,
  borderRadius: "30px",
  boxShadow: "0 2px 8px rgba(0, 0, 0, 0.15)",
  zIndex: 100
};

const useStyles = makeStyles((theme) => ({
  listEntry: {
    width: "auto",
    fontSize: 30,
    color: Config.colors.white,
    backgroundColor: Config.colors.darkBlue,
    cursor: "pointer",
    marginTop: "10 ! important",
    padding: 0
  },
  wrapper: {
    width: "100%"
  },
  box: {
    width: "auto",
    paddingLeft: "20px",
    paddingRight: "10px"
  },

  input: {
    width: "100%",
    //marginLeft: theme.spacing(1),
    flex: 1,
    color: Config.colors.white,
    textAlign: "left",
    align: "left",
    "& input": {
      textAlign: "left"
    }
  },
  iconButton: {
    padding: 10,
    paddingLeft: 0,
    color: Config.colors.white
  },
  citiesBox: {
    width: "100%"
  },
  geoButton: {
    width: "auto",
    paddingLeft: 10,
    paddingRight: 10,
    margin: "auto"
  },
  noTransform: {
    textTransform: "none"
  },
  error: {
    textAlign: "center",
    color: Config.colors.error
  },
  ...globalStyles
}));

export default function AutocompleteCities({onValueChange, lastCities} = {}) {
  const classes = useStyles();
  const ref = useRef(null);
  const size = useComponentSize(ref);
  const {width} = size; // input size, not windows size !!
  const [options, setOptions] = useState([]);
  const [selectedOption, setSelectedOption] = useState({name: ""});

  const {isDesktop} = useMediaQueries();

  let {
    getRootProps,
    getInputLabelProps,
    getInputProps,
    getListboxProps,
    getOptionProps,
    groupedOptions,
    getOptionSelected,
    focused,
    setAnchorEl,
    anchorEl,
    inputValue
  } = useAutocomplete({
    value: selectedOption || {},
    options: options,
    getOptionLabel: (option) => {
      // console.log("getOptionLabel", option);
      return option.name || "";
    },
    getOptionSelected: (option, value) => {
      /* console.log({value, option});
      if (option?.id === value?.id || option?.name.toLowerCase() === value?.name.toLowerCase()) {
        console.log(" ");
        console.log(" ");
        console.log("found");
        console.log(" ");
        console.log(" ");
        console.log(" ");
      }*/
      return option?.id === value?.id || option?.name.toLowerCase() === value?.name.toLowerCase();
    },
    selectOnFocus: true
  });

  /**
   * This hook belongs to @mnemotix/synaptix-client-toolkit.
   * It handles :
   *  - Geonames place searching by Query String.
   *  - Geonames place autolocation via browser native functionalities.
   */
  const {
    searchedPlaces, // Returns the list of found places for "qs" search.
    selectedPlace, // Returns the selected place. Either found after "proceedAutoPlace" call, either set after "setPlace" call or initially set with registered value in localStorage `GeolocServiceCurrentPlace` key.
    proceedAutoPlace, // Function to call to get autoplace thanks to browser native geolocation. Found place is saved in localStorage.
    setPlace, // Function to call to set place manually. Place is saved in localStorage.
    loading, // Loading state.
    error // Potential error after location. (Ex: your browser needs autorization)
  } = useMnbGeolocService({
    qs: inputValue
  });

  /**
   * Update autocomplete options after `useGeolocService` hook "searchedPlaces" update.
   */
  useEffect(() => {
    if (searchedPlaces) {
      setOptions(searchedPlaces.map((place) => place));
    }
  }, [searchedPlaces]);

  /**
   * Update autocomplete selected option after `useGeolocService` hook "selectedPlace" update.
   * And close the popup.
   */
  useEffect(() => {
    if (selectedPlace?.id) {
      setSelectedOption(selectedPlace);
      onValueChange(selectedPlace);
      if (anchorEl) {
        anchorEl.blur();
      }
    }
  }, [selectedPlace]);

  const inputProps = getInputProps();
  const handleFocus = inputProps.onFocus;
  inputProps.onFocus = (e) => {
    anchorEl.select();
    handleFocus(e);
  };

  const resultBox = {
    ...resultBoxStyle,
    width: width > 240 ? width - 8 : "85%",
    left: width > 240 ? null : "5%"
  };

  return (
    <div className={classes.wrapper} ref={ref}>
      <div {...getRootProps()} className={classes.box}>
        <InputBase
          inputRef={setAnchorEl}
          className={clsx(classes.input, classes.fs20)}
          placeholder="Localisation"
          inputProps={{"aria-label": "Localisation"}}
          fullWidth={true}
          startAdornment={
            isDesktop && (
              <IconButton type="submit" className={classes.iconButton} aria-label="search">
                <SearchIcon />
              </IconButton>
            )
          }
          endAdornment={
            <React.Fragment>{loading ? <CircularProgress color="inherit" size={20} /> : null}</React.Fragment>
          }
          {...inputProps}
        />
      </div>

      {focused && (
        <Paper elevation={1} variant="outlined" {...getListboxProps()} style={resultBox}>
          <Listbox>
            <ButtonBase
              onClick={proceedAutoPlace}
              className={clsx(classes.listEntry, classes.noTransform, classes.fs20)}>
              <RoomIcon className={classes.iconButton} />
              Me géolocaliser
            </ButtonBase>

            <div className={classes.horizontalBar} />

            {groupedOptions.map((option, index) => (
              <div
                {...getOptionProps({option, index})}
                className={classes.listEntry}
                onClick={() => handleSelectOption(option)}>
                <span className={classes.fs20}>{option.name}</span>
              </div>
            ))}
            {lastCities.map((option, index) => (
              <div key={index} className={classes.listEntry} onClick={() => handleSelectOption(option)}>
                <span className={classes.fs20}>{option.name}</span>
              </div>
            ))}

            <If condition={error}>
              <div className={classes.horizontalBar} />

              <div className={clsx(classes.error, classes.fs20)}>
                <Choose>
                  <When condition={error?.status === "out_of_bbox_error"}>
                    Désolé votre position géographique est en dehors de la Dordogne.
                  </When>
                  <Otherwise>Une erreur est survenue.</Otherwise>
                </Choose>
              </div>
            </If>
          </Listbox>
        </Paper>
      )}
    </div>
  );

  /**
   * @param option
   */
  function handleSelectOption(option) {
    /**
     * When selecting an option, first manually set place within `useGeolocService` hook.
     * Effects will handle the rest.
     */
    setPlace(option);
  }
}
