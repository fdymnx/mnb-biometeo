/*
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from "react";

import {Container, Grid} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import clsx from "clsx";

import Config from "../../../Config";
import globalStyles from "../../../globalStyles";
import {upperFirstLetter} from "../../../utilities/tools";
import {useMediaQueries} from "../../../utilities/responsive";

import {Specie} from "./Specie";

import HorizontalSpacer from "../HorizontalSpacer";
import air from "../../../public/backgrounds/air_m.jpg";
import eau from "../../../public/backgrounds/eau_m.jpg";
import sol from "../../../public/backgrounds/sol_m.jpg";
const bgFiles = {air, eau, sol};

const useStyles = makeStyles((theme) => ({
  relativeContainer: {
    color: Config.colors.blue,
    width: "100%",
    position: "relative"
  },
  height90: {
    height: "95vh"
  },
  absoluteContainer: {
    width: "100%",
    position: "absolute",
    left: 0,
    right: 0
  },
  overlay: {
    zIndex: 10,
    backgroundColor: Config.colors.blueWithOpacity,
    color: "white",
    position: "relative",
    height: "100%"
  },
  container: {
    width: "100%",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    paddingBottom: "25px"
  },
  mozaikContainer: {
    width: "100%",
    height: "auto",
    margin: "auto"
  },
  padTB: {
    paddingTop: 50,
    paddingBottom: 25
  },
  pB40: {
    paddingBottom: 40
  },

  ...globalStyles
}));

/**
 * affiche une catégorie air eau sol avec la mozaic de 3 images et d'autres informations
 *
 * @param string name : nom de la catégorie : "air", "eau", "sol"
 * @param object data : données à afficher
 * @param string bgpos : position du background : "bottom", "top", "center"
 * @param topChild : composant enfant à afficher au dessus de la mozaic (comme la météo pour Air, pour avoir le fond de l'air sous la météo)
 * @param bottomChild : composant enfant à afficher en dessous de la mozaic  (idem pour avoir le fond)
 * @param onlyMe : Affiche la catégorie seul, pour la route "/categorie/:name" ou il faut que tout rentre sur un écran fullhd. on ne met pas de padding top, bottom etc et il faut que ca prenne plein écran
 * @param portraitmode : pour un affichage en mode portrait, il faut que le background de la categorie fasse la hauteur de tt la page car une seul catégorie est affichée à l'écran
 */
export default function Categorie({
  name,
  data,
  bgpos,
  noPaddingBottom,
  noPaddingTop,
  onlyMe,
  topChild,
  bottomChild,
  portraitmode
}) {
  const classes = useStyles();

  const {isDesktop, isMobile, isSmallMobile} = useMediaQueries();

  const gridSize = {
    xs: 12,
    sm: !isDesktop ? 12 : 6
  };

  const maskValue = `linear-gradient(rgba(255, 255, 255, 0) ,#fff ${
    name === "air" ? "150px" : "70px"
  }  calc(100% - 50px),rgba(255, 255, 255, 0))`;

  const style = {
    top: noPaddingTop || onlyMe ? "0px" : "-50px",
    bottom: noPaddingBottom || onlyMe ? "0px" : "-50px",
    background: `url(${bgFiles[name]}) ${bgpos || "center"}/cover`,
    WebkitMask: maskValue,
    mask: maskValue
  };

  return (
    <div className={clsx(classes.relativeContainer, !!onlyMe && classes.height90)}>
      <div className={classes.absoluteContainer} style={style}></div>
      <div className={classes.overlay}>
        <div className={classes.container}>
          {topChild}

          <Container
            maxWidth={Config.containerMaxSize}
            className={clsx(!onlyMe && classes.padTB, classes.mozaikContainer)}>
            <div id={name} className={clsx(classes.fs40, classes.segoeSemiBold, classes.pB40)}>
              {upperFirstLetter(name)}
            </div>
            {data ? (
              <>
                <Specie
                  item={data[0]?.node}
                  mainImage={true}
                  isDesktop={isDesktop}
                  isMobile={isMobile}
                  isSmallMobile={isSmallMobile}
                />

                <HorizontalSpacer />
                <Grid container spacing={Config.categorie.gridSpacing} justify="center" alignItems="center">
                  <Grid item {...gridSize}>
                    <Specie
                      item={data[1]?.node}
                      mainImage={false}
                      isDesktop={isDesktop}
                      isMobile={isMobile}
                      isSmallMobile={isSmallMobile}
                    />
                  </Grid>
                  <Grid item {...gridSize}>
                    <Specie
                      item={data[2]?.node}
                      mainImage={false}
                      isDesktop={isDesktop}
                      isMobile={isMobile}
                      isSmallMobile={isSmallMobile}
                    />
                  </Grid>
                </Grid>
                <HorizontalSpacer />

                {!!onlyMe && <br />}
              </>
            ) : (
              <Container id={name} maxWidth={Config.containerMaxSize} className={classes.mozaikContainer}>
                <span className={classes.fs21}>Une erreur est survenue lors du chargement des données...</span>
              </Container>
            )}
            {bottomChild}
          </Container>
        </div>
      </div>
    </div>
  );
}

import {oneOf, bool, object} from "prop-types";
Categorie.propTypes = {
  name: oneOf(["air", "eau", "sol"]).isRequired,
  bgpos: oneOf(["bottom", "top", "center"]),
  onlyMe: bool,
  topChild: object,
  bottomChild: object
};
