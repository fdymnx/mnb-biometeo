import React, {useState} from "react";

import {Grid, Button, Modal} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import clsx from "clsx";

import {createTaxonDetailsUrl} from "../../../utilities/tools";
import Config from "../../../Config";
import globalStyles from "../../../globalStyles";
import {
  EmailIcon,
  EmailShareButton,
  FacebookShareButton,
  FacebookIcon,
  TwitterShareButton,
  TwitterIcon
} from "react-share";

const useStyles = makeStyles((theme) => ({
  pTB: {
    marginTop: 10,
    marginBottom: 10,
    cursor: "pointer"
  },
  pL8: {
    paddingLeft: "15px"
  },
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    boxShadow: "none"
  },
  content: {
    padding: 25,
    backgroundColor: Config.colors.white,
    color: Config.colors.blue,
    minWidth: 200,
    maxWidth: 500,
    textShadow: "none",
    lineHeight: "normal",
    fontWeight: "normal",
    boxShadow: "none"
  },
  ...globalStyles
}));

// view pour le partage sur les réseau sociaux et mail
export function ShareOnSocial({item, children}) {
  const classes = useStyles();

  const [open, setOpen] = useState(false);

  const handleClose = () => {
    setOpen(false);
  };

  const handleOpen = () => {
    setOpen(true);
  };

  let specieLabel = concatVernLabel(item);

  // configuration pour les textes et lien utilisés pour le partage
  const configText = {
    // utilisé par fb,twitter et email
    title: `J'ai découvert cette espèce sur l’application BioMétéo: ${specieLabel}, viens visiter !`,
    // utilisé par fb,twitter et email
    shareUrl: createTaxonDetailsUrl(item.id, true, false),
    // spécifique à l'email, l'usages des `` permet le saut de ligne et alinéa,
    // laisser qlq lignes vide car pour le mail car shareUrl est concaténé directement à la suite par le composant <EmailShareButton />
    // pas possible d'y mettre de l'html.
    email: {
      body: `
      
      J'ai découvert cette espèce sur l’application BioMétéo: ${specieLabel}, viens visiter !
      

`
    },
    // spécifique à facebook
    fb: {
      hashtag: "biométéo" // 1 seul hashtag possible
    },
    // spécifique à twitter
    tw: {
      hashtags: ["biométéo", "biodiversité", "dordogne"], // fournir un tableau , avec 0 ou plus de hashtag
      related: ["biométéo"]
    }
  };

  function renderItem(icon, text) {
    return (
      <Grid container direction="row" justify="flex-start" alignItems="stretch">
        <Grid item className={classes.flexCenter}>
          {icon}
        </Grid>
        <Grid item className={clsx(classes.fs19, classes.segoeSemiBold, classes.flexCenter, classes.pL8)}>
          {text}
        </Grid>
      </Grid>
    );
  }

  return (
    <>
      <Button onClick={handleOpen} className={classes.buttonMinWidth}>
        {children}
      </Button>
      <Modal open={open} onClose={handleClose} className={classes.modal}>
        <div className={classes.content}>
          <div className={clsx(classes.fs16, classes.segoeSemiBold)}> PARTAGER SUR</div>
          <br />
          <div className={classes.horizontalBarBlue} />
          <br />
          <div>
            <FacebookShareButton
              beforeOnClick={handleClose}
              url={configText.shareUrl}
              quote={configText.title}
              hashtag={configText.fb.hashtag}
              className={classes.pTB}>
              {renderItem(<FacebookIcon size={40} round />, "Facebook")}
            </FacebookShareButton>
          </div>
          <div>
            <TwitterShareButton
              beforeOnClick={handleClose}
              url={configText.shareUrl}
              title={configText.title}
              hashtags={configText.tw.hashtags}
              related={configText.tw.related}
              className={classes.pTB}>
              {renderItem(<TwitterIcon size={40} round />, "Twitter")}
            </TwitterShareButton>
          </div>
          <div>
            <EmailShareButton
              beforeOnClick={handleClose}
              url={configText.shareUrl}
              subject={configText.title}
              body={configText.email.body}
              className={classes.pTB}>
              {renderItem(<EmailIcon size={40} round />, "Email")}
            </EmailShareButton>
          </div>
        </div>
      </Modal>
    </>
  );
}

/**
 * prend un item contenant un attribut vernacularName et label et les concatene
 * @param {*} item
 */
function concatVernLabel(item) {
  let specieLabel = "";
  if (item?.vernacularName && item?.label) {
    specieLabel = item?.vernacularName + " (" + item.label + ")";
  } else if (!item?.vernacularName && item?.label) {
    specieLabel = item.label;
  } else if (item?.vernacularName && !item?.label) {
    specieLabel = item?.vernacularName;
  }

  return specieLabel;
}
