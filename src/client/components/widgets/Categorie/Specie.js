import React, {useRef, useState, useEffect} from "react";
import debounce from "debounce";
import {makeStyles} from "@material-ui/core/styles";
import clsx from "clsx";

import {useHistory} from "react-router-dom";
import FitText from "@kennethormandy/react-fittext";
import Config from "../../../Config";
import globalStyles from "../../../globalStyles";
import {cleanTextToDisplay, createTaxonDetailsUrl} from "../../../utilities/tools";
import {ShareOnSocial} from "./ShareOnSocial";
import {FavoriteHeartIcon} from "../FavoriteHeartIcon";
import {useFavoritesSpeciesContext} from "../../../hooks/useFavoritesSpeciesContext";
import {MyIcon} from "../MyIcons";
import sharebluePng from "../../../public/icons/share-blue.png";

import useDimensions from "react-use-dimensions";

const useStyles = makeStyles((theme) => ({
  relativeItemBox: {
    position: "relative",
    color: "white",
    width: "100%"
  },
  absoluteItemBoxBottom: {
    position: "absolute",
    paddingTop: "15px",
    width: "calc(100% - min(4vw,30px));",
    bottom: "0px",
    left: "0px",
    background: `linear-gradient(rgba(255, 255, 255, 0) 0% ,  rgba(255, 255, 255, 0) 2% , rgba(255, 255, 255, 0) 4%,rgba(${Config.colors.blueRGBValue},0.05) 8%, rgba(${Config.colors.blueRGBValue},0.8) 100% )`
  },
  absoluteItemBoxBottomPad: {
    // paddingLeft: "min(4vw,30px)",
    // paddingBottom: "min(4vw,30px)"
    paddingLeft: "2vw",
    paddingBottom: "2vw"
  },
  absoluteItemBoxBottomPadMob: {
    paddingLeft: "30px",
    paddingLeft: "4vw",
    paddingBottom: "4vw"
  },

  absoluteItemBoxTop: {
    position: "absolute",
    top: `calc(${Config.leftMarginDesktop} - 30px)`,
    right: `calc(${Config.leftMarginDesktop} - 30px)`
  },
  absoluteItemBoxTopMob: {
    top: "2vw",
    right: `2vw`
  },

  textWhite: {
    color: Config.colors.white
  },
  ...globalStyles
}));

/**
 * affiche une image avec les infos par dessus
 * @param {*} item
 * @param {bool} mainImage si image pricipale qui doit être en grand, et le texte est plus gros en mode mobile
 * @param {bool} isDesktop
 * @param {bool} isMobile
 * @param {bool} isSmallMobile
 */
export function Specie({item, mainImage, isDesktop, isMobile, isSmallMobile}) {
  let history = useHistory();

  function openDetails() {
    history.push(createTaxonDetailsUrl(item.id, false, true));
  }

  // context favoritesSpecies
  const {contextFSValues, contextFSSetValues} = useFavoritesSpeciesContext();

  const classes = useStyles();
  /**
   * si c'est desktop on affiche une mozaic avec une image en grand au dessus et deux petites en dessous
   * le ration n'est pas le même également
   *
   * si c'est en mobile on utilise le même ratio
   */
  const imageRatio = isDesktop && mainImage ? Config.imageSize.main : Config.imageSize.sub;
  let url = `${Config.imageServerUrl}/${imageRatio.width + "x" + imageRatio.height}/${
    item?.pictures?.edges?.[0]?.node?.id
  }`;
  // taille des icons coeur et partage
  const iconSize = !isDesktop || mainImage ? Config.iconSize.desktop : Config.iconSize.mobile;
  /* récupère la largeur initial, 
    en utilisant simplement useComponentSize cela crée une erreur sous mozilla lorsqu'on change la taille de la fenetre petit a petit
    j'utilise donc useDimensions pour récupérer la largeur initale de la fenetre pour calculer la hauteur de l'image en proportion
    et useEffect pour récupérer la largeur lors du changement de taille
    il faut 2 ref pour cela, une pour chaque sinon avec une seul useDimensions empeche le listener de fonctionner
  */
  const ref2 = useRef(null);
  const [ref, {width: initialWidth}] = useDimensions({liveMeasure: false});

  const [newWidth, setNewWidth] = useState(false);

  const updateNewWidth = debounce(() => {
    if (ref2.current) {
      setNewWidth(ref2.current.offsetWidth);
    }
  }, 100);

  useEffect(() => {
    window.addEventListener("resize", updateNewWidth);
    return () => {
      window.removeEventListener("resize", updateNewWidth);
    };
  }, []);

  // url = Math.random() >= 0.5 ? url + "s" : url;
  // permet de garder le ratio même si l'image ne ce charge pas ou mauvaise url
  let divSizeWithRatio = {};
  let divWidth = {};
  if (initialWidth && !newWidth) {
    divSizeWithRatio = {
      width: "100%", //initialWidth
      height: (initialWidth * imageRatio.height) / imageRatio.width
    };
    divWidth = {
      width: "100%" //initialWidth
    };
  } else if (newWidth) {
    divSizeWithRatio = {
      width: newWidth,
      height: (newWidth * imageRatio.height) / imageRatio.width
    };

    divWidth = {
      width: newWidth
    };
  }
  return (
    <div className={clsx(classes.textWhite, classes.relativeItemBox)} ref={ref}>
      <div ref={ref2} className={classes.fullWidthNoMP}>
        <div style={divSizeWithRatio}>
          <div className={clsx(classes.cursor, classes.responsiveImg)} onClick={() => openDetails()}>
            <img
              className={classes.responsiveImg}
              src={url}
              alt={null}
              onError={(e) => {
                e.target.onerror = null;
                // e.target.src =
              }}
            />
          </div>

          <div className={clsx(classes.absoluteItemBoxTop, !isDesktop && classes.absoluteItemBoxTopMob)}>
            <div className={clsx(classes.flexRowEnd, classes.zIndex2)}>
              {!(isSmallMobile && !mainImage) && (
                <ShareOnSocial item={item}>{MyIcon(sharebluePng, iconSize.width, iconSize.maxWidth)} </ShareOnSocial>
              )}
              <FavoriteHeartIcon
                iconWidth={iconSize.width}
                iconMaxWidth={iconSize.maxWidth}
                item={item}
                contextValues={contextFSValues}
                contextSetValues={contextFSSetValues}
              />
            </div>
          </div>
          <div
            className={clsx(classes.cursor, classes.absoluteItemBoxBottom)}
            style={divWidth}
            onClick={() => openDetails()}>
            <div className={clsx(classes.absoluteItemBoxBottomPad, !isDesktop && classes.absoluteItemBoxBottomPadMob)}>
              <FitText compressor={1} minFontSize={20} maxFontSize={35}>
                <span className={clsx(classes.segoeItalic)}>
                  {cleanTextToDisplay(item?.vernacularName || item?.label)}
                </span>
              </FitText>
              <div className={clsx(classes.marginTop15, classes.flexRow)}>
                {mainImage && isDesktop && (
                  <>
                    {/* fsMax14 */}
                    <div className={clsx(classes.segoeItalic, classes.fs17)}>
                      {cleanTextToDisplay(item?.bioGeographicalStatus, true, true)}
                    </div>

                    <div className={classes.verticalSpacer} />
                    <div className={classes.verticalBar} />
                    <div className={classes.verticalSpacer} />
                  </>
                )}
                <div className={classes.fs16}>
                  observée
                  <span className={clsx(classes.segoeSemiBold)}>
                    {" " + cleanTextToDisplay(item?.occurrencesCount || 0, true, true)}&nbsp;fois
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
