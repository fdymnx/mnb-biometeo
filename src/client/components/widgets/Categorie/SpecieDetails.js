/*
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from "react";
import {withStyles, makeStyles} from "@material-ui/core/styles";
import {cleanTextToDisplay, metresOrKm} from "../../../utilities/tools";
import {ButtonBase, Tooltip, Grid} from "@material-ui/core";
import FitText from "@kennethormandy/react-fittext";
import {ShareOnSocial} from "./ShareOnSocial";
import {MyIcon} from "../MyIcons";
import {FavoriteHeartIcon} from "../FavoriteHeartIcon";
import {useFavoritesSpeciesContext} from "../../../hooks/useFavoritesSpeciesContext";
import shareblueborderPng from "../../../public/icons/share-blueborder.png";
import BookIcon from "@material-ui/icons/Book";
import VisibilityIcon from "@material-ui/icons/Visibility";
import LocationOnIcon from "@material-ui/icons/LocationOn";

import clsx from "clsx";
import Config from "../../../Config";
import globalStyles from "../../../globalStyles";

const useStyles = makeStyles((theme) => ({
  relativeItemBox: {
    minHeight: "200px",
    minHeight: "min(260px,30vh)",
    position: "relative",
    color: "white"
  },
  absoluteItemBoxTop: {
    position: "absolute",
    transform: "translateY(-50%)",
    top: "0",
    left: `calc(${Config.leftMarginDesktop} - 30px)`,
    textShadow: `0 0 2px ${Config.colors.blue}`
  },
  absoluteItemBoxTopMobile: {
    left: `calc(${Config.leftMarginMobile} - 15px)`
  },
  absoluteItemBoxBottom: {
    position: "absolute",
    paddingTop: "55px",
    width: `calc(100% - ${Config.leftMarginDesktop} )`, //enlever du with autant que le paddingLeft
    paddingLeft: Config.leftMarginDesktop,
    paddingBottom: "40px",
    bottom: "4px",
    left: "0px",
    textShadow: `0 0 2px ${Config.colors.blue}`,
    background: `linear-gradient( rgba(255, 255, 255, 0) 0% , rgba(255, 255, 255, 0) 10%,rgba(${Config.colors.blueRGBValue},0.05) 25%, rgba(${Config.colors.blueRGBValue},0.8) 100% )`
  },
  absoluteItemBoxBottomMobile: {
    width: `calc(100% - ${Config.leftMarginMobile} )`, //enlever du with autant que le paddingLeft
    paddingLeft: Config.leftMarginMobile,
    paddingBottom: "10px"
  },
  paddingHoriz: {
    padding: Config.leftMarginDesktop
  },
  paddingHorizMobile: {
    padding: Config.leftMarginMobile
  },
  marginTaxoData: {
    // paddingTop: "50px",
    paddingBottom: "50px"
  },
  mBField: {
    marginBottom: 30
  },
  iconPadding: {paddingRight: "10px"},
  mBLabel: {marginBottom: 10},
  button: {
    padding: theme.spacing(4)
  },

  maxImgSize: {
    width: "100%",
    minHeight: "20vh",
    maxHeight: "70vh",
    objectFit: "cover"
  },
  whiteColor: {color: Config.colors.white},
  p5: {padding: "5px"},
  ...globalStyles
}));

/**
 * Affiche le détéails d'une espèce avec l'image et la taxonomie en dessous
 * utilisé pour la panneau latérale
 * @param {*} param0
 */
export function SpecieDetails({item, isDesktop}) {
  const classes = useStyles();
  // context favoritesSpecies
  const {contextFSValues, contextFSSetValues} = useFavoritesSpeciesContext();
  const url = `${Config.imageServerUrl}/${item?.pictures?.edges?.[0]?.node?.id}`;

  function renderField(label, value) {
    return (
      <div className={classes.mBField}>
        <div className={classes.mBLabel}>{label}</div>
        <div className={classes.segoeSemiBold}>{value}</div>
      </div>
    );
  }

  if (!item) {
    return null;
  }

  const detailsUrl = item.gbifTaxonKey
    ? `https://www.gbif.org/fr/occurrence/map?has_coordinate=true&has_geospatial_issue=false&year=*,*&taxon_key=${item.gbifTaxonKey}&geometry=POLYGON((0.62974%2045.71457,0.30267%2045.45906,0.2666%2045.29775,-0.01785%2045.16922,0.07329%2045.07012,-0.03421%2044.85211,0.28515%2044.86456,0.35459%2044.65481,0.79772%2044.70177,0.8425%2044.60096,1.10321%2044.57173,1.44193%2044.87758,1.41289%2045.12509,1.22781%2045.20603,1.32279%2045.38266,1.02333%2045.60893,0.8115%2045.57587,0.62974%2045.71457))`
    : item.page;

  return (
    <>
      <div className={classes.relativeItemBox}>
        <img
          className={clsx(classes.maxImgSize, classes.responsiveImg)}
          src={url}
          alt={null}
          onError={(e) => {
            e.target.onerror = null; // e.target.src =
          }}
        />

        <div className={clsx(classes.absoluteItemBoxTop, !isDesktop && classes.absoluteItemBoxTopMobile)}>
          <div className={clsx(classes.flexRowEnd, classes.zIndex2)}>
            <ShareOnSocial item={item}>
              {MyIcon(shareblueborderPng, Config.iconSize.desktop.width, Config.iconSize.desktop.maxWidth)}
            </ShareOnSocial>
            <FavoriteHeartIcon
              iconWidth={Config.iconSize.desktop.width}
              iconMaxWidth={Config.iconSize.desktop.maxWidth}
              item={item}
              withBorder={true}
              contextValues={contextFSValues}
              contextSetValues={contextFSSetValues}
            />
          </div>
        </div>

        <div className={clsx(classes.absoluteItemBoxBottom, !isDesktop && classes.absoluteItemBoxBottomMobile)}>
          {isDesktop && (
            <FitText compressor={1} minFontSize={20} maxFontSize={57}>
              <span className={clsx(classes.segoeItalic)}>
                {cleanTextToDisplay(item?.vernacularName || item?.label)}
              </span>
            </FitText>
          )}

          <div className={clsx(classes.marginTop15, classes.flexRow)}>
            {isDesktop && (
              <>
                <WhiteTooltip enterTouchDelay={10} title="Statut de présence de l'espèce">
                  <div className={classes.flexRow}>
                    <div className={classes.iconPadding}>
                      <BookIcon className={classes.whiteColor} />
                    </div>
                    <div className={clsx(classes.segoeItalic, classes.fs17)}>
                      {cleanTextToDisplay(item?.bioGeographicalStatus, true, true)}
                    </div>
                  </div>
                </WhiteTooltip>

                <div className={classes.verticalSpacer} />
                <div className={classes.verticalBar} />
                <div className={classes.verticalSpacer} />
              </>
            )}

            <WhiteTooltip
              enterTouchDelay={10}
              title={`Cette espèce a été observée ${item?.occurrencesCount || 0} fois dans les limites de la Dordogne`}>
              <div className={classes.flexRow}>
                <div className={classes.iconPadding}>
                  <VisibilityIcon className={classes.whiteColor} />
                </div>

                <div className={classes.fs17}>
                  {isDesktop && <span>observée </span>}
                  <span className={clsx(classes.segoeSemiBold)}>{item?.occurrencesCount || 0}&nbsp;fois</span>
                </div>
              </div>
            </WhiteTooltip>

            {item?.closestOccurrence && (
              <>
                <div className={classes.verticalSpacer} />
                <div className={classes.verticalBar} />
                <div className={classes.verticalSpacer} />

                <WhiteTooltip
                  enterTouchDelay={10}
                  title={`Cette espèce a été observée à ${metresOrKm(
                    item?.closestOccurrence || 0
                  )} au plus près de votre emplacement actuel`}>
                  <div className={classes.flexRow}>
                    <div className={classes.iconPadding}>
                      <LocationOnIcon className={classes.whiteColor} />
                    </div>

                    <div className={classes.fs17}>
                      {isDesktop && <span>observée à </span>}
                      <span className={clsx(classes.segoeSemiBold)}>{metresOrKm(item?.closestOccurrence || 0)}</span>
                    </div>
                  </div>
                </WhiteTooltip>
              </>
            )}
          </div>
        </div>
      </div>

      <div className={clsx(classes.paddingHoriz, !isDesktop && classes.paddingHorizMobile)}>
        <div className={clsx(classes.marginTaxoData, classes.fs19)}>
          <Grid container direction="row" justify="space-between" alignItems="flex-start">
            <Grid item xs={12} md={5}>
              {renderField("Nom", item["vernacularName"])}
              {renderField("Nom scientifique", item["label"])}
              {renderField("Famille", item["familyName"])}
              {renderField("Ordre", item["orderName"])}
            </Grid>
            <Grid item xs={12} md={5}>
              {renderField("Classe", item["className"])}
              {renderField("Habitats", (item["habitats"] || []).join(", "))}
              {renderField("Statut", item?.bioGeographicalStatus)}
              {renderField(
                "Copyright image",
                `${item?.pictures?.edges[0]?.node.copyright} - ${item?.pictures?.edges[0]?.node.license}`
              )}
            </Grid>
          </Grid>
        </div>

        <Grid container direction="row" justify="space-between" alignItems="center">
          <Grid item xs={12} md={5} className={classes.p5}>
            <ButtonBase className={classes.width100}>
              <a
                target="_blank"
                href={replaceEnFr(item.homepage)}
                className={clsx(classes.observationBlueButton, classes.fs19, classes.flexRowSB)}>
                Plus d'informations
                <svg fill="white" stroke="white" width="25px" height="25px" viewBox="0 0 25 25">
                  <path d="M21.36 21.36H5.62V5.62h7.87V3.373H5.62a2.25 2.25 0 00-2.248 2.248v15.74a2.25 2.25 0 002.248 2.248h15.74a2.255 2.255 0 002.248-2.248v-7.87H21.36zM15.74 3.373V5.62h4.034L8.724 16.673l1.585 1.585 11.05-11.05v4.036h2.248v-7.87z"></path>
                </svg>
              </a>
            </ButtonBase>
          </Grid>
          <Grid item xs={12} md={7} className={classes.p5}>
            <ButtonBase className={classes.width100}>
              <a
                target="_blank"
                href={detailsUrl}
                className={clsx(classes.observationWhiteButton, classes.fs19, classes.flexRowSB)}>
                Détails sur les données d'observation
                <svg
                  fill={Config.colors.blue}
                  stroke={Config.colors.blue}
                  width="25px"
                  height="25px"
                  viewBox="0 0 25 25">
                  <path d="M21.36 21.36H5.62V5.62h7.87V3.373H5.62a2.25 2.25 0 00-2.248 2.248v15.74a2.25 2.25 0 002.248 2.248h15.74a2.255 2.255 0 002.248-2.248v-7.87H21.36zM15.74 3.373V5.62h4.034L8.724 16.673l1.585 1.585 11.05-11.05v4.036h2.248v-7.87z"></path>
                </svg>
              </a>
            </ButtonBase>
          </Grid>
        </Grid>
      </div>
    </>
  );
}

function replaceEnFr(url) {
  const preffix = "https://inpn.mnhn.fr/";
  const suffixEn = "?lg=en";
  const suffixFr = "?lg=fr";
  if (url.startsWith(preffix) && url.endsWith(suffixEn)) {
    return url.replace(suffixEn, suffixFr);
  } else {
    return url;
  }
}

export const WhiteTooltip = withStyles((theme) => ({
  tooltip: {
    backgroundColor: Config.colors.white,
    color: Config.colors.blue,
    boxShadow: theme.shadows[1],
    fontSize: 20,
    padding: 10
  }
}))(Tooltip);
