import gql from "graphql-tag";
export const gqlTaxonFragment = gql`
  fragment TaxonFragment on Taxon {
    gbifTaxonKey
    id
    area
    label
    pictures(first: 1) {
      edges {
        node {
          id
          copyright
          license
        }
      }
    }
    familyName
    vernacularName
    orderName
    bioGeographicalStatus
    habitats
    className
    homepage
    page
    occurrencesCount
    closestOccurrence(geonamesId: $geonamesId)
  }
`;

export const gqlCategories = gql`
  query Taxon($geonamesId: ID!, $taxonId: ID) {
    taxonsSol: taxons(geonamesId: $geonamesId, area: Sol, taxonId: $taxonId) {
      edges {
        node {
          ...TaxonFragment
        }
      }
    }
    taxonsAir: taxons(geonamesId: $geonamesId, area: Air, taxonId: $taxonId) {
      edges {
        node {
          ...TaxonFragment
        }
      }
    }
    taxonsEau: taxons(geonamesId: $geonamesId, area: Eau, taxonId: $taxonId) {
      edges {
        node {
          ...TaxonFragment
        }
      }
    }
  }
  ${gqlTaxonFragment}
`;
