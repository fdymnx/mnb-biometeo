/*
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from "react";
import PropTypes from "prop-types";
import {makeStyles} from "@material-ui/core/styles";
import clsx from "clsx";
import ReactMarkdown from "react-markdown";
import Config from "../../Config";
import globalStyles from "../../globalStyles";

DisplayMarkDownPage.propTypes = {
  title: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired
};

const useStyles = makeStyles((theme) => ({
  main: {
    margin: 15,
    color: Config.colors.blue
  },
  ...globalStyles
}));

export function DisplayMarkDownPage(props) {
  const {title, content} = props;
  const classes = useStyles();
  return (
    <div className={clsx(classes.main, classes.fs17)}>
      <ReactMarkdown source={content} linkTarget="_blank" />
    </div>
  );
}
