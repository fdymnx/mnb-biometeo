import React, {useState} from "react";

import {Button} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import heartbluePng from "../../public/icons/heart-blue.png";
import heartwhitePng from "../../public/icons/heart-white.png";
import heartblueborderPng from "../../public/icons/heart-blueborder.png";
import heartwhiteborderPng from "../../public/icons/heart-whiteborder.png";
import globalStyles from "../../globalStyles";
import {MyIcon} from "./MyIcons";
import Config from "../../Config";

const useStyles = makeStyles((theme) => ({
  ...globalStyles
}));

/**
 *  affiche l'icon du coeur avec un bouton pour rajouter dans les favoris l'item courant
 lit directement si l'id de l'item est dans les favoris et affiche icon bleu ou blanche
 coeur blanc & contour bleu : favori, coeur bleu et contour blanc : pas en favori
 * @param {*} param0 
 */
export function FavoriteHeartIcon({item, iconWidth, iconMaxWidth, withBorder, contextValues, contextSetValues}) {
  const classes = useStyles();

  const isFav = !!contextValues[item?.id];
  const icon = isFav
    ? withBorder
      ? heartwhiteborderPng
      : heartwhitePng
    : withBorder
    ? heartblueborderPng
    : heartbluePng;

  function handleClick() {
    const newFS = {...contextValues};
    if (isFav) {
      delete newFS[item?.id];
      //      setIsFav(false);
    } else {
      newFS[item?.id] = item;
      //    setIsFav(true);
    }
    contextSetValues(newFS);
  }

  return (
    <Button className={classes.buttonMinWidth} onClick={handleClick}>
      {MyIcon(icon, iconWidth, iconMaxWidth)}
    </Button>
  );
}
