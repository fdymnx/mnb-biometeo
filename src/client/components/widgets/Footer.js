/*
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from "react";
import {Grid} from "@material-ui/core";
import DordogneV from "../../public/icons/dordogneV.svg";
import {makeStyles} from "@material-ui/core/styles";
import clsx from "clsx";
import {UnstyledLink} from "./styledComponents/UnstyledLink";

const useStyles = makeStyles((theme) => ({
  maxHeight: {
    height: "125px"
  },
  link: {
    fontFamily: "Segoe UI regular !important",
    fontSize: "17px"
  },
  unstyledLink: {
    textDecoration: "none",
    color: "white",
    "&:focus": {textDecoration: "none"},
    "&:hover": {textDecoration: "none"},
    "&:visited": {textDecoration: "none"},
    "&:link": {textDecoration: "none"},
    "&:active": {textDecoration: "none"}
  }
}));

/***
 * affiche le logo de la dordogne en bas à gauche
 */

export default function Footer(props) {
  const classes = useStyles();

  return (
    <>
      <br />
      <Grid container direction="row" justify="space-between" alignItems="flex-end">
        <Grid item xs={8} lg={10}>
          <Grid container direction="row" justify="center" alignItems="flex-end" spacing={2}>
            <Grid item xs={11} lg={3}>
              <UnstyledLink to="/especesfavorites" className={classes.link}>
                Espèces favorites
              </UnstyledLink>
            </Grid>
            <Grid item xs={11} lg={3}>
              <UnstyledLink to="/actualitesfavorites" className={classes.link}>
                Actualités favorites
              </UnstyledLink>
            </Grid>
            <Grid item xs={11} lg={3}>
              <UnstyledLink to="/infopratiques" className={classes.link}>
                Infos pratiques
              </UnstyledLink>
            </Grid>
            <Grid item xs={11} lg={3}>
              <UnstyledLink to="/credits" className={classes.link}>
                Crédits
              </UnstyledLink>
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={4} lg={2}>
          <Grid container direction="row" justify="center" alignItems="flex-end">
            <a target="_blank" href="https://mnb.dordogne.fr/" className={classes.unstyledLink}>
              <img src={DordogneV} className={clsx(classes.responsiveIcon, classes.maxHeight)} />
            </a>
          </Grid>
        </Grid>
      </Grid>

      <br />
    </>
  );
}
