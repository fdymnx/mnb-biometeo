import React from "react";
import {StateProvider, ObjectWatcher} from "reenhance-components";

const LoadedState = StateProvider(false);
const ImageRefWatcher = ObjectWatcher(React.createRef());

////   BUGGUé !!!

// <ImageWithLoading key={id} src={'https://picsum.photos/100/100/?image=' + id} />

export function ImageWithLoading({src, className}) {
  return (
    <LoadedState>
      {({state: loaded, setState: setLoaded}) => (
        <ImageRefWatcher watch="current">
          {(imageRef) => {
            const complete = imageRef.current && imageRef.current.complete;
            return (
              <div>
                {!complete ? (
                  <div
                    style={{
                      width: "100%",
                      height: "100%"
                    }}></div>
                ) : null}
                <img
                  src={src}
                  className={className}
                  style={!complete ? {visibility: "hidden"} : {}}
                  ref={imageRef}
                  onLoad={() => setLoaded(true)}
                />
              </div>
            );
          }}
        </ImageRefWatcher>
      )}
    </LoadedState>
  );
}

// https://stackoverflow.com/questions/59207735/show-a-react-placeholder-skeleton-until-a-image-loaded-completely-in-a-page
