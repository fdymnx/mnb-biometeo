/*
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from "react";
import dayjs from "dayjs";
import {makeStyles} from "@material-ui/core/styles";
import {Grid} from "@material-ui/core";
import {MapIconCodeSvg} from "./MapIconCodeSvg";
import clsx from "clsx";
import Config from "../../../Config";
import {toInt, dayFromWeek} from "../../../utilities/tools";
import globalStyles from "../../../globalStyles";

const useStyles = makeStyles((theme) => ({
  pBDate: {
    paddingBottom: 20
  },
  mBverticalBar: {
    marginBottom: 10
  },
  textLeft: {textAlign: "left"},
  ...globalStyles
}));

export function DetailsMeteo({meteoData, isDesktop, isSmallMobile, isMobile}) {
  const classes = useStyles();

  function renderDayMeteo({date, icon, temperatureMin, temperatureMax}, index) {
    return (
      <div style={{width: "100%", margin: "auto"}}>
        <div
          className={clsx(
            isDesktop && classes.fs17,
            isMobile && classes.fs14,
            isSmallMobile && classes.fs13,
            classes.segoeSemiBold,
            classes.pBDate,
            classes.blueText
          )}>
          {dayFromWeek(dayjs(date).day(), index === 0).toUpperCase()}
          {isDesktop ? <span> </span> : <span>&nbsp;</span>}
          {dayjs(date).format("DD/MM")}
        </div>
        <div className={clsx(classes.horizontalBarLightBlue, classes.mBverticalBar)} />

        {MapIconCodeSvg(icon, Config.colors.blue, Config.colors.blue)}

        <Grid
          container
          spacing={3}
          direction="row"
          justify="space-around"
          alignItems="flex-start"
          className={clsx(classes.fs29, classes.selawik)}>
          <Grid item xs={6} md={4} className={clsx(classes.blueText, classes.textLeft)}>
            {toInt(temperatureMin)}°
          </Grid>
          <Grid item xs={6} md={4} className={clsx(classes.blueText, classes.textLeft)}>
            {toInt(temperatureMax)}°
          </Grid>
        </Grid>
      </div>
    );
  }

  return (
    <div className={classes.fullHeight}>
      <div className={clsx(classes.fs27, classes.drawerPaddingTitle, classes.blueText)}>Prévisions météo</div>
      <Grid container spacing={4} direction="row" justify="space-between" alignItems="flex-start">
        {meteoData?.forcast?.slice(1, 8).map((item, index) => (
          <Grid key={index} item xs={6} md={4}>
            {renderDayMeteo(item, index)}
          </Grid>
        ))}
      </Grid>
      <br />
      <br />
    </div>
  );
}

/*  <>
            <br />
            <br />
            {["d", "n"].map((lettre, index) => (
              <div key={lettre} style={{backgroundColor: "red", color: "white"}}>
                <div>TEST bleue + {lettre} </div>
                <Grid container spacing={6} direction="row" justify="flex-start" alignItems="flex-start">
                  {["01", "02", "03", "04", "09", "10", "11", "13", "50"].map((icon, index) => (
                    <Grid key={index} item xs={6} md={4}>
                      {icon + lettre}
                      <hr />
                      {MapIconCodeSvg(icon + lettre, Config.colors.blue, Config.colors.blue)}
                      <hr />
                    </Grid>
                  ))}
                </Grid>
                <br />
                <br />
                <div>TEST blanc + {lettre} </div>

                <Grid container spacing={6} direction="row" justify="flex-start" alignItems="flex-start">
                  {["01", "02", "03", "04", "09", "10", "11", "13", "50"].map((icon, index) => (
                    <Grid key={index} item xs={6} md={4}>
                      {icon + lettre}
                      <hr />
                      {MapIconCodeSvg(icon + lettre, Config.colors.white, Config.colors.white)}
                      <hr />
                    </Grid>
                  ))}
                </Grid>
              </div>
            ))}
          </>
       */
