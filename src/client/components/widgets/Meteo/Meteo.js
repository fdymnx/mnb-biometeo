/*
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from "react";
import {makeStyles} from "@material-ui/core/styles";
import clsx from "clsx";
import {Container, Box, Grid} from "@material-ui/core";
import InfoIcon from "@material-ui/icons/Info";

import dayjs from "dayjs";
import FitText from "@kennethormandy/react-fittext";

import {UnstyledLink} from "../styledComponents/UnstyledLink";
import {RoundedButton} from "../styledComponents/RoundedButton";
import {toInt} from "../../../utilities/tools";
import {useMediaQueries} from "../../../utilities/responsive";
import AlignCheck from "../AlignCheck";

import globalStyles from "../../../globalStyles";
import Config from "../../../Config";

import upPng from "../../../public/icons/up.png";
import downPng from "../../../public/icons/down.png";

import {MapIconCodeSvg} from "./MapIconCodeSvg";
import {MyIcon} from "../MyIcons";

const useStyles = makeStyles((theme) => ({
  container: {width: "100%"},
  gridRootMax: {
    // paddingTop: 35,
    width: "100%",
    margin: "auto"
  },
  flexstart: {
    display: "flex",
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "flex-start",
    marginTop: "30px"
  },
  flexRowFS: {
    display: "flex",
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "flex-start"
  },
  flexRowFSFE: {
    display: "flex",
    flexDirection: "row",
    alignItems: "flex-end",
    justifyContent: "flex-start"
  },
  marginL8: {
    marginLeft: "8px"
  },
  marginLT8: {
    marginLeft: "8px",
    marginTop: "8px"
  },
  marginTB15: {
    marginTop: "25px",
    marginBottom: "25px"
  },
  marginL15: {marginLeft: 15},
  marginL10B5: {marginLeft: 10, marginBottom: 5},

  wrap: {
    webkitFlexWrap: "wrap",
    flexWrap: "wrap"
  },
  flexContainer: {
    padding: 0,
    margin: 0,
    msBoxOrient: "horizontal",
    display: "flex"
  },
  jcSB: {
    justifyContent: "space-between"
  },
  jcFS: {
    justifyContent: "flex-start"
  },
  minDesktopSVGSize: {
    width: "min(40vw,270px)", //"265px",
    height: "min(40vw,270px)" // "265px"
  },

  minMobileSVGSize: {
    height: "40vw",
    margin: "auto"
  },

  minDesktopMeteoTitleSize: {fontSize: "25px", fontFamily: "Segoe UI Semi Bold"},
  minMobileMeteoTitleSize: {fontSize: "17px", fontFamily: "Segoe UI Semi Bold"},
  minDesktopTemperatureSize: {fontSize: "66px", fontFamily: "Selawik !important", fontWeight: "bold"},
  minMobileTemperatureSize: {fontSize: "50px", fontFamily: "Selawik !important", fontWeight: "bold"},
  lineHeight: {lineHeight: 0.8},
  fs18White: {fontSize: 18, color: Config.colors.white},
  spacerDesk: {
    margin: 7,
    marginRight: "min(17px,2vw)"
  },
  spacerMob: {
    margin: 7,
    marginRight: "2vw"
  },

  ...globalStyles
}));

/**
 * Composant qui affiche la météo ( nooon sérieux )  avec le SVG + temperature et sur une seconde ligne des détails clickable comme durée du jour, uv etc
 */
export default function Meteo({meteoData}) {
  const classes = useStyles();

  const {isDesktop} = useMediaQueries();

  let temperature = meteoData?.temperature || 0;
  try {
    temperature = toInt(temperature);
  } catch (error) {}

  temperature += "°";

  let uvIndex = meteoData?.uvIndex || 0;
  try {
    uvIndex = toInt(uvIndex);
  } catch (error) {}

  let dayTime = 0;
  if (meteoData?.sunset && meteoData?.sunrise) {
    let totalMinutes = dayjs(meteoData?.sunset).diff(dayjs(meteoData?.sunrise), "m");
    let totalHours = Math.floor(totalMinutes / 60);
    totalMinutes = totalMinutes - totalHours * 60;
    if (totalMinutes < 10) {
      totalMinutes = "0" + totalMinutes;
    }
    dayTime = totalHours + "h" + totalMinutes;
  }

  // affiche le SVG + temperature + bouton "voir détails"
  function renderFirstPart() {
    // pour avoir les tailles grid xs sm md .... selon le type
    const gridSizes = {
      desktop: {
        left: {xs: 6, sm: 5, md: 6, lg: 4},
        right: {xs: 6, sm: 5, md: 6, lg: 3}
      },
      mobile: {
        left: {x: 8, sm: 6},
        right: {x: 8, sm: 6}
      }
    };

    // taille de l'icon meteo à gauche
    const leftGridSize = isDesktop ? gridSizes.desktop.left : gridSizes.mobile.left;
    // taille du texte "méto locale etc" a droite
    const rightGridSize = isDesktop ? gridSizes.desktop.right : gridSizes.mobile.right;

    return (
      <Grid className={classes.gridRootMax} container direction="row" justify="center" alignItems="center" spacing={2}>
        <Grid {...leftGridSize} item>
          <Box display="flex" alignItems="center" justifyContent="flex-end">
            <div className={clsx(isDesktop ? classes.minDesktopSVGSize : classes.minMobileSVGSize)}>
              {MapIconCodeSvg(meteoData?.icon)}
            </div>
          </Box>
        </Grid>

        <Grid {...rightGridSize} item className={clsx(isDesktop && classes.fullWidthNoMP)}>
          <Grid container direction="column" justify="space-between" alignItems="stretch">
            <Grid
              item
              className={clsx(
                classes.lineHeight,
                isDesktop ? classes.minDesktopMeteoTitleSize : classes.minMobileMeteoTitleSize
              )}>
              Météo&nbsp;locale
            </Grid>

            <Grid item className={clsx(classes.marginTB15, classes.flexRowFS)}>
              <div
                className={clsx(
                  classes.lineHeight,
                  isDesktop ? classes.minDesktopTemperatureSize : classes.minMobileTemperatureSize
                )}>
                {temperature}
              </div>
              {/*  <div className={clsx(classes.lineHeight, classes.minDesktopTemperatureSize)}>{temperature}</div> */}
              <div className={classes.marginL15}>
                {meteoData?.temperatureTrend &&
                  meteoData?.temperatureTrend === "increasing" &&
                  MyIcon(upPng, "6vw", isDesktop ? "25px" : "35px")}
                {meteoData?.temperatureTrend &&
                  meteoData?.temperatureTrend === "decreasing" &&
                  MyIcon(downPng, "6vw", isDesktop ? "25px" : "35px")}
              </div>
            </Grid>

            <Grid item>
              <UnstyledLink to={"/previsions"}>
                <RoundedButton onClick={() => null}>
                  <span className={clsx(isDesktop ? classes.fs16 : classes.fs12)}>Prévisions</span>
                </RoundedButton>
              </UnstyledLink>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    );
  }

  // affiche durée du jour, uv etc
  function renderSecondPart() {
    const leverSoleil = {
      label: "Lever du soleil",
      content: dayjs(meteoData["sunrise"]).format("HH:mm").replace(":", "h")
    };
    const coucherSoleil = {
      label: "Coucher du soleil",
      content: dayjs(meteoData["sunset"]).format("HH:mm").replace(":", "h")
    };
    const dureeJournee = {
      label: "Durée journée",
      content: (
        <div className={classes.flexRowFSFE}>
          <div>{dayTime}</div>
          <div className={clsx(classes.marginL10B5, classes.fs14)}>({meteoData["dayTimeDiff"]}&nbsp;min)</div>
        </div>
      )
    };
    const indiceUV = {
      label: "Indice UV",
      content: (
        <Grid container direction="row" justify="flex-start" alignItems="stretch">
          <Grid item>UV {uvIndex}</Grid>
          <Grid item className={classes.marginLT8}>
            <Box m={0} p={0} display="flex" alignItems="center" justifyContent="flex-start">
              {meteoData?.temperatureTrend &&
                meteoData?.temperatureTrend === "increasing" &&
                MyIcon(upPng, isDesktop ? "min(3vw,15px)" : "15px", "25px")}
              {meteoData?.temperatureTrend &&
                meteoData?.temperatureTrend === "decreasing" &&
                MyIcon(downPng, isDesktop ? "min(3vw,15px)" : "15px", "25px")}
            </Box>
          </Grid>
        </Grid>
      )
    };
    const indiceAtmo = {
      label: (
        <Grid container direction="row" justify="flex-start" alignItems="stretch">
          <Grid item>Indice Atmo</Grid>
          <Grid item className={classes.marginL8}>
            <UnstyledLink to={"/qualiteair"}>
              <InfoIcon className={clsx(classes.cursor, classes.fs18White)} />
            </UnstyledLink>
          </Grid>
        </Grid>
      ),

      content: meteoData?.atmoIndex + " / 100"
    };

    // méthode d'affiche avec des div et flex, plus adapté au desktop
    function divSP() {
      return (
        <div className={clsx(classes.flexContainer, classes.wrap, classes.jcFS)}>
          <FlexItem data={leverSoleil} isDesktop={isDesktop} />
          <FlexItem data={coucherSoleil} isDesktop={isDesktop} />
          <FlexItem data={dureeJournee} isDesktop={isDesktop} />
          <FlexItem data={indiceUV} isDesktop={isDesktop} />
          <FlexItem data={indiceAtmo} isDesktop={isDesktop} />
        </div>
      );
    }

    // méthode d'affichage avec des Grid,  plus adpaté au mobile car on ne veut pas que les éléments aient la même largeur sur desktop
    function gridSP() {
      return (
        <Grid container direction="row" justify="space-between" alignItems="stretch" spacing={2}>
          <Grid xs={12} sm={6} md={4} lg={2} item>
            <FlexItem data={leverSoleil} isDesktop={isDesktop} />
          </Grid>
          <Grid item xs={12} sm={6} md={4} lg={2}>
            <FlexItem data={coucherSoleil} isDesktop={isDesktop} />
          </Grid>
          <Grid item xs={12} sm={6} md={4} lg={3}>
            <FlexItem data={dureeJournee} isDesktop={isDesktop} />
          </Grid>

          <Grid item xs={12} sm={6} md={4} lg={2}>
            <FlexItem data={indiceUV} isDesktop={isDesktop} />
          </Grid>

          <Grid item xs={12} sm={12} md={8} lg={3}>
            <FlexItem data={indiceAtmo} isDesktop={isDesktop} />
          </Grid>
        </Grid>
      );
    }

    //return !isSmallMobile ? divSP() : gridSP();
    return isDesktop ? divSP() : gridSP();
  }

  return (
    <div id="meteo" className={classes.container}>
      {meteoData && (
        <Container maxWidth={Config.containerMaxSize}>
          {renderFirstPart()}
          {renderSecondPart()}
        </Container>
      )}
    </div>
  );
}

function FlexItem({data, isDesktop}) {
  const classes = useStyles();

  return (
    <div className={classes.flexstart}>
      {isDesktop ? (
        <>
          <div className={classes.verticalBar2} />
          <div className={classes.spacerDesk} />
          <div>
            {/* fsMax17 */} {/* fsMax36 */}
            <div className={clsx(/* classes.segoeSemiBold,*/ classes.fsMax16, classes.nowrap)}>{data.label}</div>
            <div className={clsx(classes.selawikRegularBold, isDesktop ? classes.fsMax34 : classes.fs34)}>
              {data.content}
            </div>
          </div>
          <div className={classes.verticalSpacer} />
        </>
      ) : (
        <>
          {/* <div className={classes.spacerMob} />*/}
          <div>
            <div className={clsx(classes.segoeSemiBold, classes.fs14, classes.nowrap)}>{data.label}</div>
            <FitText compressor={1} minFontSize={24} maxFontSize={30}>
              <div className={classes.selawikSemiBold}>{data.content}</div>
            </FitText>
          </div>
          {/* <div className={classes.verticalSpacer} />*/}
        </>
      )}
    </div>
  );
}
