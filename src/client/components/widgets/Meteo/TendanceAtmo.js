/*
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from "react";
import {makeStyles} from "@material-ui/core/styles";
import {ButtonBase, Grid} from "@material-ui/core";
import clsx from "clsx";

import upPng from "../../../public/icons/up-blue.png";
import downPng from "../../../public/icons/down-blue.png";
import {MyIcon} from "../MyIcons";

import Config from "../../../Config";
import globalStyles from "../../../globalStyles";

const useStyles = makeStyles((theme) => ({
  marginL15: {marginLeft: 15},
  pBottom20: {
    paddingBottom: 20
  },
  tablecell: {
    display: "table-cell"
  },
  labelRound: {
    fontSize: 16,
    padding: "6px 20px",
    lineHeight: 1.5,
    color: Config.colors.blue,
    backgroundColor: "#ECECEC",
    borderRadius: "50px",
    marginTop: 30,
    marginBottom: 30,
    fontSize: "19px",
    maxWidth: "120px",
    fontFamily: "Segoe UI Bold !important",
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  },
  flexColSB: {
    display: "flex",
    flexDirection: "column",
    alignItems: "stretch",
    justifyContent: "space-between",
    minHeight: "100%"
  },
  ...globalStyles
}));

export function TendanceAtmo({meteoData, isDesktop}) {
  const classes = useStyles();
  const {atmoIndex, atmoNextDayIndex, atmoNextDayTrend, atmoTrend} = meteoData;

  return (
    <div className={classes.flexColSB}>
      <div className={clsx(isDesktop ? classes.drawerHorizontalPadding : classes.drawerHorizontalPaddingMobile)}>
        <div className={clsx(classes.fs27, classes.drawerPaddingTitle, classes.blueText)}>Qualité de l'air</div>
        <Grid container direction="row" justify="space-around" alignItems="stretch" spacing={2}>
          <Grid item xs={12} sm={6}>
            <RenderDay label={"aujourd'hui"} atmoIndex={atmoIndex} atmoTrend={atmoTrend} isDesktop={isDesktop} />
          </Grid>
          <Grid item xs={12} sm={6}>
            <RenderDay
              label={"demain"}
              atmoIndex={atmoNextDayIndex}
              atmoTrend={atmoNextDayTrend}
              isDesktop={isDesktop}
            />
          </Grid>
        </Grid>
      </div>

      <Grid container direction="row" justify="center" alignItems="center">
        <Grid item xs={12} lg={10}>
          <ButtonBase style={{width: "100%"}}>
            <a
              target="_blank"
              href={"https://www.atmo-nouvelleaquitaine.org/monair/commune/24322"}
              className={clsx(classes.observationBlueButton, classes.fs19, classes.flexRowSB)}>
              Détails sur les données d'observation
              <svg fill="white" stroke="white" width="25px" height="25px" viewBox="0 0 25 25">
                <path d="M21.36 21.36H5.62V5.62h7.87V3.373H5.62a2.25 2.25 0 00-2.248 2.248v15.74a2.25 2.25 0 002.248 2.248h15.74a2.255 2.255 0 002.248-2.248v-7.87H21.36zM15.74 3.373V5.62h4.034L8.724 16.673l1.585 1.585 11.05-11.05v4.036h2.248v-7.87z"></path>
              </svg>
            </a>
          </ButtonBase>
        </Grid>
      </Grid>
    </div>
  );
}

function RenderDay({label, atmoIndex, atmoTrend, isDesktop}) {
  const classes = useStyles();
  return (
    <div>
      <div className={clsx(classes.fs17, classes.segoeSemiBold, classes.pBottom20, classes.blueText)}>
        {label.toUpperCase()}
      </div>

      <div
        style={{
          display: "table",
          position: "relative"
        }}>
        {/* fsMax75 */}
        <div
          className={clsx(
            classes.blueText,
            isDesktop ? classes.fs75 : classes.fs57,
            classes.selawikSemiBold,
            classes.tablecell
          )}>
          {atmoIndex}
        </div>
        <div>
          <div className={clsx(classes.blueText, classes.fs20, classes.selawikSemiBold, classes.tablecell)}>
            /&nbsp;100
          </div>
        </div>

        <div style={{position: "absolute", top: 12, right: 0}}>
          {atmoTrend === "increasing" && MyIcon(upPng, isDesktop ? "min(3vw,15px)" : "15px", "22px")}
          {atmoTrend === "decreasing" && MyIcon(downPng, isDesktop ? "min(3vw,15px)" : "15px", "22px")}
        </div>
      </div>

      <div className={classes.labelRound}>{mapLabel(atmoIndex)}</div>
    </div>
  );
}

/**
 * 0 < index <= 3 => "Très bon"
3 < index <= 5 => "Bon"
5 < index <= 6 => "Moyen"
6 < index <= 9 => "Médiocre"
9 < index <= 10 => "Mauvais"
10 < index => "Très Mauvais"
 * @param {*} index 
 */
function mapLabel(index) {
  switch (index / 10) {
    case 0:
    case 1:
    case 2:
    case 3:
      return "Très bon";
    case 4:
    case 5:
      return "Bon";
    case 6:
      return "Moyen";
    case 7:
    case 8:
    case 9:
      return "Médiocre";
    case 10:
      return "Mauvais";
    default:
      return "Très Mauvais";
  }
}
