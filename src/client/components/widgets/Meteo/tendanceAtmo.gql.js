import gql from "graphql-tag";

export const tendanceAtmoGql = gql`
  query Weather($geonamesId: ID!) {
    weather(geonamesId: $geonamesId) {
      sunset
      sunrise
      dayTimeDiff
      pressure
      temperature
      temperatureTrend
      uvIndex
      uvIndexTrend
      icon
      forcast {
        date
        icon
        temperatureMin
        temperatureMax
      }
      atmoIndex
      atmoNextDayIndex
      atmoNextDayTrend
      atmoTrend
    }
  }
`;
