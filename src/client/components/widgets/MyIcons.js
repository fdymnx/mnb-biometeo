import React from "react";

import {ReactComponent as svg01d} from "../../public/icons/meteo/01d.svg";
import {ReactComponent as svg01n} from "../../public/icons/meteo/01n.svg";
import {ReactComponent as svg02d} from "../../public/icons/meteo/02d.svg";
import {ReactComponent as svg02n} from "../../public/icons/meteo/02n.svg";
import {ReactComponent as svg03d} from "../../public/icons/meteo/03d.svg";
import {ReactComponent as svg04d} from "../../public/icons/meteo/04d.svg";
import {ReactComponent as svg09d} from "../../public/icons/meteo/09d.svg";
import {ReactComponent as svg10d} from "../../public/icons/meteo/10d.svg";
import {ReactComponent as svg10n} from "../../public/icons/meteo/10n.svg";
import {ReactComponent as svg11d} from "../../public/icons/meteo/11d.svg";
import {ReactComponent as svg13d} from "../../public/icons/meteo/13d.svg";
import {ReactComponent as svg50d} from "../../public/icons/meteo/50d.svg";

import {ReactComponent as LinkSvg} from "../../public/icons/link.svg";

const svgs = {
  svg01d,
  svg01n,
  svg02d,
  svg02n,
  svg03d,
  svg04d,
  svg09d,
  svg10d,
  svg10n,
  svg11d,
  svg13d,
  svg50d,
  LinkSvg
};

// icon from png/jpg
export function MyIcon(iconPng, width, maxWidth) {
  const responsiveIconStyle = {
    width: width || "6vw",
    maxWidth: maxWidth || "57px",
    height: "auto"
  };
  return <img src={iconPng} style={responsiveIconStyle} />;
}
/**
 * icon à partir d'un svg
 * remplit 100 % du parent
 * @param {*} param0
 */
export function MySvg({svgfilename, fill = "#fff", stroke = "#fff"}) {
  const SvgIcon = svgs[svgfilename];
  const props = {
    fill,
    stroke
  };

  const style = {
    width: "100%",
    height: "100%"
  };

  return (
    <div style={style}>
      <SvgIcon preserveAspectRatio="xMinYMin meet" width="100%" height="100%" {...props} />
    </div>
  );
}
