import React from "react";
import {makeStyles} from "@material-ui/core/styles";

import clsx from "clsx";
import {Grid} from "@material-ui/core";
import dayjs from "dayjs";
import globalStyles from "../../../globalStyles";
import Config from "../../../Config";
import {useFavoritesNewsContext} from "../../../hooks/useFavoritesNewsContext";
import {FavoriteHeartIcon} from "../FavoriteHeartIcon";
import twitterPng from "../../../public/icons/twitter.png";
import {MyIcon} from "../MyIcons";
import Linkify from "react-linkify";

const twitterLink = "https://twitter.com/biometeo24";

const useStyles = makeStyles((theme) => ({
  container: {
    padding: "min(4vw,30px)",
    paddingTop: 15,
    paddingBottom: 15
  },
  containerMob: {
    padding: "4vw"
  },
  newsText: {
    minHeight: "75px",
    fontSize: 20,
    fontFamily: "Selawik Light !important",
    paddingLeft: 10,
    paddingRight: 10
  },
  newsDateSmall: {
    marginTop: 15,
    fontSize: 16,
    fontStyle: "italic",
    fontFamily: "Selawik Light !important"
  },
  ...globalStyles
}));

// affiche le contenue d'un twit sans les bouttons. utilisé dans le newsReader et dans les news favorites
export function News({item, isDesktop, addContainer}) {
  const classes = useStyles();

  // context favoritesNews
  const {contextFNValues, contextFNSetValues} = useFavoritesNewsContext();

  // taille des icons coeur  et twitter
  const iconSize = Config.iconSize.desktop;

  if (isDesktop) {
    return (
      <div
        id="actualites"
        className={clsx(addContainer && classes.container, addContainer && !isDesktop && classes.containerMob)}>
        <Grid container direction="row" justify="center" alignItems="stretch">
          <Grid item xs={4} sm={3} md={2} lg={1} className={classes.flexCenter} style={{paddingTop: "8px"}}>
            <a target="_blank" href={item?.tweetURL} className={classes.textDecorationNone}>
              {MyIcon(twitterPng, iconSize.width, iconSize.maxWidth)}
            </a>
          </Grid>

          <Grid item xs={8} lg={10}>
            <div className={classes.newsText}>
              <Linkify
                componentDecorator={(decoratedHref, decoratedText, key) => (
                  <a target="blank" href={decoratedHref} key={key}>
                    {decoratedText}
                  </a>
                )}>
                {item?.content}
              </Linkify>
              <div className={classes.newsDateSmall}>
                Le {dayjs(item?.date).format("DD/MM/YYYY")} à {dayjs(item?.date).format("hh:mm")}
              </div>
            </div>
          </Grid>

          <Grid item xs={4} sm={3} md={2} lg={1} className={classes.flexCenter}>
            <FavoriteHeartIcon
              iconWidth={iconSize.width}
              iconMaxWidth={iconSize.maxWidth}
              item={item}
              contextValues={contextFNValues}
              contextSetValues={contextFNSetValues}
            />
          </Grid>
        </Grid>
      </div>
    );
  } else {
    return (
      <div
        id="actualites"
        className={clsx(addContainer && classes.container, addContainer && !isDesktop && classes.containerMob)}>
        <Grid container direction="row" justify="center" alignItems="stretch">
          <Grid item xs={3} sm={2} md={2} lg={1} className={classes.flexCenter} style={{paddingTop: "8px"}}>
            <a target="_blank" href={item?.tweetURL} className={classes.textDecorationNone}>
              {MyIcon(twitterPng, iconSize.width, iconSize.maxWidth)}
            </a>
          </Grid>

          <Grid item xs={6} sm={8} lg={10}>
            <div className={classes.newsDateSmall}>
              Le {dayjs(item?.date).format("DD/MM/YYYY")} à {dayjs(item?.date).format("hh:mm")}
            </div>
          </Grid>

          <Grid item xs={3} sm={2} md={2} lg={1} className={classes.flexCenter}>
            <FavoriteHeartIcon
              iconWidth={iconSize.width}
              iconMaxWidth={iconSize.maxWidth}
              item={item}
              contextValues={contextFNValues}
              contextSetValues={contextFNSetValues}
            />
          </Grid>
        </Grid>
        <div className={classes.newsText}>{item?.content}</div>
      </div>
    );
  }
}

// affiche l'icon de twitter et le texte "Retrouvez tous les messages partagés sur la page twitter..."
export function NewsLink({isDesktop}) {
  const classes = useStyles();
  const iconSize = Config.iconSize.desktop;

  return (
    <div className={clsx(classes.fs20, classes.container, !isDesktop && classes.containerMob)}>
      <Grid container direction="row" justify="center" alignItems="stretch">
        <Grid
          item
          xs={11}
          md={2}
          lg={1}
          className={classes.flexRowStart}
          style={{paddingTop: "3px", marginBottom: "5px"}}>
          {MyIcon(twitterPng, iconSize.width, iconSize.maxWidth)}
        </Grid>

        <Grid item xs={12} md={10} lg={11} className={clsx(classes.flexRowStart, classes.newsText)}>
          <a target="_blank" href={twitterLink} className={classes.textDecorationNone}>
            Retrouvez tous les messages partagés sur la page twitter de la BioMétéo&nbsp;: {twitterLink}
          </a>
        </Grid>
      </Grid>
    </div>
  );
}
