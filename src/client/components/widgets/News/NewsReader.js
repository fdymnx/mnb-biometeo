import React, {useState, useContext} from "react";
import {makeStyles} from "@material-ui/core/styles";

import clsx from "clsx";
import {Grid, Box, IconButton} from "@material-ui/core";

import FitText from "@kennethormandy/react-fittext";
import {NewsContext} from "../../../hooks/NewsContext";
import Config from "../../../Config";
import {MyIcon} from "../MyIcons";
import {useQuery} from "@apollo/react-hooks";
import {gqlNews} from "./news.gql.js";

import globalStyles from "../../../globalStyles";
import {News} from "./News";
import {useMediaQueries} from "../../../utilities/responsive";
import cercleBackPng from "../../../public/icons/cercle-back-x1.png";
import cercleNextPng from "../../../public/icons/cercle-next-x1.png";

import {_readReadedNews, _writeReadedNews, _addReadedNews, _isThisNewsReaded} from "./readedNews";

const useStyles = makeStyles((theme) => ({
  center: {display: "flex", flexDirection: "column", alignItems: "center", justifyContent: "center"},

  container: {
    backgroundColor: Config.colors.white, // "#172B37",
    color: Config.colors.blue,
    margin: "auto",
    // marginTop: Config.categorie.gridSpacing * 8,
    padding: "30px",
    padding: "min(4vw,30px)",
    paddingTop: 15,
    paddingBottom: 15,
    borderRadius: "10px"
  },
  containerMob: {
    padding: "4vw"
  },
  mr16: {
    marginRight: "16px"
  },
  pB40: {
    paddingBottom: 40
  },
  font20: {
    fontSize: 20,
    fontFamily: "Selawik Light !important"
  },
  font16: {
    fontSize: 16,
    fontFamily: "Selawik Light !important"
  },

  ...globalStyles
}));

export default function NewsReader({city}) {
  const classes = useStyles();

  const {isDesktop} = useMediaQueries();
  const [allNews, setAllNews] = useState([]);
  const [newsIndex, setNewsIndex] = useState(0);

  const {newsContextValue, newsContextSetValue} = useContext(NewsContext);

  function nextNews() {
    if (allNews?.length > newsIndex + 1) {
      _addReadedNews(allNews?.[newsIndex]);
      setNewsIndex(newsIndex + 1);
      setUnreadNews(allNews);
    }
  }

  function previousNews() {
    if (newsIndex > 0) {
      // si c'est la dernière on la marque comme lu sinon on ne pourra jamais le faire avec le bouton next qui est desactivé pour la dernière
      if (allNews?.length === newsIndex + 1) {
        _addReadedNews(allNews?.[newsIndex]);
        setUnreadNews(allNews);
      }
      setNewsIndex(newsIndex - 1);
    }
  }

  useQuery(gqlNews, {
    variables: {geonamesId: city.id},
    onCompleted: (data) => {
      let notifications = data?.news?.notifications /*.reverse()*/ || [];
      setAllNews(notifications);
      setUnreadNews(notifications);
    }
  });

  function setUnreadNews(all_news) {
    let unreaded = 0;
    for (let i in all_news) {
      if (!_isThisNewsReaded(all_news?.[i])) {
        unreaded++;
      }
    }
    newsContextSetValue(unreaded);
  }

  if (allNews && allNews.length > 0) {
    return (
      <div className={clsx(classes.container, !isDesktop && classes.containerMob)}>
        <Grid
          container
          direction="row"
          justify="space-between"
          alignItems="stretch"
          id={"actu"}
          className={clsx(classes.fs40, classes.segoeSemiBold, classes.pB40)}>
          <Grid item xs={8} lg={9}>
            <FitText compressor={1} minFontSize={20} maxFontSize={35}>
              Actualités
            </FitText>
          </Grid>
          <Grid item xs={4} lg={3} className={clsx(classes.font16, classes.flexCenter)}>
            {!_isThisNewsReaded(allNews?.[newsIndex]) && <i>Non lu</i>}
          </Grid>
        </Grid>

        <div className={classes.flexColumnCC}>
          <div className={classes.width100}>
            <News item={allNews?.[newsIndex]} isDesktop={isDesktop} addContainer={false} />
          </div>
          {allNews.length > 1 && (
            <Grid container direction="row" justify="center" alignItems="stretch">
              <Grid item xs={2} lg={1}>
                <Box display="flex" alignItems="center" justifyContent="center">
                  {newsIndex > 0 && (
                    <IconButton onClick={() => previousNews()}>{MyIcon(cercleBackPng, "8vw", "35px")}</IconButton>
                  )}
                </Box>
              </Grid>
              <Grid item xs={4} lg={2} className={clsx(classes.center, classes.font20)}>
                {newsIndex + 1 + " / " + allNews.length}
              </Grid>
              <Grid item xs={2} lg={1}>
                <Box display="flex" alignItems="center" justifyContent="center">
                  {allNews?.length > newsIndex + 1 && (
                    <IconButton onClick={() => nextNews()}>{MyIcon(cercleNextPng, "8vw", "35px")}</IconButton>
                  )}
                </Box>
              </Grid>
            </Grid>
          )}
        </div>
      </div>
    );
  } else return null;
}
