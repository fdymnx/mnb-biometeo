import gql from "graphql-tag";

export const gqlNews = gql`
  query News($geonamesId: ID!) {
    news(geonamesId: $geonamesId) {
      notifications {
        id
        date
        content
        tweetURL
      }
    }
  }
`;
