import React from "react";
import {Drawer as DrawerMaterialUI} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import clsx from "clsx";
import {useMediaQueries} from "../../../utilities/responsive";
import globalStyles from "../../../globalStyles";

import {SideMenuTopBar} from "./SideMenuTopBar";

const useStyles = makeStyles((theme) => ({
  container: {
    width: "auto",
    minWidth: "350px",
    //minWidth: "min(85vw,700px)",
    maxWidth: "700px",
    //maxWidth: "max(35vw,700px)",
    paddingTop: 10,
    paddingBottom: 35
  },
  containerMob: {
    minWidth: "85vw",
    maxWidth: "100vw"
  },
  ...globalStyles
}));

/**
 * Affiche une modal blanche sur tout le coté droit de la page
 *
 * @param {*}
 */
export function Drawer({show, setShow, showHorizontalPadding, children, title, showTopBar}) {
  const classes = useStyles();

  const {isDesktop} = useMediaQueries();

  // onClose={() => {  !disableOnClose && setShow();  }}
  return (
    <DrawerMaterialUI anchor={"right"} open={show} onClose={setShow}>
      <div
        className={clsx(
          !!showHorizontalPadding && isDesktop && classes.drawerHorizontalPadding,
          !!showHorizontalPadding && !isDesktop && classes.drawerHorizontalPaddingMobile,
          classes.container,
          !isDesktop && classes.containerMob,
          classes.fullHeight
        )}
        role="presentation">
        {showTopBar && <SideMenuTopBar title={title} />}
        {children}
      </div>
    </DrawerMaterialUI>
  );
}
