/*
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, {useEffect} from "react";
import {useHistory} from "react-router-dom";
import {Drawer} from "./Drawer";
import {SpecieDetails} from "../Categorie/SpecieDetails";
import {useMediaQueries} from "../../../utilities/responsive";
import {useUrlQuery} from "../../../hooks/useUrlQuery";
import {useMnbGeolocService} from "../../../hooks/useMnbGeolocService";
import {useLazyQuery} from "@apollo/react-hooks";
import gql from "graphql-tag";

import {gqlTaxonFragment} from "../../../components/widgets/Categorie/categorie.gql.js";
const gqlSpecie = gql`
  query Taxon($geonamesId: ID!, $id: ID!) {
    taxon(id: $id) {
      ...TaxonFragment
    }
  }
  ${gqlTaxonFragment}
`;

/**
 * affiche un drawer latéral avec le contenu détaillée d'une espece dont l'id a été passé en paramètre ( de l'url)
 * @param {*} param0
 */

export function DrawerForOneSpecie({}) {
  let history = useHistory();
  const {isDesktop, isMobile, isSmallMobile} = useMediaQueries();
  const taxonid = useUrlQuery().get("taxonid"); // récupère l'id du taxon a afficher sur le drawer laterale ( pour le partage de taxon )
  /** quand on clic sur l'url partagée depuis le mail par ex, si on fait 'history.goBack();' alors on sort du site directement,
   * donc l'utilisateur ne peux pas voir le contenu si il click à gauche de la popup ( dans la partie grisée )
   *
   * ce composant est appelé également depuis la vue "mes espèces favorites",
   */
  const cangoback = useUrlQuery().get("back");

  const {selectedPlace: city} = useMnbGeolocService();
  const [getData, {loading, data}] = useLazyQuery(gqlSpecie);

  useEffect(() => {
    if (city && city.id) {
      getData({
        variables: {id: taxonid, geonamesId: city?.id}
      });
    }
  }, [city]);

  return (
    <Drawer
      show={!loading && !!data?.taxon}
      setShow={() => {
        console.log(cangoback);
        if (cangoback === "t") {
          history.goBack();
        } else {
          history.push("/");
        }
      }}
      showHorizontalPadding={false}
      title={false}
      showTopBar={true}>
      <SpecieDetails isDesktop={isDesktop} isMobile={isMobile} isSmallMobile={isSmallMobile} item={data?.taxon} />
    </Drawer>
  );
}
