/*
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from "react";
import {Switch, Route, useHistory} from "react-router-dom";

import globalStyles from "../../../globalStyles";
import {makeStyles} from "@material-ui/core/styles";
import {sideMenuPages, SideMenu} from "./SideMenu";

import {useMediaQueries} from "../../../utilities/responsive";

import {DetailsMeteo} from "../Meteo/DetailsMeteo";
import {TendanceAtmo} from "../Meteo/TendanceAtmo";
import {Drawer} from "./Drawer";
import {DrawerForOneSpecie} from "./DrawerForOneSpecie";

const useStyles = makeStyles((theme) => ({
  ...globalStyles
}));
/**
 * affiche un drawer latéral avec le contenu children
 */
export function DrawerRouter({geonamesId, meteoData}) {
  let history = useHistory();

  const {isDesktop, isSmallMobile, isMobile} = useMediaQueries();
  function onClose() {
    history.goBack();
  }

  return (
    <Switch>
      <Route path="/menu">
        <Drawer show={true} setShow={onClose} showHorizontalPadding={true}>
          <SideMenu />
        </Drawer>
      </Route>

      <Route path="/taxon">
        <DrawerForOneSpecie />
      </Route>
      <Route path="/previsions">
        <Drawer show={true} setShow={onClose} showHorizontalPadding={true} title="Retour" showTopBar={true}>
          <DetailsMeteo isDesktop={isDesktop} isSmallMobile={isSmallMobile} isMobile={isMobile} meteoData={meteoData} />
        </Drawer>
      </Route>
      <Route path="/qualiteair">
        <Drawer show={true} setShow={onClose} showHorizontalPadding={true} title="Retour" showTopBar={true}>
          <TendanceAtmo meteoData={meteoData} isDesktop={isDesktop} />
        </Drawer>
      </Route>
      <Route path="/taxon">
        <DrawerForOneSpecie />
      </Route>

      {sideMenuPages.map((item, index) => {
        return (
          <Route path={`/${item.id}`} key={index}>
            <Drawer
              show={true}
              setShow={() => onClose()}
              showHorizontalPadding={true}
              title={item.title}
              showTopBar={true}>
              {item.component}
            </Drawer>
          </Route>
        );
      })}
    </Switch>
  );
}
