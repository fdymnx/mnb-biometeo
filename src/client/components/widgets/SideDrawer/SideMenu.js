/*
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, {useState} from "react";
import {makeStyles} from "@material-ui/core/styles";
import Config from "../../../Config";
import {Box, Divider, IconButton} from "@material-ui/core";
import clsx from "clsx";
import {useMediaQueries} from "../../../utilities/responsive";
import staticPages from "../../../assets/markdownPage/staticPages";
import globalStyles from "../../../globalStyles";
import {DisplayMarkDownPage} from "../DisplayMarkDownPage";
import FavoritesSpecies from "../../pages/FavoritesSpecies";
import FavoritesNews from "../../pages/FavoritesNews";

import {UnstyledLink} from "../styledComponents/UnstyledLink";
import {MyIcon} from "../MyIcons";
import rightArrowBluePng from "../../../public/icons/right-arrow-blue.png";
import heartwhite from "../../../public/icons/heart-white.png";
import twitter from "../../../public/icons/twitter.png";
import {SideMenuTopBar} from "./SideMenuTopBar";

const useStyles = makeStyles((theme) => ({
  center: {
    padding: 15,
    height: "90%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  },
  pTB30: {
    paddingTop: 30,
    paddingBottom: 30
  },
  maxWidth: {
    maxWidth: "600px",
    minWidth: "30vw"
  },
  colorBlue: {
    color: Config.colors.blue,
    fontSize: "calc(10px + 2vmin)"
  },
  ...globalStyles
}));

// on rajoute la page pour les espèces favorites
let pagesTmp = [
  {
    title: "Espèces favorites",
    id: "especesfavorites",
    component: <FavoritesSpecies />,
    icon: heartwhite
  },
  {
    title: "Actualités favorites",
    id: "actualitesfavorites",
    component: <FavoritesNews />,
    icon: twitter
  }
];
for (let i in staticPages) {
  pagesTmp.push({
    title: staticPages[i].title,
    id: staticPages[i].id,
    icon: staticPages[i].icon,
    component: <DisplayMarkDownPage title={staticPages[i].title} content={staticPages[i].content} />
  });
}

export const sideMenuPages = pagesTmp;
/**
 * affiche le contenu du menu latérale avec les entrées "espèces favorites", "actualités favorites" et les pages statiques
 */
export function SideMenu({}) {
  const classes = useStyles();

  const {isDesktop} = useMediaQueries();

  return (
    <div className={classes.height100}>
      <SideMenuTopBar title={false} />
      <div className={classes.center}>
        <div
          className={clsx(
            isDesktop ? classes.drawerHorizontalPadding : classes.drawerHorizontalPaddingMobile,
            classes.maxWidth
          )}>
          {sideMenuPages.map((item, index) => (
            <div key={index}>
              {index !== 0 && <Divider />}

              <UnstyledLink to={`/${item.id}`} className={clsx(classes.flexRowSB, classes.pTB30)}>
                <div className={classes.flexRow}>
                  {item.icon && (
                    <div
                      className={clsx(
                        classes.sideMenuMarginRightIcon,
                        !isDesktop && classes.sideMenuMarginRightIconMob
                      )}>
                      <Box display="flex" alignItems="center" justifyContent="center">
                        {MyIcon(item.icon, "12vw", "50px")}
                      </Box>
                    </div>
                  )}
                  <span className={classes.colorBlue}>{item.title}</span>
                </div>
                <div className={clsx(classes.sideMenuMarginLeftIcon, !isDesktop && classes.sideMenuMarginLeftIconMob)}>
                  <Box display="flex" alignItems="center" justifyContent="center">
                    {MyIcon(rightArrowBluePng, "3vw", "10px")}
                  </Box>
                </div>
              </UnstyledLink>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}
