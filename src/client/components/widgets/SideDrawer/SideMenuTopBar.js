/*
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, {useState} from "react";

import {makeStyles} from "@material-ui/core/styles";
import {Grid, Box, IconButton} from "@material-ui/core";
import {useHistory} from "react-router-dom";
import CloseIcon from "@material-ui/icons/Close";
import clsx from "clsx";
import {MyIcon} from "../MyIcons";
import {useMediaQueries} from "../../../utilities/responsive";
import cerclePng from "../../../public/icons/cercle-back-x1.png";
import Config from "../../../Config";
import globalStyles from "../../../globalStyles";

const useStyles = makeStyles((theme) => ({
  blueText: {
    color: Config.colors.blue
  },
  ...globalStyles
}));
/**
 * affiche la bar du haut dans le menu latérale, avec
 * à gauche facultativement un bouton retour, un titre,
 * à droite un bouton X
 *
 * title : titre à afficher
 */
export function SideMenuTopBar({title}) {
  const classes = useStyles();
  const {isDesktop} = useMediaQueries();
  let history = useHistory();

  return (
    <Grid container direction="row-reverse" justify="space-between" alignItems="center">
      <Grid item>
        <IconButton
          onClick={() => {
            history.push("/");
          }}
          className={classes.zIndex2}>
          <CloseIcon style={{color: Config.colors.blue}} />
        </IconButton>
      </Grid>
      {title && (
        <Grid
          item
          onClick={() => {
            history.goBack();
          }}>
          <div className={clsx(classes.fs20, classes.cursor, classes.marginTop15, classes.flexRow, classes.blueText)}>
            <div
              className={clsx(
                classes.sideMenuMarginRightIcon,
                !isDesktop && classes.sideMenuMarginRightIconMob,
                classes.zIndex2
              )}>
              <Box display="flex" alignItems="center" justifyContent="center">
                {MyIcon(cerclePng, "10vw", "42px")}
              </Box>
            </div>
            {title}
          </div>
        </Grid>
      )}
    </Grid>
  );
}
