/*
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, {useState, useContext} from "react";
import {makeStyles} from "@material-ui/core/styles";
import {useHistory} from "react-router-dom";

import {IconButton, Grid, withWidth} from "@material-ui/core";
import MenuRoundedIcon from "@material-ui/icons/MenuRounded";
import BioDesktop from "../../public/icons/biometeoDesktop.png";
import BioMobile from "../../public/icons/biometeo.png";

import UserLocalisation from "./UserLocalisation";
import clsx from "clsx";
import {useMediaQueries} from "../../utilities/responsive";
import {NewsContext} from "../../hooks/NewsContext";
import globalStyles from "../../globalStyles";
import BlueBadge from "./styledComponents/BlueBadge";
import {goToAnchor} from "react-scrollable-anchor";

const useStyles = makeStyles((theme) => ({
  responsiveIcon: {
    width: "100%",
    maxWidth: "180px",
    height: "auto"
  },
  responsiveIconSmall: {
    width: "100%",
    maxWidth: "100px",
    height: "auto"
  },
  padding: {
    padding: 16,
    paddingLeft: "32px",
    paddingLeft: "min(32px, 4vw)",
    paddingRight: "32px",
    paddingRight: "min(32px, 4vw)",
    marginBottom: "60px",
    marginBottom: "min(60px, 4vw)"
  },
  compactPaddingBottom: {
    marginBottom: "20px"
  },
  paddingMob: {
    padding: 16,
    paddingLeft: "4vw",
    paddingRight: "4vw",
    marginBottom: "4vw"
  },

  ...globalStyles
}));
/**
 * Affiche la barre du haut,  avec l'icon BioMétéo à gauche, la zone de recherche de localisation et le menu à droite
 * @param {*} props
 */
function TopMenu({width, compact}) {
  let history = useHistory();
  const classes = useStyles();
  const {isDesktop} = useMediaQueries();
  const {newsContextValue} = useContext(NewsContext);

  return (
    <Grid
      className={clsx(
        classes.padding,
        !isDesktop && classes.paddingMob,
        isDesktop && compact && classes.compactPaddingBottom
      )}
      container
      direction="row"
      justify="space-between"
      alignItems="center"
      spacing={0}>
      <Grid item xs={3}>
        {width && ["sm", "xs"].includes(width) ? (
          <img src={BioMobile} className={classes.responsiveIconSmall} alt="biometeo" />
        ) : (
          <img src={BioDesktop} className={classes.responsiveIcon} alt="biometeo" />
        )}
      </Grid>
      <Grid item xs={6} className={classes.fullWidthNoMP}>
        <Grid container alignItems="center" justify="center" className={classes.fullWidthNoMP}>
          <UserLocalisation />
        </Grid>
      </Grid>

      <Grid item xs={3} lg={2}>
        <Grid container direction="row" justify="flex-end" alignItems="center" spacing={2}>
          {!compact && (
            <Grid item xs={6} lg={4} className={classes.flexRowEnd}>
              <IconButton size="small" color="inherit" onClick={() => goToAnchor("actualites")}>
                <BlueBadge badgeContent={newsContextValue} />
              </IconButton>
            </Grid>
          )}

          <Grid item xs={6} lg={4} className={classes.flexRowEnd}>
            <IconButton size="small" color="inherit" onClick={() => history.push("/menu")}>
              <MenuRoundedIcon />
            </IconButton>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
}

export default withWidth()(TopMenu);
