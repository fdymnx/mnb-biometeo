/*
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, {useState, useContext, useEffect} from "react";
import {makeStyles} from "@material-ui/core/styles";
import {Paper} from "@material-ui/core";

import AutocompleteCities from "./AutocompleteCities";
import Config from "../../Config";

const useStyles = makeStyles((theme) => ({
  searchRoot: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: Config.colors.darkBlue,
    borderRadius: "50px",
    margin: 5,
    width: "100%",
    maxWidth: "400px"
  }
}));

/**
 * Affiche la zone de recherche de localisation, affiche button de localisation auto, de recherche et les dernières communes sauvegardées en localstorage
 */
export default function UserLocalisation({}) {
  const classes = useStyles();

  // pour gérer la liste des villes deja saisies par l'utilisateur
  const [lastCities, setLastCities] = useState(
    localStorage.getItem("userCities") ? JSON.parse(localStorage.getItem("userCities")) : []
  );

  // au démarrage on essai de lire la localisation depuis le navigateur si le code postale n'est pas fourni
  useEffect(() => {
    // et on initialise le localStorage si il est null
    if (!localStorage.getItem("userCities")) {
      localStorage.setItem("userCities", JSON.stringify([]));
    }
  }, []);

  /**
   * Garder les 5 dernières localisations dans le localstorage 'userCities' et dans le state lastCities
   * @param {*} city
   */
  function addToLastCities(city) {
    var found = lastCities.some(function (value) {
      return city?.name.toLowerCase() === value?.name.toLowerCase() || city?.id === value?.id;
    });

    if (!found) {
      let ncv = [city, ...lastCities].splice(0, 4);
      setLastCities(ncv);
      localStorage.setItem("userCities", JSON.stringify(ncv));
    }
  }

  return (
    <Paper elevation={1} className={classes.searchRoot} variant="outlined">
      <AutocompleteCities
        key={"b"}
        onValueChange={(city) => {
          // console.log("city changed", city);
          // si l'utilisateur saisi une ville alors il faut récupérer ses informations et la
          // sauvegarder dans le state et dans le localStorage
          handleChangeCity(city);
        }}
        lastCities={lastCities}
      />
    </Paper>
  );

  function handleChangeCity(city) {
    addToLastCities(city);
  }
}
