import React from "react";
import {makeStyles} from "@material-ui/core/styles";
import clsx from "clsx";
import {ButtonBase, Grid} from "@material-ui/core";

import globalStyles from "../../../globalStyles";
import Config from "../../../Config";

const useStyles = makeStyles((theme) => ({
  blueRound: {
    padding: 7,
    color: Config.colors.white,
    backgroundColor: "#0A1F2E",
    borderRadius: "30px",
    boxShadow: "0 2px 8px rgba(0, 0, 0, 0.15)"
  },
  roundWidth: {
    width: "180px"
  },
  roundWidthMob: {
    width: "110px"
  },
  whiteRound: {
    backgroundColor: Config.colors.white,
    color: Config.colors.blue,
    borderRadius: "30px",
    boxShadow: "0 2px 8px rgba(0, 0, 0, 0.15)"
  },
  txt: {
    padding: "10px",
    paddingLeft: "25px",
    paddingRight: "25px",
    fontSize: 15,
    fontFamily: "Selawik !important"
  },
  txtMob: {
    paddingLeft: "10px",
    paddingRight: "10px",
    fontSize: 11
  },
  ...globalStyles
}));

export function SwitchButton({value, setValue, isDesktop}) {
  const classes = useStyles();
  return (
    <div className={clsx(classes.flexRowSB, classes.blueRound, isDesktop ? classes.roundWidth : classes.roundWidthMob)}>
      <ButtonBase
        onClick={() => {
          setValue(false);
        }}
        className={clsx(classes.txt, !isDesktop && classes.txtMob, !value && classes.whiteRound)}>
        Nappe
      </ButtonBase>
      <ButtonBase
        onClick={() => {
          setValue(true);
        }}
        className={clsx(classes.txt, !isDesktop && classes.txtMob, value && classes.whiteRound)}>
        Rivière
      </ButtonBase>
    </div>
  );
}
