import React from "react";

import {Container, Grid} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import clsx from "clsx";
import FitText from "@kennethormandy/react-fittext";
import {useHistory} from "react-router-dom";

import Config from "../../../Config";
import globalStyles from "../../../globalStyles";
import {upperFirstLetter} from "../../../utilities/tools";
import {useMediaQueries} from "../../../utilities/responsive";
import upPng from "../../../public/icons/up.png";
import downPng from "../../../public/icons/down.png";
import {MyIcon} from "../MyIcons";
import eauWPng from "../../../public/icons/eauW.png";
import {useUrlQuery} from "../../../hooks/useUrlQuery";
import {goToAnchor} from "react-scrollable-anchor";

const useStyles = makeStyles((theme) => ({
  container: {
    mardin: 0,
    padding: 0,
    marginTop: 25,
    marginBottom: 25
  },
  flexRowStartStretch: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "stretch"
  },
  paddingRight: {
    paddingRight: 25
  },
  paddingRightMob: {
    paddingRight: 15
  },
  ...globalStyles
}));

/**
 * Affiche les textes sur le détail textuelle sur la riviere/nappe
 * @param {*} param0
 */
export function WaterDetails({waterData}) {
  const classes = useStyles();

  let history = useHistory();
  const {isDesktop} = useMediaQueries();
  const urlSearchEntries = useUrlQuery().entries();

  return (
    <Container maxWidth={Config.containerMaxSize} className={classes.container}>
      <Grid container spacing={4} justify="center" alignItems="stretch">
        <Grid
          item
          xs={12}
          sm={6}
          onClick={() => {
            history.push(createAnchorLink(urlSearchEntries, "nappe", true));
            goToAnchor("graph");
          }}
          className={classes.cursor}>
          <div className={clsx(classes.fs16, classes.segoeSemiBold, classes.flexRowStartStretch)}>
            <div className={clsx(isDesktop ? classes.paddingRight : classes.paddingRightMob)}>
              {MyIcon(eauWPng, "5vw", "35px")}
            </div>
            <div className={classes.flexCenter}>NIVEAU DE LA NAPPE</div>
          </div>
          <div className={classes.horizontalBar} />
          <FitText compressor={1} minFontSize={20} maxFontSize={32}>
            <span className={clsx(classes.segoeSemiBold)}>
              {waterData?.groundWaterObservation?.station?.stationType}
            </span>
          </FitText>
        </Grid>

        <Grid
          item
          xs={12}
          sm={6}
          onClick={() => {
            history.push(createAnchorLink(urlSearchEntries, "riviere", true));
            goToAnchor("graph");
          }}
          className={classes.cursor}>
          <div className={clsx(classes.fs16, classes.segoeSemiBold, classes.flexRowStartStretch)}>
            <div className={clsx(isDesktop ? classes.paddingRight : classes.paddingRightMob)}>
              {MyIcon(eauWPng, "5vw", "35px")}
            </div>
            <div className={classes.flexCenter}>NIVEAU DE LA RIVIERE</div>
          </div>
          <div className={classes.horizontalBar} />
          <FitText compressor={1} minFontSize={20} maxFontSize={32}>
            <span className={clsx(classes.segoeSemiBold)}>
              {waterData?.riverObservation?.station?.label &&
                upperFirstLetter(waterData?.riverObservation?.station?.label)}
              {waterData?.riverObservation?.station?.city && (
                <span className={classes.cityName}>
                  {" "}
                  à {upperFirstLetter(waterData?.riverObservation?.station?.city)}
                </span>
              )}
            </span>
          </FitText>
        </Grid>

        <Grid item xs={12} sm={6}>
          <Grid container direction="row" justify="space-between" alignItems="stretch">
            <Grid item xs={10}>
              <FitText compressor={1} minFontSize={20} maxFontSize={27}>
                {waterData?.groundWaterObservation?.station?.aquiferLabel}
              </FitText>
            </Grid>
            <Grid item xs={2}>
              {waterData?.groundWaterObservation?.trend &&
                waterData?.groundWaterObservation?.trend === "increasing" &&
                MyIcon(upPng, "5vw", isDesktop ? "25px" : "35px")}
              {waterData?.groundWaterObservation?.trend &&
                waterData?.groundWaterObservation?.trend === "decreasing" &&
                MyIcon(downPng, "5vw", isDesktop ? "25px" : "35px")}
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={12} sm={6}>
          <Grid container direction="row" justify="space-between" alignItems="stretch">
            <Grid item xs={10}>
              {waterData?.riverObservation?.lastLevel && (
                <FitText compressor={1} minFontSize={20} maxFontSize={27}>
                  {waterData?.riverObservation?.lastLevel + " L/s"}
                </FitText>
              )}
            </Grid>
            <Grid item xs={2}>
              {waterData?.riverObservation?.trend &&
                waterData?.riverObservation?.trend === "increasing" &&
                MyIcon(upPng, "5vw", isDesktop ? "25px" : "35px")}
              {waterData?.riverObservation?.trend &&
                waterData?.riverObservation?.trend === "decreasing" &&
                MyIcon(downPng, "5vw", isDesktop ? "25px" : "35px")}
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Container>
  );
}

/**
 * pour creer un lien avec la nouvelle ancre vers le graphique et son type (nappe ou riviere)
 * @param {*} urlSearchEntries
 * @param {*} param  : nappe ou riviere
 * @param {*} addAnchor : ajout anchor ou non,
 */
export function createAnchorLink(urlSearchEntries, param, addAnchor) {
  let url = (addAnchor ? "?anchor=graph&" : "?") + "type=" + param;
  for (var pair of urlSearchEntries) {
    if (pair[0] !== "anchor" && pair[0] !== "type") {
      url += "&" + pair[0] + "=" + pair[1];
    }
  }
  return url;
}
