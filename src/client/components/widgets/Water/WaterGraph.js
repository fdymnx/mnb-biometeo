/*
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, {useState} from "react";
import {CartesianGrid, Area, AreaChart, XAxis, YAxis, Tooltip, ResponsiveContainer, ReferenceLine} from "recharts";
import {makeStyles} from "@material-ui/core/styles";
import InfoOutlinedIcon from "@material-ui/icons/InfoOutlined";

import {useMediaQueries} from "../../../utilities/responsive";
import clsx from "clsx";
import {Grid, IconButton, CircularProgress, ButtonBase} from "@material-ui/core";
import dayjs from "dayjs";

import globalStyles from "../../../globalStyles";
import Config from "../../../Config";

import {upperFirstLetter} from "../../../utilities/tools";
import {createAnchorLink} from "./WaterDetails";
import {useUrlQuery} from "../../../hooks/useUrlQuery";
import {SwitchButton} from "./SwitchButton";
import {useHistory} from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  container: {
    width: "calc(100%-50px)",
    backgroundColor: "#172B37",
    color: Config.colors.white,
    padding: "25px",
    borderBottomRightRadius: "10px",
    borderBottomLeftRadius: "10px"
  },
  containerMob: {
    width: "calc(100%-10px)",
    padding: "5px"
  },
  graphSize: {
    width: "100%",
    height: "300px"
  },
  graphSizeMob: {
    width: "100%",
    height: "200px"
  },
  padding15B50: {
    padding: 15,
    paddingBottom: 50
  },
  padding15: {
    padding: 15
  },
  paddingL15: {
    paddingLeft: 15
  },
  paddingR15: {
    paddingRight: 15
  },
  cityName: {
    fontSize: 16,
    fontFamily: "Selawik Light !important",
    paddingLeft: "15px"
  },
  tooltip: {
    fontSize: 17,
    fontFamily: "Selawik Light !important",
    padding: 15,
    backgroundColor: Config.colors.white,
    color: Config.colors.blue,
    borderRadius: "30px",
    boxShadow: "0 2px 8px rgba(0, 0, 0, 0.15)"
  },
  legendText: {
    fontSize: 12,
    fontFamily: "Selawik Light !important"
  },
  whiteText12: {fontFamily: "Selawik !important", fontSize: 12, color: "white"},
  whiteText: {
    color: Config.colors.white,
    fontSize: "calc(10px + 2vmin)",
    textAlign: "left",
    textDecoration: "none"
  },
  dashed: {
    backgroundImage: `url( "data:image/svg+xml,%3csvg width='100%25' height='100%25' xmlns='http://www.w3.org/2000/svg'%3e%3crect width='100%25' height='100%25' fill='none' stroke='%23038FF5FF' stroke-width='4' stroke-dasharray='4%2c2' stroke-dashoffset='0' stroke-linecap='butt'/%3e%3c/svg%3e")`,
    height: "1px",
    width: "20px",
    marginRight: "15px"
  },

  ...globalStyles
}));

// affiche un graph sur niveau de nappe / riviere
export function WaterGraph({waterData}) {
  const classes = useStyles();
  const {isDesktop} = useMediaQueries();
  const urlSearchEntries = useUrlQuery().entries();

  let history = useHistory();
  // on recupere le type de graphique passé en parametre, riviere ou nappe
  const graphType = useUrlQuery().get("type");

  // si riverOrGround === true alors charge les données de "river" sinon pour "ground"; pour pouvoir basculer entre les deux via UI
  const [riverOrGround, setRiverOrGround] = useState(graphType === "nappe");
  // init useState ne met pas à jour en cas de changement
  if (riverOrGround != (graphType === "riviere")) {
    setRiverOrGround(graphType === "riviere");
  }

  function handleSwitchSetValue(newValue) {
    history.push(createAnchorLink(urlSearchEntries, newValue ? "riviere" : "nappe", false));
    setRiverOrGround(newValue);
  }

  const {xaxisData, chartData, average} = riverOrGround
    ? formatRiverObservation(waterData?.riverObservation)
    : formatGroundWaterObservation(waterData?.groundWaterObservation);

  return (
    <div id="graph" className={clsx(classes.container, !isDesktop && classes.containerMob)}>
      <Grid
        container
        direction="row"
        justify="space-between"
        alignItems="stretch"
        className={clsx(isDesktop ? classes.padding15B50 : classes.padding15)}>
        <Grid item xs={6}>
          <ButtonBase type="submit" aria-label="search" onClick={() => null}>
            <a
              target="_blank"
              href={
                riverOrGround
                  ? waterData?.riverObservation?.station?.pageUrl
                  : waterData?.groundWaterObservation?.station?.pageUrl
              }
              className={clsx(classes.flexRow, classes.whiteText)}>
              <InfoOutlinedIcon className={classes.paddingR15} />
              {riverOrGround ? (
                <div>
                  {(waterData?.riverObservation?.station?.label &&
                    upperFirstLetter(waterData?.riverObservation?.station?.label)) ||
                    "Dordogne"}
                  <span className={classes.cityName}>
                    {" "}
                    à {upperFirstLetter(waterData?.riverObservation?.station?.city)}
                  </span>
                </div>
              ) : (
                <span>{upperFirstLetter(waterData?.groundWaterObservation?.station?.aquiferLabel)}</span>
              )}
            </a>
          </ButtonBase>
          {/*
          {riverOrGround ? (
            <span>
              {(waterData?.riverObservation?.station?.label &&
                upperFirstLetter(waterData?.riverObservation?.station?.label)) ||
                "Dordogne"}
              <span className={classes.cityName}>
                {" "}
                à {upperFirstLetter(waterData?.riverObservation?.station?.city)}
              </span>
            </span>
          ) : (
            <span>{upperFirstLetter(waterData?.groundWaterObservation?.station?.aquiferLabel)}</span>
          )}
         */}
        </Grid>
        <Grid item xs={6}>
          <Grid container direction="row" justify="flex-end" alignItems="center">
            <SwitchButton value={riverOrGround} setValue={handleSwitchSetValue} isDesktop={isDesktop} />
          </Grid>
        </Grid>
      </Grid>

      <div className={clsx(classes.paddingL15, classes.whiteText12)}>
        {riverOrGround ? _riverObservation.label : "Niveau en mètres"}
      </div>
      <div className={clsx(isDesktop ? classes.graphSize : classes.graphSizeMob)}>
        <ResponsiveContainer height="100%" width="100%">
          <AreaChart data={chartData} margin={{top: 30, right: 0, bottom: 50, left: 0}}>
            <defs>
              <linearGradient id="colorLevel" x1="0" y1="0" x2="0" y2="1">
                <stop offset="5%" stopColor={Config.colors.lightBlue} stopOpacity={0.8} />
                <stop offset="85%" stopColor={Config.colors.lightBlue} stopOpacity={0} />
              </linearGradient>
            </defs>
            <XAxis
              dataKey="dateForxAxis"
              ticks={xaxisData}
              tick={{angle: -45, fontFamily: "Selawik !important", fontSize: 12, fill: "white"}}
              textAnchor="end"
            />
            <YAxis
              // width={50}
              // tickFormatter={(v) => (riverOrGround ? v + " " + _riverObservation.unit : v + " m")}
              tick={{fontFamily: "Selawik !important", fontSize: 12, fill: "white"}}
              domain={["dataMin - dataMin/10", "dataMax + dataMin/10"]}
            />
            <CartesianGrid horizontal={false} strokeDasharray="4 25" />
            <Area type="monotone" dataKey="level" stroke="#1E80CA" fillOpacity={1} fill="url(#colorLevel)" />
            <Tooltip content={<CustomTooltip riverOrGround={riverOrGround} />} />
            <ReferenceLine y={average} stroke={Config.colors.lightBlue} strokeDasharray="4 2" />
          </AreaChart>
        </ResponsiveContainer>
      </div>
      <div className={clsx(classes.flexRow, classes.legendText)}>
        <div className={classes.dashed} />

        {riverOrGround ? (
          <div>Débit moyen sur 1 mois : {average + " " + _riverObservation.unit}</div>
        ) : (
          <div>Niveau moyen sur 1 ans : {average + " m"}</div>
        )}
      </div>
    </div>
  );
}

/** affiche un tooltip au survol du graph */
function CustomTooltip(props) {
  const {active, payload, riverOrGround} = props;

  if (active && payload?.[0]?.payload) {
    const classes = useStyles();

    const {level, date} = payload?.[0]?.payload;
    return (
      <div className={classes.tooltip}>
        Le {dayjs(date).format("DD/MM/YYYY")} <br />à {dayjs(date).format("hh:mm")} <br />
        {riverOrGround ? (
          <span>
            le débit était de {level} {_riverObservation.unit}
          </span>
        ) : (
          <span>le niveau était à {level + " m"}</span>
        )}
      </div>
    );
  } else {
    return null;
  }
}

/**
 * format les données pour la nappe
 * @param {*} groundWaterObservation
 */
function formatGroundWaterObservation(groundWaterObservation) {
  let daysSet = new Set();

  // .slice() pour créer une copie sinon cela retourne le données à chaque click sur le switch
  let chartData = groundWaterObservation?.levels
    ?.slice()
    .reverse()
    .map((item) => {
      let dateForxAxis = dayjs(item.date).format("DD/MM/YY");
      daysSet.add(dateForxAxis);
      return {level: parseFloat(item.level), dateForxAxis, date: item.date};
    });
  return {xaxisData: Array.from(daysSet), chartData, average: groundWaterObservation?.mean};
}

/** format les données sur la riviere  */
function formatRiverObservation(riverObservation) {
  let daysSet = new Set();
  // .slice() pour créer une copie sinon cela retourne le données à chaque click sur le switch
  let chartData = riverObservation?.levels
    ?.slice()
    .reverse()
    .map((item) => {
      let dateForxAxis = dayjs(item.date).format("DD/MM");
      daysSet.add(dateForxAxis);
      let level = parseFloat(item.level);
      return {level, dateForxAxis, date: item.date};
    });

  return {xaxisData: Array.from(daysSet), chartData, average: riverObservation?.mean};
}

/**
 * type de donnée souhaité pour les rivières
 * Q : débit, dont l'unité est en L/s   {type: "Q", unit: "L/s", label : "Débit en L/s"}
 * H : hauteur en mm      {type: "H", unit: "mm" , label : "Hauteur en mm"}
 */
export const _riverObservation = {type: "Q", unit: "L/s", label: "Débit en L/s"};
