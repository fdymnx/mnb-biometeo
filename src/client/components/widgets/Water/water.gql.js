import gql from "graphql-tag";

// pour avoir les données des graphiques (G)
export const gqlWater = gql`
  query RiverAndGroundWaterObservation(
    $geonamesId: ID!
    $riverStepUnit: RiverObservationStepUnit
    $riverObservationType: RiverObservationType
    $groundWaterStepUnit: GroundWaterObservationStepUnit
  ) {
    riverObservation(geonamesId: $geonamesId, observationType: $riverObservationType, stepUnit: $riverStepUnit) {
      mean
      lastLevel
      trend
      station {
        label
        city
        pageUrl
      }
      levels {
        date
        level
      }
    }

    groundWaterObservation(geonamesId: $geonamesId, stepUnit: $groundWaterStepUnit) {
      mean
      trend
      levels {
        date
        level
      }
      station {
        pageUrl
        aquiferLabel
        stationType
      }
    }
  }
`;
