import React from "react";
import {withStyles} from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Config from "../../../Config";

const CustomButton = withStyles({
  root: {
    boxShadow: "none",
    textTransform: "none",
    fontSize: 16,
    padding: "6px 30px",
    border: "1px solid",
    lineHeight: 1.5,
    borderRadius: "50px",
    // width: "100%",
    backgroundColor: Config.colors.white,
    borderColor: Config.colors.white,
    color: Config.colors.blue,
    fontFamily: [
      '"Segoe UI"',
      "Arial",
      "sans-serif",
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"'
    ].join(","),
    "&:hover": {
      backgroundColor: Config.colors.white,
      borderColor: Config.colors.white,
      boxShadow: "none"
    },
    "&:active": {
      backgroundColor: Config.colors.white,
      borderColor: Config.colors.white,
      borderColor: "#005cbf",
      boxShadow: "none"
    }
  }
})(Button);

export function RoundedButton({onClick, children}) {
  return (
    <CustomButton variant="contained" color="primary" disableRipple disableElevation={true} onClick={onClick}>
      {children}
    </CustomButton>
  );
}
