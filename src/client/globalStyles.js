import Config from "./Config";

export default {
  horizontalBar: {
    borderBottom: `2px solid rgba(255, 255, 255, 0.12)`,
    width: "95%",
    margin: "auto",
    marginBottom: 2,
    padding: "0px !important"
  },
  height100: {
    height: "100%"
  },
  fullWidthNoMP: {
    width: "100%",
    margin: 0,
    padding: 0
  },
  width100: {
    width: "100%"
  },
  horizontalBarBlue: {
    borderBottom: `1px solid ${Config.colors.blue}`,
    width: "95%"
  },
  horizontalBarLightBlue: {
    borderBottom: `2px solid rgba(13, 53, 78, 0.2)`,
    width: "95%"
  },
  marginTP10: {
    marginTop: 2,
    marginBottom: 10
  },
  verticalSpacer: {
    margin: 7
  },
  verticalBar: {
    border: `1px solid rgba(255, 255, 255, 0.12)`,
    height: "17px"
  },
  verticalBar2: {
    border: `1px solid rgba(255, 255, 255, 0.35)`,
    height: "80%",
    margin: "auto"
  },
  responsiveImg: {
    width: "100%",
    height: "100%",
    backgroundColor: "rgba(13, 53, 78, 0.2)"
  },
  marginTop15: {
    marginTop: 15
  },
  zIndex2: {
    zIndex: 2
  },
  flexRow: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-start"
  },
  flexRowStart: {
    display: "flex",
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "flex-start"
  },
  flexRowEnd: {
    display: "flex",
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "flex-end"
  },
  flexRowSB: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between"
  },
  flexColumnCC: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center"
  },
  flexCenter: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  noMP: {
    margin: "0px ! important",
    padding: "0px ! important"
  },
  buttonMinWidth: {
    minWidth: "48px ! important" // 32px
  },
  blueText: {
    color: Config.colors.blue
  },
  blueGradient: {
    zIndex: 10,
    background: "rgb(0,45,75)",
    background: `linear-gradient(9deg, ${Config.colors.blueWithOpacity} 28%, rgba(0,45,75,0.02874653279280459) 100%)`
  },
  borderRed: {
    border: "1px solid red"
  },
  cursor: {cursor: "pointer"},
  textDecorationNone: {textDecoration: "none", color: Config.colors.blue},
  observationBlueButton: {
    padding: 14,
    paddingLeft: 30,
    width: "100%",
    minHeight: "80px",
    color: Config.colors.white,
    backgroundColor: Config.colors.darkBlue,
    cursor: "pointer",
    borderRadius: "10px",
    boxShadow: "0 2px 8px rgba(0, 0, 0, 0.15)",
    textDecoration: "none"
  },
  observationWhiteButton: {
    padding: 14,
    paddingLeft: 30,
    width: "100%",
    minHeight: "80px",
    color: Config.colors.darkBlue,
    backgroundColor: Config.colors.white,
    cursor: "pointer",
    borderRadius: "10px",
    boxShadow: "0 2px 8px rgba(0, 0, 0, 0.15)",
    textDecoration: "none"
  },
  drawerPaddingTitle: {
    paddingTop: 70,
    paddingBottom: 40
  },
  drawerHorizontalPadding: {
    marginLeft: Config.leftMarginDesktop,
    marginRight: Config.leftMarginDesktop //`calc( ${Config.leftMarginDesktop} /2)`,
  },
  drawerHorizontalPaddingMobile: {
    paddingLeft: Config.leftMarginMobile,
    paddingRight: Config.leftMarginMobile
  },
  sideMenuMarginRightIcon: {
    paddingRight: "min(28px , 5vw)",
    color: Config.colors.white
  },
  sideMenuMarginRightIconMob: {
    paddingRight: "5vw"
  },
  sideMenuMarginLeftIcon: {
    paddingLeft: "min(28px , 10vw)"
  },
  sideMenuMarginLeftIconMob: {
    paddingLeft: "10vw"
  },
  nowrap: {
    overflow: "hidden",
    textOverflow: "ellipsis",
    whiteSpace: "nowrap"
  },
  fullHeight: {
    minHeight: "80vh"
  },
  horizontalSpacer: {
    marginTop: Config.categorie.gridSpacing * 8
  },
  segoeBold: {fontFamily: "Segoe UI Bold !important"},
  segoeItalic: {fontFamily: "Segoe UI Italic !important"},
  segoeBoldItalic: {fontFamily: "Segoe UI Bold Italic !important"},
  segoeSemiBold: {fontFamily: "Segoe UI Semi Bold !important"},

  selawik: {fontFamily: "Selawik !important"},
  selawikRegularBold: {fontFamily: "Selawik Regular Bold !important"},
  selawikBold: {fontFamily: "Selawik Bold !important"},
  selawikItalic: {fontFamily: "Selawik !important", fontStyle: "italic"},
  selawikBoldItalic: {fontFamily: "Selawik Bold !important", fontStyle: "italic"},
  selawikSemiBold: {fontFamily: "Selawik Semibold !important"},
  selawikLight: {fontFamily: "Selawik Light !important"},

  fs8: {fontSize: "8px", fontFamily: "Segoe UI Regular"},
  fs10: {fontSize: "10px", fontFamily: "Segoe UI Regular"},
  fs12: {fontSize: "12px", fontFamily: "Segoe UI Regular"},
  fs13: {fontSize: "13px", fontFamily: "Segoe UI Regular"},
  fs14: {fontSize: "14px", fontFamily: "Segoe UI Regular"},
  fsrem14: {fontSize: "1.4rem", fontFamily: "Segoe UI Regular"},
  fs15: {fontSize: "15px", fontFamily: "Segoe UI Regular"},
  fs16: {fontSize: "16px", fontFamily: "Segoe UI Regular"},
  fs17: {fontSize: "17px", fontFamily: "Segoe UI Regular"},
  fs19: {fontSize: "19px", fontFamily: "Segoe UI Regular"},
  fs20: {fontSize: "20px", fontFamily: "Segoe UI Regular"},
  fs21: {fontSize: "21px", fontFamily: "Segoe UI Regular"},
  fs25: {fontSize: "25px", fontFamily: "Segoe UI Regular"},
  fs27: {fontSize: "27px", fontFamily: "Segoe UI Regular"},
  fs29: {fontSize: "29px", fontFamily: "Segoe UI Regular"},
  fs30: {fontSize: "30px", fontFamily: "Segoe UI Regular"},
  fs31: {fontSize: "31px", fontFamily: "Segoe UI Regular"},
  fs32: {fontSize: "32px", fontFamily: "Segoe UI Regular"},
  fs34: {fontSize: "34px", fontFamily: "Segoe UI Regular"},
  fs35: {fontSize: "35px", fontFamily: "Segoe UI Regular"},
  fs36: {fontSize: "36px", fontFamily: "Segoe UI Regular"},
  fs38: {fontSize: "3.8rem", fontFamily: "Segoe UI Regular"},
  fs40: {fontSize: "40px", fontFamily: "Segoe UI Regular"},
  fs57: {fontSize: "57px", fontFamily: "Segoe UI Regular"},
  fs66: {fontSize: "66px", fontFamily: "Segoe UI Regular"},
  fs75: {fontSize: "75px", fontFamily: "Segoe UI Regular"},
  fs85: {fontSize: "85px", fontFamily: "Segoe UI Regular"},

  fsMax16: {
    fontSize: "min(3vw,16px)"
  },
  fsMax17: {
    fontSize: "min(4vw,17px)"
  },
  fsMax34: {
    fontSize: "min(5vw,34px)"
  },
  fsMax36: {
    fontSize: "min(6vw,36px)"
  }
};
