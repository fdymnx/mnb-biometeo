import React, {createContext, useContext} from "react";
import {func, array} from "prop-types";

//contiendra la liste des données pour les drawers, il est possible d'en avoir plusieurs superposé
let _drawers = [];

function getDrawers() {
  return _drawers;
}

/**
 *
 * @param {string} type : push ou pop
 * @param {contenu pour le drawer} value
 */
function setDrawers(type, value) {
  if (type === "pop") {
    _drawers.pop();
  } else if (type === "push") {
    _drawers.push(value);
  }
}

// creating the context
const DrawerContext = createContext(null);

// the provider avoid to write <DrawerContext.provider /> for main classe
function DrawerContextProvider({contextDrawerValues, contextDrawerSetValues, children}) {
  const {Provider} = DrawerContext;
  return <Provider value={{contextDrawerValues, contextDrawerSetValues}}>{children}</Provider>;
}

// à utiliser dans les classes enfants qui veulent récupérer le context
function useDrawerContext() {
  const {contextDrawerValues, contextDrawerSetValues} = useContext(DrawerContext) || {};

  return {contextDrawerValues, contextDrawerSetValues};
}

export {useDrawerContext, DrawerContext, DrawerContextProvider};

DrawerContextProvider.propTypes = {
  contextDrawerSetValues: func.isRequired,
  contextDrawerValues: array.isRequired
};
