import React, {createContext, useContext} from "react";
import {func, object} from "prop-types";

//localStorage name
// un entier est intégré dans le nom pour eviter les bugs lors de changement de structure des news, ex changement d'url etc
const _FN = "favoritenews_2";
//value getter from local stoage
function getFavoritesNews() {
  const a = localStorage.getItem(_FN);
  return a ? JSON.parse(a) : {};
}
//new value setter for localstorage
function setFavoritesNews(newValue) {
  localStorage.setItem(_FN, JSON.stringify(newValue));
}

// creating the context and init it from readed value from localstorage
const FavoritesNewsContext = createContext(getFavoritesNews());

// the provider avoid to write <FavoritesNewsContext.provider /> for main classe
function FavoritesNewsContextProvider({contextFNValues, contextFNSetValues, children}) {
  const {Provider} = FavoritesNewsContext;
  return <Provider value={{contextFNValues, contextFNSetValues}}>{children}</Provider>;
}

// à utiliser dans les classes enfants qui veulent récupérer le context
function useFavoritesNewsContext() {
  const {contextFNValues, contextFNSetValues} = useContext(FavoritesNewsContext) || {};

  return {contextFNValues, contextFNSetValues};
}

export {
  useFavoritesNewsContext,
  FavoritesNewsContext,
  FavoritesNewsContextProvider,
  getFavoritesNews,
  setFavoritesNews
};

FavoritesNewsContextProvider.propTypes = {
  contextFNSetValues: func.isRequired,
  contextFNValues: object.isRequired
};
