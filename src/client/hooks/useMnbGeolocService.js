import {GeolocService, useGeolocService} from "@mnemotix/synaptix-client-toolkit";
import {useApolloClient} from "@apollo/react-hooks";

/**
 * This this a wrapper hook to circumvent useGeolocService search to Dordogne cities.
 * @param qs
 * @return {{selectedPlace: Object, searchedPlaces: Array<Object>, proceedAutoPlace: Function, setPlace: Function, setPlaceFromId: Function, loading: Boolean, error: String}}
 */
export function useMnbGeolocService({qs} = {}) {
  const apolloClient = useApolloClient();
  // Dordogne bounding box
  const bbox = [45.7146, -0.042, 44.5707, 1.4483];
  const defaultPlace = {
    id: "geonames:6429478",
    name: "Périgueux"
  };

  return useGeolocService({
    geolocService: getGeolocService({
      apolloClient,
      defaultPlace,
      bbox
    }),
    qs,
    featureCodes: ["ADM4"], // Returns only citites
    countryName: "FR",
    bbox,
    defaultPlace
  });
}

let geolocService;

/**
 * Get a geoloc service.
 *
 * We use singleton pattern to share it in the whole application.
 *
 * @param defaultPlace
 * @param apolloClient
 * @param bbox
 * @return {GeolocService}
 */
function getGeolocService({defaultPlace, apolloClient, bbox}) {
  if (!geolocService) {
    geolocService = new GeolocService({
      defaultPlace,
      apolloClient,
      bbox
    });
  }
  return geolocService;
}
