import {useLocation} from "react-router-dom";

/**
 * Return a URLSearchParams object from location.
 *
 * @return {URLSearchParams}
 */
export function useUrlQuery() {
  return new URLSearchParams(useLocation().search);
}
