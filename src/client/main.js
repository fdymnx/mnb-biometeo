/*
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import "./App.css";
import React from "react";
import ReactDOM from "react-dom";
import {BrowserRouter} from "react-router-dom";
import {ApolloProvider} from "@apollo/react-hooks";

import {BioMeteo} from "./BioMeteo";
import {i18nInitialize} from "../locales/i18nInitialize";

import {LoadingSplashScreen} from "./components/widgets/LoadingSplashScreen";

import {getApolloClient} from "./utilities/getApolloClient";
import {SnackbarProvider} from "notistack";
import {useSnackbar} from "notistack";

let reactRootElement = document.getElementById("react-root");

const ApolloContainer = ({i18n, children}) => {
  const {enqueueSnackbar} = useSnackbar();
  return <ApolloProvider client={getApolloClient({i18n, enqueueSnackbar})}>{children}</ApolloProvider>;
};

i18nInitialize().then((i18n) => {
  ReactDOM.render(
    <BrowserRouter>
      <SnackbarProvider maxSnack={3}>
        <ApolloContainer i18n={i18n}>
          <React.Suspense fallback={<LoadingSplashScreen />}>
            <BioMeteo />
          </React.Suspense>
        </ApolloContainer>
      </SnackbarProvider>
    </BrowserRouter>,
    reactRootElement
  );
});
