/*
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import React from "react";
import {ApolloClient} from "apollo-client";
import {HttpLink} from "apollo-link-http";
import {onError} from "apollo-link-error";
import {ApolloLink, concat} from "apollo-link";
import {InMemoryCache, IntrospectionFragmentMatcher} from "apollo-cache-inmemory";
import {FragmentTypes} from "../../generated/FragmentTypes";
import fetch from "unfetch";

const fragmentMatcher = new IntrospectionFragmentMatcher({
  introspectionQueryResultData: FragmentTypes
});

export function getApolloClient({i18n, enqueueSnackbar} = {}) {
  if (process.env.NODE_ENV === "test") {
    let miniStackTrace = new Error().stack.split("\n").splice(1, 5).join("\n");
    console.warn(
      "You are using the default apollo client ApolloClient in a test environment. This probably won't work " +
        "because the default apollo client hasn't access to the graphQL mocks (as provided by apollo MockedProvider). You should either mock " +
        "the service that use the apollo client in your test suite, or inject this service with a mocked apollo client such as the one " +
        "defined at /jest/utilities/MockedApolloClient.js\n\n" +
        miniStackTrace
    );
  }

  // const httpLink = new HttpLink({uri: "/graphql"});
  const httpLink = new HttpLink({uri: "/graphql", fetch});

  // middleware for language
  const languageMiddleware = new ApolloLink((operation, forward) => {
    // add the authorization to the headers
    operation.setContext(({headers = {}}) => ({
      headers: {
        ...headers,
        Lang: i18n.language || "fr"
      }
    }));
    return forward(operation);
  });

  // middleware for gql error
  const errorLink = onError(({graphQLErrors, networkError}) => {
    if (graphQLErrors) {
      graphQLErrors.map(({message, extraInfos, stack, path}) => {
        const ignore = ["NOT AUTHENTICATED", "USER_MUST_BE_AUTHENTICATED", "INVALID_CREDENTIALS"];
        if (ignore.includes(message.toUpperCase())) {
          // what to do if user not authenticated?
          // route to login is already done
        } else {
          if (typeof extraInfos === "object" && extraInfos !== null) {
            extraInfos = JSON.stringify(extraInfos);
          }
          console.log("-------------------------");
          console.log();
          console.log(
            `[GraphQL error]: Message: ${message}, extraInfos: ${extraInfos}, 
            stack:${JSON.stringify(stack, null, 2)}, Path: ${path}`
          );
          console.log();
          console.log("//////////////////////////");

          /* enqueueSnackbar(message, {
            content: (id, message) => {
              return (
                <SnackErrorMessage
                  id={id}
                  message={message}
                  error={
                    <>
                      In path : {(path || []).join(">")}
                      <br />
                      Stack :{""}
                      {(stack || []).map((line, index) => (
                        <div key={index}>{line}</div>
                      ))}
                    </>
                  }
                />
              );
            },
            persist: true,
            anchorOrigin: {
              vertical: "bottom",
              horizontal: "right"
            }
          });*/
        }
      });
    }
    if (networkError) console.log(`[Network error]: ${networkError}`);
  });

  // create the client and pass the middleware et cache config
  const apolloClient = new ApolloClient({
    link: concat(languageMiddleware, concat(errorLink, httpLink)),
    cache: new InMemoryCache({fragmentMatcher}),
    defaultOptions: {
      watchQuery: {
        query: "cache-and-network"
      }
    }
  });

  i18n.on("languageChanged", (lang) => {
    apolloClient.resetStore();
  });

  return apolloClient;
}
