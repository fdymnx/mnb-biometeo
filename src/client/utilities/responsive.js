import {useMediaQuery} from "react-responsive";
import Config from "../Config";
/**
 * Hook returning the object
 *
 * {
 *   isDesktop: <boolean> w > 600px
 *   isMobile : <boolean> 360px < w < 600px
 *   isSmallMobile: <boolean>  w < 360px
 * }
 * usage :
 * import {useMediaQueries} from '../../utilities/responsive';
 * const {isDesktop,isMobile,isSmallMobile} = useMediaQueries();
 *
 */
export function useMediaQueries() {
  const isSmallMobile = useMediaQuery({maxWidth: Config.breakpoints.values.sm});
  const isMobile = useMediaQuery({
    minWidth: Config.breakpoints.values.sm,
    maxWidth: Config.breakpoints.values.md - 1
  });

  return {
    isSmallMobile,
    isMobile,
    isDesktop: !isSmallMobile && !isMobile
  };
}

//https://www.npmjs.com/package/react-device-detect
/*


import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { findLast } from 'ramda';

const useResponsive = () => {
  const theme = useTheme();

  const matches = {
    xs: useMediaQuery(theme.breakpoints.up('xs')),
    sm: useMediaQuery(theme.breakpoints.up('sm')),
    md: useMediaQuery(theme.breakpoints.up('md')),
    lg: useMediaQuery(theme.breakpoints.up('lg')),
    xl: useMediaQuery(theme.breakpoints.up('xl')),
  };

  return function(responsiveValues: { breakpoint }) {
    const match = findLast(
      (breakpoint) => matches[breakpoint] && responsiveValues[breakpoint] != null,
      theme.breakpoints.keys,
    );

    return match && responsiveValues[match];
  };
};

export default useResponsive;

https://github.com/mui-org/material-ui/issues/6140
*/
