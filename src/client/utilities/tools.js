/*
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * A.I
 */

import React from "react";
import dayjs from "dayjs";
/**
 * upper case first letter of a string
 * @param {*} string
 */
export function upperFirstLetter(string) {
  if (!string) {
    return "";
  }
  string = string.trim().toLowerCase();
  return string.charAt(0).toUpperCase() + string.slice(1);
}

/**
 * create the url to taxon details
 * @param {*} id
 * @param {*} addLocation
 * @param {*} addCanGoBack, si dans l'url le paramètre back=t alors au clic dans la zone grisée de la popup appelera history.goBack() sinon history.push("/")
 */
export function createTaxonDetailsUrl(id, addLocation, addCanGoBack) {
  let url = addLocation ? window.location.origin : "";
  url += "/taxon/?taxonid=" + encodeURIComponent(id);
  if (addCanGoBack) {
    url += "&back=t";
  }
  return url;
}
// arrondi les float à l'entier, pour les températures par exemple
export function toInt(string) {
  if (string) {
    return parseFloat(string).toFixed(0);
  } else return 0;
}

const todayInt = dayjs().day();
/**
 * recupère le nom du jour
 * @param {int} day numéro du jour dans la semaine
 * @param {bool} enableToday : retourne "aujourd'hui"
 */
export function dayFromWeek(day, enableToday = false) {
  if (enableToday && day === todayInt) {
    return "Aujourd'hui";
  }
  const days = ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"];
  return days[day];
}

/**
 * nettoie un nom à afficher,
 * si il contient des virgules prend uniquement la partie avant la première virgule
 * si il contient des parenthès ne prend que la partie avant la première parenthèse,
 * et ne retourne que les 35 premiers caractères
 * @param {*} text
 */
export function cleanTextToDisplay(text, split = true, slice = true) {
  if (text && text.length > 0) {
    if (split) {
      text = text.split(",")[0];
      text = text.split("(")[0];
    }
    if (slice && text.length > 35) {
      text = text.slice(0, 35) + "...";
    }
    return text;
  }
  return text;
}

export function metresOrKm(distance) {
  if (distance) {
    distance = toInt(distance);
    if (distance > 999) {
      return toInt(distance / 1000) + " Km";
    }

    return distance + " m";
  }
  return distance;
}
