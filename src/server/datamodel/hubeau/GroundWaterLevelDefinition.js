import {
  LiteralDefinition,
  ModelDefinitionAbstract,
} from "@mnemotix/synaptix.js";
import {GroundWaterLevelGraphQLDefinition} from "./graphql/GroundWaterLevelGraphQLDefinition";

export class GroundWaterLevelDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getNodeType() {
    return "GroundWaterLevel";
  }

  static getIndexType(){
    return "nappes";
  }

  /**
   * @inheritDoc
   */
  static getGraphQLDefinition() {
    return GroundWaterLevelGraphQLDefinition;
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    return [
      new LiteralDefinition({
        literalName: "bssCode",
        description: "Station code",
        rdfDataProperty: "",
      }),
      new LiteralDefinition({
        literalName: "date",
        pathInIndex: "date_mesure",
        description: "Observation date",
        rdfDataProperty: "",
      }),
      new LiteralDefinition({
        literalName: "level",
        pathInIndex: "niveau_nappe_eau",
        description: "Observation level"
      })
    ]
  }
}
