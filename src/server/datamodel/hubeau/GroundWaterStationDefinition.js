import {
  LiteralDefinition,
  ModelDefinitionAbstract,
  FilterDefinition, SortingDefinition
} from "@mnemotix/synaptix.js";
import {GroundWaterStationGraphQLDefinition} from "./graphql/GroundWaterStationGraphQLDefinition";

export class GroundWaterStationDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "mnb:GroundWaterStation";
  }

  /**
   * @return {string}
   */
  static getIndexType(){
    return "groundwaterstations";
  }

  /**
   * @inheritDoc
   */
  static getGraphQLDefinition() {
    return GroundWaterStationGraphQLDefinition;
  }

  /**
   * @inheritDoc
   */
  static getLiterals(){
    return [
      new LiteralDefinition({
        literalName: "label",
        description: "Station label",
        rdfDataProperty: "http://www.w3.org/2000/01/rdf-schema#label",
      }),
      new LiteralDefinition({
        literalName: "bssCode",
        description: "BSS code",
        rdfDataProperty: "http://mnb.dordogne.fr/ontology/bssCode",
      }),
      new LiteralDefinition({
        literalName: "aquiferLabel",
        pathInIndex: "labelAQUI",
        description: "Aquifer label",
        rdfDataProperty: "http://mnb.dordogne.fr/ontology/labelAQUI",
      }),
      new LiteralDefinition({
        literalName: "stationType",
        description: "Station type",
        rdfDataProperty: "http://mnb.dordogne.fr/ontology/stationType",
      }),
      new LiteralDefinition({
        literalName: "pageUrl",
        pathInIndex: "page",
        description: "Station page URI",
        rdfDataProperty: "http://xmlns.com/foaf/0.1/page",
      })
    ]
  }

  /**
   * @inheritDoc
   */
  static getFilters() {
    return [
      ...super.getFilters(),
      new FilterDefinition({
        filterName: "closestFromGeoCoords",
        indexFilter: ({ lat, lon, distance }) => ({
          geo_distance: {
            distance: distance || "500km",
            hasGeometry: { lat, lon }
          }
        })
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getSortings() {
    return [
      new SortingDefinition({
        sortingName: "closestFromGeoCoords",
        indexSorting: ({ lat, lon, direction, unit, mode, distanceType }) => ({
          _geo_distance: {
            hasGeometry: {
              lat,
              lon
            },
            order: direction || "asc",
            unit: unit || "km",
            mode: mode || "min",
            distance_type: distanceType || "arc",
            ignore_unmapped: true
          }
        })
      })
    ];
  }
}
