import {
  LiteralDefinition,
  ModelDefinitionAbstract,
} from "@mnemotix/synaptix.js";
import {RiverLevelGraphQLDefinition} from "./graphql/RiverLevelGraphQLDefinition";

export class RiverLevelDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getNodeType() {
    return "RiverLevel";
  }

  static getIndexType(){
    return "rivieres";
  }

  /**
   * @inheritDoc
   */
  static getGraphQLDefinition() {
    return RiverLevelGraphQLDefinition;
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    return [
      new LiteralDefinition({
        literalName: "codeSite",
        description: "Station code",
        rdfDataProperty: "",
      }),
      new LiteralDefinition({
        literalName: "date",
        pathInIndex: "date_obs",
        description: `
Observation date

The format will depend on "stepUnit" parameter ;
- Day  => YYYY-MM-DD
- Week => YYYY-MM (ww) where (ww) is the week number of the year @see https://day.js.org/docs/en/plugin/advanced-format
- Month => YYYY-MM
        
        `,
        rdfDataProperty: "",
      }),
      new LiteralDefinition({
        literalName: "level",
        pathInIndex: "resultat_obs",
        description: `
Observation level 

The unit will depend on "observationType" parameter:

- H => mm
- Q => m3/h        
`
      })
    ]
  }
}
