import {
  ModelDefinitionAbstract,
} from "@mnemotix/synaptix.js";
import {RiverObservationGraphQLDefinition} from "./graphql/RiverObservationGraphQLDefinition";

export class RiverObservationDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getNodeType() {
    return "RiverObservation";
  }

  /**
   * @inheritDoc
   */
  static getGraphQLDefinition() {
    return RiverObservationGraphQLDefinition;
  }
}
