import {
  LiteralDefinition,
  ModelDefinitionAbstract,
  FilterDefinition, SortingDefinition
} from "@mnemotix/synaptix.js";
import {RiverStationGraphQLDefinition} from "./graphql/RiverStationGraphQLDefinition";

export class RiverStationDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "mnb:RiverStation";
  }

  /**
   * @return {string}
   */
  static getIndexType(){
    return "riverstations";
  }

  /**
   * @inheritDoc
   */
  static getGraphQLDefinition() {
    return RiverStationGraphQLDefinition;
  }

  /**
   * @inheritDoc
   */
  static getLiterals(){
    return [
      new LiteralDefinition({
        literalName: "label",
        pathInIndex: "isSampleOf",
        description: "Station label",
        rdfDataProperty: "http://www.w3.org/ns/sosa/isSampleOf / http://www.w3.org/2000/01/rdf-schema#label",
      }),
      new LiteralDefinition({
        literalName: "stationCode",
        description: "Station code",
        rdfDataProperty: "http://mnb.dordogne.fr/ontology/stationCode",
      }),
      new LiteralDefinition({
        literalName: "city",
        description: "Station city",
        rdfDataProperty: "http://ns.mnemotix.com/ontologies/2019/8/generic-model/city",
      }),
      new LiteralDefinition({
        literalName: "pageUrl",
        pathInIndex: "isSampleOfURI",
        description: "Station page URI",
        rdfDataProperty: "http://www.w3.org/ns/sosa/isSampleOf",
      })
    ]
  }

  /**
   * @inheritDoc
   */
  static getFilters() {
    return [
      ...super.getFilters(),
      new FilterDefinition({
        filterName: "closestFromGeoCoords",
        indexFilter: ({ lat, lon, distance }) => ({
          geo_distance: {
            distance: distance || "500km",
            hasGeometry: { lat, lon }
          }
        })
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getSortings() {
    return [
      new SortingDefinition({
        sortingName: "closestFromGeoCoords",
        indexSorting: ({ lat, lon, direction, unit, mode, distanceType }) => ({
          _geo_distance: {
            hasGeometry: {
              lat,
              lon
            },
            order: direction || "asc",
            unit: unit || "km",
            mode: mode || "min",
            distance_type: distanceType || "arc",
            ignore_unmapped: true
          }
        })
      })
    ];
  }
}
