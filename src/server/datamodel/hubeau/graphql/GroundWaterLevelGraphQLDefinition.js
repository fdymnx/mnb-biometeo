import {
  GraphQLTypeDefinition,
  GraphQLProperty,
} from "@mnemotix/synaptix.js";

export class GroundWaterLevelGraphQLDefinition extends GraphQLTypeDefinition {
  /**
   * Return list of properties to override
   * Throw error if one of the properties is already existing in related model
   *
   * @returns {GraphQLProperty[]}
   */
//   static getExtraProperties() {
//     return [
//       new GraphQLProperty({
//         name: "date",
//         description: `
// Observation date
//
// The format will depend on "stepUnit" parameter ;
// - Day  => YYYY-MM-DD
// - Week => YYYY-MM (ww) where (ww) is the week number of the year @see https://day.js.org/docs/en/plugin/advanced-format
// - Month => YYYY-MM
//
//         `,
//         type: "String",
//         typeResolver: (hubEauResult) => hubEauResult.date
//       }),
//       new GraphQLProperty({
//         name: "level",
//         description: "Observation level in meter",
//         type: "String",
//         typeResolver: (hubEauResult) => hubEauResult.level
//       })
//     ];
//   }
}


/**
 case "Day":
 roundedDate = dayjs(date_obs).format("YYYY-MM-DD");
 break;
 case "Week":
 roundedDate = dayjs(date_obs).format("YYYY-MM (ww)");
 break;
 case "Month":
 roundedDate = dayjs(date_obs).format("YYYY-MM");
 break;
 */