import meanBy from 'lodash/meanBy';
import round from 'lodash/round';

import { GraphQLTypeDefinition, GraphQLProperty } from '@mnemotix/synaptix.js';
import { RiverObservationGraphQLTypeQuery } from './RiverObservationGraphQLTypeQuery';

export class RiverObservationGraphQLDefinition extends GraphQLTypeDefinition {
  /**
   * @inheritDoc
   */
  static getTypeQuery() {
    return new RiverObservationGraphQLTypeQuery();
  }

  /**
   * Return list of properties to override
   * Throw error if one of the properties is already existing in related model
   *
   * @returns {GraphQLProperty[]}
   */
  static getExtraProperties() {
    return [
      new GraphQLProperty({
        name: 'mean',
        description: 'Mean of levels',
        type: 'Float',
        typeResolver: (observation) =>
          round(
            meanBy(observation.levels, ({ level }) => level),
            2
          ),
      }),
      new GraphQLProperty({
        name: 'lastLevel',
        description: 'Last observed level',
        type: 'Float',
        typeResolver: (observation) => observation.levels[0]?.level || 0,
      }),
      new GraphQLProperty({
        name: 'trend',
        description: 'Trend of level (stable|increasing|decreasing)',
        type: 'String',
        typeResolver: (observation) => {
          const currentLevel = observation.levels[0]?.level || 0;
          const lastLevel = observation.levels[1]?.level || currentLevel;

          if (currentLevel === lastLevel) {
            return 'stable';
          } else if (currentLevel > lastLevel) {
            return 'increasing';
          } else {
            return 'decreasing';
          }
        },
      }),
      new GraphQLProperty({
        name: 'station',
        description: 'River station definition',
        type: 'RiverStation',
        typeResolver: (observation) => observation.riverStation,
      }),
      new GraphQLProperty({
        name: 'levels',
        description: 'List of levels',
        type: '[RiverLevel]',
        typeResolver: (observation) => observation.levels,
      }),
    ];
  }

  /**
   * @inheritDoc
   */
  static getExtraGraphQLCode() {
    return `
      """ River observation enum values """
      enum RiverObservationType {
        """ Flow """
        Q 
        """ Height """
        H
      }
      
       """ River observation step units (gather values and compute mean) """
      enum RiverObservationStepUnit {
        """ Mean by month """
        Month
        
        """ Mean by week """
        Week
        
         """ Mean by day """
        Day
        
        """ Mean by hour """
        Hour
      }
    `;
  }
}
