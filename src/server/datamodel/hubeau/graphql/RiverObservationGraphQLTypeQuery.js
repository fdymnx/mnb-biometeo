import {
  GraphQLTypeQuery,
  SynaptixDatastoreSession,
  QueryFilter, Sorting
} from "@mnemotix/synaptix.js";
import dayjs from "dayjs";
import weekOfYear from "dayjs/plugin/weekOfYear";
import advancedFormat from "dayjs/plugin/advancedFormat";

import mean from "lodash/mean";
import round from "lodash/round";

import {RiverStationDefinition} from "../RiverStationDefinition";
import {geonamesClient} from "@mnemotix/synaptix-api-toolkit-geonames";
import {hubEauClient} from "../../../services/HubEauClient";
import {GroundWaterLevelDefinition} from "../GroundWaterLevelDefinition";
import {RiverLevelDefinition} from "../RiverLevelDefinition";

dayjs.extend(weekOfYear);
dayjs.extend(advancedFormat);

export class RiverObservationGraphQLTypeQuery extends GraphQLTypeQuery {
  /**
   * @inheritdoc
   */
  generateType(modelDefinition) {
    const graphQLType = modelDefinition.getGraphQLType();
    return this._wrapQueryType(`
      """
       This service returns a list of river levels filtered by following parameters :
       
       Parameters :
         - geonamesId: [REQUIRED] Geonames place id.
         - startDate: [optional] Start observation date (default to current day - 7 )
         - endDate: [optional]  End observation date (default to current day)
         - observationType: [optional] Observation type. "H" for height and "Q for flow rate. Default "H".
         - stepUnit: [optional]        Gather results and compute mean by step unit.  Default "Day".
      """
      ${this.generateFieldName(
        modelDefinition
      )}(
          """ Geonames place id """ 
          geonamesId:ID! 
          """ Start observation date (default to current day - 30 ) """ 
          startDate: String 
          """ End observation date (default to current day) """ 
          endDate:String 
          """ Observation type. H for height and Q for flow rate """ 
          observationType: RiverObservationType = H
          """ Gather results and compute mean by step unit.  Default "Day". """ 
          stepUnit: RiverObservationStepUnit = Day
        ): ${graphQLType}
    `);
  }

  /**
   * @inheritdoc
   */
  generateResolver(modelDefinition) {
    return this._wrapQueryResolver({
      /**
       * @param observationType
       * @param args
       */
      [this.generateFieldName(modelDefinition)]:
        /**
         * @param _
         * @param {string} geonamesId
         * @param {string} startDate
         * @param {number} endDate
         * @param {string} observationType
         * @param {string} stepUnit
         * @param {SynaptixDatastoreSession} synaptixSession
         * @param {object} args
         * @param {object} info
         */
        async (_, { geonamesId, startDate, endDate, observationType, stepUnit, ...args }, synaptixSession, info) => {
          const place = await geonamesClient.getPlaceById({ id: geonamesId });
          let riverStation;

          if (!["H", "Q"].includes(observationType)){
            observationType = "H";
          }

          // As the HubEau API only return results up to 1 month in the past,
          // we must control the endDate and startDate to not receive Error 400
          if(endDate &&  dayjs(endDate).isAfter(dayjs().subtract(7, "d").subtract(1, "month"), 'day')){
            endDate = dayjs(endDate);
          } else {
            endDate = dayjs().set("minute", 0).set("second", 0).set("hour", 0).set("ms", 0);
          }

          if (startDate && dayjs(startDate).isAfter(dayjs().subtract(1, "month"), 'day')){
            startDate = dayjs(startDate);
          } else {
            startDate = endDate.subtract(30, "d");
          }

          if(place){
            const riverStations = await synaptixSession.getObjects({
              modelDefinition: RiverStationDefinition,
              args: {
                first: 3,
                queryFilters: [
                  new QueryFilter({
                    filterDefinition: RiverStationDefinition.getFilter(
                      "closestFromGeoCoords"
                    ),
                    filterGenerateParams: {
                      lat: place.lat,
                      lon: place.lng
                    }
                  })
                ],
                sortings: [
                  new Sorting({
                    sortingDefinition: RiverStationDefinition.getSorting("closestFromGeoCoords"),
                    params: {
                      lat: place.lat,
                      lon: place.lng
                    }
                  })
                ]
              }
            });

            for(riverStation of riverStations){
              // Get the raw level from HubEau API
              const levelsRaw = await hubEauClient.getRiverLevels({
                stationCode: riverStation.stationCode,
                type: observationType,
                startDate: startDate.toISOString(),
                endDate: endDate.toISOString(),
              });

              // const levelsRaw = await synaptixSession
              //   .getIndexService()
              //   .getNodes({
              //     modelDefinition: RiverLevelDefinition,
              //     getRootQueryWrapper: ({ query }) => ({
              //       bool: {
              //         must: [
              //           {
              //             term: {
              //               [RiverLevelDefinition.getProperty("codeSite").getPathInIndex()+`.keyword`]: {
              //                 value: riverStation.stationCode
              //               }
              //             }
              //           },
              //           {
              //             range: {
              //               [RiverLevelDefinition.getProperty("date").getPathInIndex()]: {
              //                 lte: endDate.toISOString(),
              //                 gte: startDate.toISOString()
              //               }
              //             }
              //           }
              //         ]
              //       }
              //     })
              //   });

              if (levelsRaw.length > 0){
                // Reduce levels to mean by desired unit step.
                const levelsGroups = levelsRaw.reduce((acc, {date, level}) => {
                  let roundedDate;

                  switch (stepUnit) {
                    case "Day":
                      roundedDate = dayjs(date).format("YYYY-MM-DD");
                      break;
                    case "Week":
                      roundedDate = dayjs(date).format("YYYY-MM (ww)");
                      break;
                    case "Month":
                      roundedDate = dayjs(date).format("YYYY-MM");
                      break;
                    default:
                      roundedDate = date;
                  }

                  if (!acc[roundedDate]){
                    acc[roundedDate] = [];
                  }

                  acc[roundedDate].push(level);

                  return acc;
                }, {});

                return {
                  levels: Object.entries(levelsGroups).map(([date, levelGroup]) => ({
                    date,
                    level: round(mean(levelGroup), 2)
                  })),
                  riverStation
                }
              }
            }

            return {
              levels: [],
              riverStation
            }
          }
        }
    });
  }
}
