import {
  LinkDefinition,
  LiteralDefinition,
  ModelDefinitionAbstract,
  LinkPath,
  MnxOntologies, LabelDefinition
} from "@mnemotix/synaptix.js";
import {TaxonGraphQLDefinition} from "./graphql/TaxonGraphQLDefinition";
import {TaxonOccurrenceDefinition} from "./TaxonOccurrenceDefinition";
import {TaxonPictureDefinition} from "./TaxonPictureDefinition";

export class TaxonDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "mnb:Taxon";
  }

  /**
   * @return {string}
   */
  static getIndexType(){
    return "taxon";
  }

  /**
   * @inheritDoc
   */
  static getGraphQLDefinition() {
    return TaxonGraphQLDefinition;
  }

  /**
   * @inheritDoc
   */
  static getLinks(){
    return [
      new LinkDefinition({
        linkName: "hasHabitat",
        description: "Related Habitat SKOS concept",
        rdfObjectProperty: "http://taxref.mnhn.fr/lod/property/habitat",
        relatedModelDefinition: MnxOntologies.mnxSkos.ModelDefinitions.ConceptDefinition,
        isPlural: true
      }),
      new LinkDefinition({
        linkName: "hasArea",
        description: "Related Area SKOS concept",
        rdfObjectProperty: "mnb:area",
        relatedModelDefinition: MnxOntologies.mnxSkos.ModelDefinitions.ConceptDefinition
      }),
      new LinkDefinition({
        linkName: "hasOccurrence",
        pathInIndex: "isFeatureOfInterestOf",
        description: "Taxon occurrences. @see [this page](https://inpn.mnhn.fr/telechargement/standard-occurrence-taxon) for more information",
        rdfObjectProperty: "http://www.w3.org/ns/sosa/isFeatureOfInterestOf",
        isPlural: true,
        relatedModelDefinition: TaxonOccurrenceDefinition,
        graphQLPropertyName: "occurrences"
      }),
      new LinkDefinition({
        linkName: "hasPicture",
        pathInIndex: "depiction",
        description: "Taxon representative pictures",
        rdfObjectProperty: "http://xmlns.com/foaf/0.1/depiction",
        isPlural: true,
        relatedModelDefinition: TaxonPictureDefinition,
        graphQLPropertyName: "pictures"
      })
    ]
  }

  /**
   * @inheritDoc
   */
  static getLabels(){
    return [
      new LabelDefinition({
        labelName: "area",
        description: "Taxon area",
        linkPath: new LinkPath()
          .step({linkDefinition: TaxonDefinition.getLink("hasArea")})
          .property({
            propertyDefinition: MnxOntologies.mnxSkos.ModelDefinitions.ConceptDefinition.getLabel("prefLabel"),
            rdfDataPropertyAlias: 'mnb:area'
          })
      })
    ]
  }

  /**
   * @inheritDoc
   */
  static getLiterals(){
    return [
      new LiteralDefinition({
        literalName: "vernacularName",
        description: "Taxon varnacular name",
        rdfDataProperty: "http://taxref.mnhn.fr/lod/property/vernacularName",
      }),
      new LiteralDefinition({
        literalName: "label",
        rdfDataProperty: "http://www.w3.org/2000/01/rdf-schema#label",
      }),
      new LiteralDefinition({
        literalName: "familyName",
        description: "Taxon family name",
        rdfDataProperty: "mnb:familyName",
      }),
      new LiteralDefinition({
        literalName: "orderName",
        description: "Taxon order name",
        rdfDataProperty: "mnb:orderName",
      }),
      new LiteralDefinition({
        literalName: "className",
        description: "Taxon class name",
        rdfDataProperty: "mnb:className",
      }),
      new LiteralDefinition({
        literalName: "gbifTaxonKey",
        pathInIndex: "P846",
        description: "Taxon GBIF key also known as P846 property from wikidata entity",
        rdfDataProperty: "http://www.wikidata.org/entity/P846",
      }),
      new LiteralDefinition({
        literalName: "homepage",
        description: "Taxon information web homepage",
        rdfDataProperty: "foaf:homepage"
      }),
      new LiteralDefinition({
        literalName: "page",
        description: "Taxon information webpage",
        rdfDataProperty: "foaf:page"
      }),
      new LiteralDefinition({
        literalName: "bioGeographicalStatus",
        pathInIndex: "hasBioGeographicalStatusFranceMetro",
        description: "Taxon biogeographical status",
        rdfDataProperty: "mnb:hasBioGeographicalStatusFranceMetro"
      }),
      new LiteralDefinition({
        literalName: "habitats",
        pathInIndex: "habitat",
        description: "Taxon habitats",
        isPlural: true
      }),
    ]
  }
}
