import {
  LiteralDefinition,
  ModelDefinitionAbstract
} from "@mnemotix/synaptix.js";
import {TaxonPictureGraphQLDefinition} from "./graphql/TaxonPictureGraphQLDefinition";

export class TaxonPictureDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "http://xmlns.com/foaf/0.1/Image";
  }

  /**
   * @inheritDoc
   */
  static getGraphQLType() {
    return "TaxonPicture";
  }

  /**
   * @return {string}
   */
  static getIndexType(){
    return "image";
  }

  /**
   * @inheritDoc
   */
  static getGraphQLDefinition() {
    return TaxonPictureGraphQLDefinition;
  }

  /**
   * @inheritDoc
   */
  static getLiterals(){
    return [
      new LiteralDefinition({
        literalName: "copyright",
        pathInIndex: "rights",
        description: "Taxon picture copyright",
        rdfDataProperty: "http://purl.org/dc/elements/1.1/rights",
      }),
      new LiteralDefinition({
        literalName: "license",
        description: "Taxon picture license",
        rdfDataProperty: "http://purl.org/dc/terms/license",
      })
    ]
  }
}
