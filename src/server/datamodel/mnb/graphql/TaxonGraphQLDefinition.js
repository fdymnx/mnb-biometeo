import {
  GraphQLTypeDefinition,
  GraphQLProperty,
  SynaptixDatastoreSession,
  PropertyFilter,
  QueryFilter,
  Sorting
} from "@mnemotix/synaptix.js";
import { TaxonOccurrenceDefinition } from "../TaxonOccurrenceDefinition";
import { geonamesClient } from "@mnemotix/synaptix-api-toolkit-geonames";
import {TaxonDefinition} from "../TaxonDefinition";
import {TaxonGraphQLTypeConnectionQuery} from "./TaxonGraphQLTypeConnectionQuery";

export class TaxonGraphQLDefinition extends GraphQLTypeDefinition {
  /**
   * @inheritDoc
   */
  static getTypeConnectionQuery(){
    return new TaxonGraphQLTypeConnectionQuery();
  }

  /**
   * @inheritDoc
   */
  static getOverridenProperties() {
    return [
      new GraphQLProperty({
        name: TaxonDefinition.getLiteral("page").getLiteralName(),
        description: TaxonDefinition.getLiteral("page").getDescription(),
        type: "String",
        typeResolver: (taxon) => {
          if (Array.isArray(taxon.page)){
            const pages = taxon.page;
            return pages.find(page => page.indexOf("gbif.org") > -1);
          } else {
            return taxon.page;
          }
        }
      }),
      new GraphQLProperty({
        name: TaxonDefinition.getLiteral("habitats").getLiteralName(),
        description: TaxonDefinition.getLiteral("habitats").getDescription(),
        type: "[String]",
        typeResolver: (taxon) => {
          if(!Array.isArray(taxon.habitats)){
            return [taxon.habitats];
          }
          return taxon.habitats;
        }
      })
    ];
  }

  /**
   * Return list of properties to override
   * Throw error if one of the properties is already existing in related model
   *
   * @returns {GraphQLProperty[]}
   */
  static getExtraProperties() {
    return [
      new GraphQLProperty({
        name: "closestOccurrence",
        args: "geonamesId: ID!, distance: String",
        description: "Taxon closest occurence in meters",
        type: "Float",
        /**
         * @param {object} taxon
         * @param {Data} args
         * @param {SynaptixDatastoreSession} synaptixSession
         */
        typeResolver: async (taxon, { geonamesId, distance }, synaptixSession) => {
          const place = await geonamesClient.getPlaceById({ id: geonamesId });

          if (place) {
            const occurence = await synaptixSession.getObjects({
              modelDefinition: TaxonOccurrenceDefinition,
              args: {
                propertyFilters: [
                  new PropertyFilter({
                    propertyDefinition: TaxonOccurrenceDefinition.getLiteral(
                      "gbifTaxonKey"
                    ),
                    value: taxon.gbifTaxonKey
                  })
                ],
                queryFilters: [
                  new QueryFilter({
                    filterDefinition: TaxonOccurrenceDefinition.getFilter(
                      "closestFromGeoCoords"
                    ),
                    filterGenerateParams: {
                      lat: place.lat,
                      lon: place.lng,
                      distance
                    }
                  })
                ],
                sortings: [
                  new Sorting({
                    sortingDefinition: TaxonOccurrenceDefinition.getSorting("closestFromGeoCoords"),
                    params: {
                      lat: place.lat,
                      lon: place.lng
                    }
                  })
                ]
              },
              firstOne: 1
            });

            return occurence?.distance?.[0];
          }
        }
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getExtraGraphQLCode() {
    return `
      """ List of area terms to filter taxons on"""
      enum TaxonArea {
        Eau
        Sol
        Air
      }
    `;
  }
}
