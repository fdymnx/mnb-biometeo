import {
  LinkDefinition,
  LiteralDefinition,
  ModelDefinitionAbstract,
  MnxOntologies
} from "@mnemotix/synaptix.js";

export class CantonDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "http://rdf.insee.fr/def/geo#Canton2015";
  }

  /**
   * @return {string}
   */
  static getIndexType(){
    return "cantons";
  }

  /**
   * @inheritDoc
   */
  static getLinks(){
    return [
      new LinkDefinition({
        linkName: "hasSubdivision",
        pathInIndex: "subdivision",
        inIndexOnly: true,
        description: "Canton subvision (List of geonames entities)",
        relatedModelDefinition: MnxOntologies.mnxSkos.ModelDefinitions.ConceptDefinition
      })
    ]
  }

  /**
   * @inheritDoc
   */
  static getLiterals(){
    return [
      new LiteralDefinition({
        literalName: "label",
        description: "Canton label",
        rdfDataProperty: "http://www.w3.org/2000/01/rdf-schema#label",
      }),
      new LiteralDefinition({
        literalName: "codeCanton",
        description: "Canton code",
        rdfDataProperty: "http://rdf.insee.fr/def/geo#codeCanton",
      })
    ]
  }
}
