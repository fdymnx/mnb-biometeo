import { ModelDefinitionAbstract} from "@mnemotix/synaptix.js";
import {NewsGraphQLDefinition} from "./graphql/NewsGraphQLDefinition";

export class NewsDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getNodeType() {
    return "News";
  }

  /**
   * @inheritDoc
   */
  static getGraphQLDefinition() {
    return NewsGraphQLDefinition;
  }
}
