import { ModelDefinitionAbstract} from "@mnemotix/synaptix.js";
import {NotificationGraphQLDefinition} from "./graphql/NotificationGraphQLDefinition";

export class NotificationDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getNodeType() {
    return "Notification";
  }

  /**
   * @inheritDoc
   */
  static getGraphQLDefinition() {
    return NotificationGraphQLDefinition;
  }
}
