import {GraphQLTypeDefinition, GraphQLProperty} from "@mnemotix/synaptix.js";

import dayjs from "dayjs";
import {NewsGraphQLTypeQuery} from "./NewsGraphQLTypeQuery";

export class NewsGraphQLDefinition extends GraphQLTypeDefinition {
  /**
   * @inheritDoc
   */
  static getTypeQuery(){
    return new NewsGraphQLTypeQuery();
  }

  /**
   * @inheritDoc
   */
  static getExtraProperties() {
    return [
      new GraphQLProperty({
        name: "notifications",
        description: "This is the last notifications",
        type: "[Notification]",
        typeResolver: object => object.notifications
      })
    ];
  }
}