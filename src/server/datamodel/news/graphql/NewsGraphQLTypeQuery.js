import {
  GraphQLTypeQuery,
  LinkFilter,
  logDebug,
  logInfo,
  SynaptixDatastoreSession,
} from '@mnemotix/synaptix.js';
import env from 'env-var';
import dayjs from 'dayjs';
import { twitterClient } from '../../../services/TwitterClient';
import { geonamesClient } from '@mnemotix/synaptix-api-toolkit-geonames';
import { CantonDefinition } from '../CantonDefinition';

const idsToNameMappingCache = {};

export class NewsGraphQLTypeQuery extends GraphQLTypeQuery {
  /**
   * @inheritdoc
   */
  generateType(modelDefinition) {
    const graphQLType = modelDefinition.getGraphQLType();
    return this._wrapQueryType(`
      """
       This service returns a weather situation for a geonames place.
       
       Parameters :
         - geonamesId: [REQUIRED] Geonames place id.
      """
      ${this.generateFieldName(modelDefinition)}(geonamesId:ID!): ${graphQLType}
    `);
  }

  /**
   * @inheritdoc
   */
  generateResolver(modelDefinition) {
    return this._wrapQueryResolver({
      [this.generateFieldName(modelDefinition)]:
        /**
         * @param _
         * @param {string} geonamesId
         * @param {SynaptixDatastoreSession} synaptixSession
         * @param {object} info
         */
        async (_, { geonamesId }, synaptixSession, info) => {
          const twitterName = env
            .get('TWITTER_USER_SCREEN_NAME')
            .required()
            .asString();

          const place = await geonamesClient.getPlaceById({ id: geonamesId });
          geonamesId = geonamesId.replace('geonames:', '');

          if (place) {
            // Get list of cantons
            const canton = await synaptixSession.getObjects({
              modelDefinition: CantonDefinition,
              firstOne: true,
              args: {
                linkFilters: [
                  new LinkFilter({
                    // Yes I know it's weird, but the "cantons" index subvision URIs are indexed with trailing "/"
                    id: synaptixSession.extractIdFromGlobalId(place.id) + '/',
                    linkDefinition: CantonDefinition.getLink('hasSubdivision'),
                  }),
                ],
              },
            });

            let cantonId;

            if (canton) {
              cantonId = canton.id.replace(
                'http://id.insee.fr/geo/canton/',
                ''
              );
            }

            const tweets = await twitterClient.getTweets();

            // Interesting part of Tweet structure :
            // {
            //  created_at: 'Fri Jul 03 13:45:45 +0000 2020',
            //  id_str: '1279048688963408000',
            //  full_text: 'Les plus belles balades du secteur sud de la Dordogne #2410 et #2401 https://t.co/L37ZZyeivS',
            //   text   full_text tronqué
            //}

            const notifications = tweets.reduce((acc, tweet) => {
              let { id, id_str } = tweet;
              // don't use tweet.id, use tweet.id_str instead
              const tweetURL = `https://twitter.com/${twitterName}/status/${id_str}`;
              const content =
                tweet?.retweeted_status?.full_text ||
                tweet?.full_text ||
                tweet?.text ||
                '';

              const targetedIds = (
                content.match(/#[0-9]+/g) || []
              ).map((targetedId) => targetedId.replace('#', ''));
              // We add notification to the list if :
              // - User geonamesId doesn't match any canton in the DB (it happens as Geonames is not perfectly up to date)
              // - Tweet DOES NOT target any canton ID
              // - Tweet DOES target user canton ID
              // - Tweet DOES target use geonames ID
              if (
                !cantonId ||
                targetedIds?.length === 0 ||
                targetedIds.includes(cantonId) ||
                targetedIds.includes(geonamesId)
              ) {
                acc.push({
                  id: id_str,
                  date: dayjs(tweet.created_at).toISOString(),
                  content,
                  tweetURL,
                });
              }
              return acc;
            }, []);

            for (let notification of notifications) {
              let content = notification.content;
              const targetedIds = (
                content.match(/#[0-9]+/g) || []
              ).map((targetedId) => targetedId.replace('#', ''));

              if (targetedIds) {
                for (const targetedId of targetedIds) {
                  if (!idsToNameMappingCache[targetedId]) {
                    if (targetedId === geonamesId) {
                      idsToNameMappingCache[targetedId] = place.name;
                    } else if (targetedId.length === 7) {
                      const place = await geonamesClient.getPlaceById({
                        id: targetedId,
                      });

                      if (place) {
                        idsToNameMappingCache[targetedId] = place.name;
                      }
                    } else {
                      const canton = await synaptixSession.getObject({
                        modelDefinition: CantonDefinition,
                        objectId: `http://id.insee.fr/geo/canton/${targetedId}`,
                      });

                      if (canton) {
                        idsToNameMappingCache[targetedId] = canton.label;
                      }
                    }
                  }

                  content = content.replace(
                    new RegExp(`#${targetedId}`, 'g'),
                    `${idsToNameMappingCache[targetedId]}`
                  );
                }

                notification.content = content;
              }
            }

            return {
              notifications,
            };
          }

          return [];
        },
    });
  }
}
