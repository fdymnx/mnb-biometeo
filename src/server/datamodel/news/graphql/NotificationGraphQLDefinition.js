import { GraphQLTypeDefinition, GraphQLProperty } from '@mnemotix/synaptix.js';
import dayjs from 'dayjs';

export class NotificationGraphQLDefinition extends GraphQLTypeDefinition {
  /**
   * @inheritDoc
   */
  static getExtraProperties() {
    return [
      new GraphQLProperty({
        name: 'date',
        description: 'This is the notification date',
        type: 'String',
        typeResolver: (object) => object.date,
      }),
      new GraphQLProperty({
        name: 'content',
        description: 'This is the notification content',
        type: 'String',
        typeResolver: (object) => object.content,
      }),
      new GraphQLProperty({
        name: 'tweetURL',
        description: 'The original tweet url to see comments etc',
        type: 'String',
        typeResolver: (notification) => notification.tweetURL,
      }),
    ];
  }
}
