import {NewsDefinition} from "./NewsDefinition";
import {DataModel} from "@mnemotix/synaptix.js";
import {NotificationDefinition} from "./NotificationDefinition";
import {CantonDefinition} from "./CantonDefinition";

export const newsDataModel = new DataModel({
  modelDefinitions: [NewsDefinition, NotificationDefinition, CantonDefinition]
});