import { ModelDefinitionAbstract} from "@mnemotix/synaptix.js";
import {WeatherGraphQLDefinition} from "./graphql/WeatherGraphQLDefinition";

export class WeatherDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getNodeType() {
    return "Weather";
  }

  /**
   * @inheritDoc
   */
  static getGraphQLDefinition() {
    return WeatherGraphQLDefinition;
  }
}
