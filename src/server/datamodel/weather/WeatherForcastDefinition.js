import { ModelDefinitionAbstract} from "@mnemotix/synaptix.js";
import {WeatherForcastGraphQLDefinition} from "./graphql/WeatherForcastGraphQLDefinition";

export class WeatherForcastDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getNodeType() {
    return "WeatherForcast";
  }

  /**
   * @inheritDoc
   */
  static getGraphQLDefinition() {
    return WeatherForcastGraphQLDefinition;
  }
}
