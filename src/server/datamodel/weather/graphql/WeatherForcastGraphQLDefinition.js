import {GraphQLTypeDefinition, GraphQLProperty} from "@mnemotix/synaptix.js";
import dayjs from "dayjs";

export class WeatherForcastGraphQLDefinition extends GraphQLTypeDefinition {
  /**
   * @inheritDoc
   */
  static getExtraProperties() {
    return [
      new GraphQLProperty({
        name: "date",
        description: `
          This is forcast data
        `,
        type: "String",
        typeResolver: object => dayjs(object.dt * 1000).toISOString()
      }),
      new GraphQLProperty({
        name: "icon",
        description: `
          This is the weather icon code following.
          @see https://openweathermap.org/weather-conditions to get more information.
        `,
        type: "String",
        typeResolver: object => {           
          return object.weather?.[0]?.icon
        }
      }),
      new GraphQLProperty({
        name: "humidity",
        description: `
          This is the humidity in %
        `,
        type: "String",
        typeResolver: object => object.humidity
      }),
      new GraphQLProperty({
        name: "temperatureMin",
        description: `
          This is the mininum temperature in Celsius
        `,
        type: "String",
        typeResolver: object => object.temp.min
      }),
      new GraphQLProperty({
        name: "temperatureMax",
        description: `
          This is the maximum temperature in Celsius
        `,
        type: "String",
        typeResolver: object => object.temp.max
      }),
      new GraphQLProperty({
        name: "sunrise",
        description: `
          This is the sunrise datetime
        `,
        type: "String",
        typeResolver: object => dayjs(object.sunrise * 1000).toISOString()
      }),
      new GraphQLProperty({
        name: "sunset",
        description: `
          This is the sunset datetime
        `,
        type: "String",
        typeResolver: object => dayjs(object.sunset * 1000).toISOString()
      }),
      new GraphQLProperty({
        name: "windOrientation",
        description: `
          This is the wind orientation
        `,
        type: "String",
        typeResolver: object => object.wind_deg
      }),
      new GraphQLProperty({
        name: "windSpeed",
        description: `
          This is the wind speed in meter/sec
        `,
        type: "String",
        typeResolver: object => object.wind_speed
      }),
      new GraphQLProperty({
        name: "pressure",
        description: `
          This is the pressure in hPa
        `,
        type: "String",
        typeResolver: object => object.pressure
      }),
      new GraphQLProperty({
        name: "uvIndex",
        description: `
          This is the UV index 
        `,
        type: "String",
        typeResolver: object => object.uvi
      })
    ];
  }
}