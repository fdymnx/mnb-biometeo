import {WeatherDefinition} from "./WeatherDefinition";
import {WeatherForcastDefinition} from "./WeatherForcastDefinition";
import {DataModel} from "@mnemotix/synaptix.js";

export const weatherDataModel = new DataModel({
  modelDefinitions: [WeatherDefinition, WeatherForcastDefinition]
});