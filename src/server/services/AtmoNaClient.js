/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import got from "got";
import {I18nError, logError} from "@mnemotix/synaptix.js";
import dayjs from "dayjs";
import {createCache} from "./cache/Cache";

// TTL = 1 hour
const cache = createCache({ttl: 3600});

class AtmoNaClient {
  /**
   * Make an API Request
   * @param {string} service - OpenWeather API service name (IE: onecall)
   * @param {object} [params]  - OpenWeather API service params
   * @return {object}
   */
  async get({ params = {} } = {}) {
    params.cacheBuster = dayjs().format("YYYYMMDDHH");

    const serializedParams = Object.entries(params)
      .reduce((acc, [key, value]) => {
        if (Array.isArray(value)) {
          for (let valueFragment of value) {
            acc.push(`${key}=${valueFragment}`);
          }
        } else {
          acc.push(`${key}=${value}`);
        }
        return acc;
      }, [])
      .join("&");

    const uri = `https://opendata.atmo-na.org/api/v1/indice/atmo?${serializedParams}`;

    if (cache.has(uri)) {
      return cache.get(uri);
    }

    let response = await got(uri, { retry: 3 }).json();

    if (response) {
      cache.set(uri, response);
      return response;
    }
  }

  /**
   * Make an API Request
   * @param {object} [params]  - OpenWeather API service params
   * @return {{
   *   previousDay: object,
   *   current: object,
   *   nextDay: object
   * }}
   */
  async getCurrentAtmoIndices({ params = {} } = {}) {
    // @see https://gitlab.com/cg24/mnb-repository/-/blob/master/doc/indice-atmo-station-and-connector.md
    // So far there's only 1 station for Dordogne referenced by zone id "24322"
    const zone = 24322;

    try {
      let {features} =
      (await this.get({
        params: {
          zone,
          date_deb: dayjs().subtract(1, "day").format("YYYY-MM-DD"),
          date_fin: dayjs().add(1, "day").format("YYYY-MM-DD"),
          type: "json"
        }
      })) || {};

      const previousDay = (features?.[2]?.properties?.valeur || 0) * 10;
      const current     = (features?.[1]?.properties?.valeur || 0) * 10;
      const nextDay     = (features?.[0]?.properties?.valeur || 0) * 10;

      return {
        previousDay,
        current,
        nextDay,
        trend: previousDay === current ? "stable" :  previousDay > current ? "decreasing" : "increasing",
        nextDayTrend: current === nextDay ? "stable" : current > nextDay ? "decreasing" : "increasing"
      };
    } catch (e) {
      logError("There is an error while fetch Atom NA API :");
      logError(e);
      return {};
    }
  }
}

export const atmoNaClient = new AtmoNaClient();
