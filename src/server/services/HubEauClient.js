/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import got from "got";
import dayjs from "dayjs";
import {createCache} from "./cache/Cache";

// TTL = 1 day
const cache = createCache({ttl: 86400});

class HubEauClient {
  /**
   * Make an API Request
   * @param {string} service - OpenWeather API service name (IE: onecall)
   * @param {object} [params]  - OpenWeather API service params
   * @param {string} [openWeatherKey] - OpenWeather key
   * @return {object}
   */
  async get({ service, params = {} } = {}) {
    params.cacheBuster = dayjs().format("YYYYMMDD");

    const serializedParams = Object.entries(params)
      .reduce((acc, [key, value]) => {
        if (Array.isArray(value)) {
          for (let valueFragment of value) {
            acc.push(`${key}=${valueFragment}`);
          }
        } else {
          acc.push(`${key}=${value}`);
        }
        return acc;
      }, [])
      .join("&");

    const uri = `https://hubeau.eaufrance.fr/api/v1/${service}?${serializedParams}`;

    if (cache.has(uri)) {
      return cache.get(uri);
    }

    try{
      let response = await got(uri, { retry: 3 }).json();

      if (response) {
        cache.set(uri, response);
        return response;
      }
    } catch (e) {
      console.error("There is an error from HubEau API from requested URI : ", uri);
      console.error(e);
    }

  }

  /**
   * Make an API Request to get river levels for a station code
   * @param {string} stationCode - Station Code
   * @param {string} startDate - Start date of observations
   * @param {string} endDate   - End date of observations
   * @param {object} [type="H"]  - Type of observation. H : level, Q: debit.
   * @return {Promise<any>}
   */
  async getRiverLevels({ stationCode, startDate, endDate, type = "H"} = {}) {
    const results = await this.get({
      service: "hydrometrie/observations_tr",
      params: {
        code_entite: stationCode,
        date_debut_obs: startDate,
        date_fin_obs: endDate,
        size: 800,
        grandeur_hydro: type,
        timestep:60,
        fields: "resultat_obs,date_obs"
      }
    });

    return results?.data.map(({date_obs, resultat_obs}) => ({
      date: date_obs,
      level: resultat_obs
    })) || [];
  }

  /**
   * Make an API Request to get river levels for a station code
   * @param {string} stationCode - Station Code
   * @param {string} startDate   -  Start date of observation
   * @param {string} endDate     - End of observations
   * @return {Promise<any>}
   */
  async getGroundWaterLevels({ stationCode, startDate, endDate } = {}) {
    let results = await this.get({
      service: "niveaux_nappes/chroniques",
      params: {
        code_bss: stationCode,
        sort: "desc",
        size: 400,
        date_debut_mesure: startDate,
        date_fin_mesure: endDate,
        fields: "niveau_nappe_eau,date_mesure"
      }
    });

    return results?.data?.map(({date_mesure, niveau_nappe_eau}) => ({
      date: date_mesure,
      level: niveau_nappe_eau
    })) || [];
  }
}

export const hubEauClient = new HubEauClient();
