/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import got from "got";
import { I18nError } from "@mnemotix/synaptix.js";
import { geonamesClient } from "@mnemotix/synaptix-api-toolkit-geonames";
import dayjs from "dayjs";
import {createCache} from "./cache/Cache";

// TTL = 1 hour
const cache = createCache({ttl: 3600});

class OpenWeatherClient {
  /**
   * Make an API Request
   * @param {string} service - OpenWeather API service name (IE: onecall)
   * @param {object} [params]  - OpenWeather API service params
   * @param {string} [openWeatherKey] - OpenWeather key
   * @return {object}
   */
  async get({ openWeatherKey, service, params = {} } = {}) {
    if (!openWeatherKey) {
      if (!!process.env.OPEN_WEATHER_KEY) {
        openWeatherKey = process.env.OPEN_WEATHER_KEY;
      } else {
        throw new I18nError(
          "You must provide a OpenWeather authorization key to request API. Pass it in OPEN_WEATHER_KEY environment variable to set it globally."
        );
      }
    }

    params.appid = openWeatherKey;
    params.cacheBuster = dayjs().format("YYYYMMDDHH");
    params.units = "metric";

    const serializedParams = Object.entries(params)
      .reduce((acc, [key, value]) => {
        if (Array.isArray(value)) {
          for (let valueFragment of value) {
            acc.push(`${key}=${valueFragment}`);
          }
        } else {
          acc.push(`${key}=${value}`);
        }
        return acc;
      }, [])
      .join("&");

    const uri = `https://api.openweathermap.org/data/2.5/${service}?${serializedParams}`;

    if (cache.has(uri)) {
      return cache.get(uri);
    }

    let response = await got(uri, { retry: 3 }).json();

    if (response) {
      cache.set(uri, response);
      return response;
    }
  }

  /**
   * Make an API Request
   * @param {string} geonamesId
   * @param {object} [params]  - OpenWeather API service params
   * @param {string} [openWeatherKey] - OpenWeather key
   * @return {Promise<any>}
   */
  async getCurrentWeatherForGeonamesId({
    geonamesId,
    openWeatherKey,
    params = {}
  } = {}) {
    // First get the location of geonames place.
    let place = await geonamesClient.getPlaceById({ id: geonamesId });

    if (place) {

      let {current: currentWeather, daily} = (await this.get({
        openWeatherKey,
        service: "onecall",
        params: {
          lat: place.lat,
          lon: place.lng,
          exclude: "minutely,hourly"
        }
      })) || {};

      currentWeather.forcast = daily;

      let pastDayWeather = (await this.get({
        openWeatherKey,
        service: "onecall/timemachine",
        params: {
          lat: place.lat,
          lon: place.lng,
          dt: dayjs().subtract(1, "day").set('second', 0).set('minute', 0).unix() ,
          exclude: "minutely,hourly,daily"
        }
      }))?.current;

      // Add missing values
      const dayTimeDiff = ((currentWeather.sunset - currentWeather.sunrise) - (pastDayWeather.sunset - pastDayWeather.sunrise)) / 60;
      currentWeather.dayTimeDiff = dayTimeDiff > 0 ? Math.ceil(dayTimeDiff) : Math.floor(dayTimeDiff);
      currentWeather.temperatureTrend = pastDayWeather.temp > currentWeather.temp ? "decreasing" : "increasing";
      currentWeather.uvIndexTrend = pastDayWeather.uvi > currentWeather.uvi ? "decreasing" : "increasing";

      return currentWeather;
    } else {
      throw new I18nError(
        `Geonames place is not found for geonames ID ${geonamesId}`
      );
    }
  }
}

export const weatherClient = new OpenWeatherClient();
