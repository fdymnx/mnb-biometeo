import dayjs from 'dayjs';
import env from 'env-var';
import Twitter from 'twitter-lite';
import { createCache } from './cache/Cache';

// TTL = 1 minute
const cache = createCache({ ttl: 3600 });

class TwitterClient {
  /**
   * Get last tweets and cache them every minute.
   * @param {string} [apiKey] - Twitter API key. TWITTER_API_KEY env var is required if paramater not set.
   * @param {string} [apiSecret] -  Twitter API secret key. TWITTER_SECRET_KEY env var is required if paramater not set.
   * @param {string} [userScreenName] - Tweet user Screen name (name followed by @). TWITTER_USER_SCREEN_NAME env var is required if paramater not set.
   * @return {array}
   */
  async getTweets({ apiKey, apiSecret, userScreenName } = {}) {
    if (!apiKey) {
      apiKey = env.get('TWITTER_API_KEY').required().asString();
    }

    if (!apiSecret) {
      apiSecret = env.get('TWITTER_SECRET_KEY').required().asString();
    }

    if (!userScreenName) {
      userScreenName = env
        .get('TWITTER_USER_SCREEN_NAME')
        .required()
        .asString();
    }

    this._userScreenName = userScreenName;

    this._twitterClient = new Twitter({
      consumer_key: apiKey,
      consumer_secret: apiSecret,
      // access_token_key: accessTokenKey,
      // access_token_secret: accessTokenSecret
    });

    // Cache API request to every minute.
    // Might limit rate to 1440 req/day. So this is ok !
    const cacheKey = dayjs().format('YYYYMMDDHHmm');

    if (cache.has(cacheKey)) {
      return cache.get(cacheKey);
    }

    const { access_token: bearer_token } =
      (await this._twitterClient.getBearerToken()) || {};

    const twitterApp = new Twitter({
      bearer_token,
    });

    const response = await twitterApp.get('statuses/user_timeline', {
      screen_name: this._userScreenName,
      count: 200,
      tweet_mode: 'extended',
    });

    if (response) {
      cache.set(cacheKey, response);
      return response;
    }
  }

  /**
   * @return {Twitter}
   */
  get twitterClient() {
    return this._twitterClient;
  }

  /**
   * @return {string}
   */
  get userScreenName() {
    return this._userScreenName;
  }
}

export const twitterClient = new TwitterClient();
